import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import Device from './core/Device'
import './index.scss'
import Storage from './storage/Storage'
import Translations from './translations/Translations'
import registerServiceWorker, { unregister } from './registerServiceWorker'

// Initialize engine classes
Storage.initialize()
Translations.initialize()

// Initialize the DOM
ReactDOM.render(
  <App />,
  document.getElementById('root')
)

// Register the service worker
if (window.innerWidth <= Device.mobileResolution) {
	registerServiceWorker()
} else {
	unregister()
}