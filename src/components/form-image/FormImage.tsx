import { ImageRounded } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import Translations from '../../translations/Translations'
import FormButton from '../form-button/FormButton'
import InfoModal from '../modals/info/InfoModal'
import Modal from '../modals/Modal'
import './form-image.scss'

/**
 * The props
 */
interface FormImageProps {
    /**
     * The current image
     */
    value: string | null

    /**
     * The on fail callback
     */
    onFail?: () => void

    /**
     * The on change callback
     */
    onChange: (base64: string|null) => void

    /**
     * The desired width for the image to be used
     */
    desiredWidth?: number
}

/**
 * An image in a form.
 * @author Stan Hurks
 */
export default class FormImage extends React.Component<FormImageProps> {
    /**
     * The ref to the input element
     */
    private inputRef: React.RefObject<HTMLInputElement> = React.createRef()

    public render = () => {
        return (
            <div className={classNames({
                'form-image': true
            })}>
                <input type="file" ref={this.inputRef} accept="image/*" style={{
                    position: 'fixed',
                    left: '-1000px',
                    zIndex: -1
                }} onChange={(event) => {
                    if (event.target.files && event.target.files.length === 1) {
                        this.onChange(event.target.files[0])
                    }
                }} />
                <div className="form-image-content">
                    {
                        this.props.value === null
                        ? <>
                            <div className="form-image-empty" onClick={() => {
                                if (this.inputRef.current) {
                                    this.inputRef.current.click()
                                }
                            }} 
                            onDragEnter={(event) => {
                                event.preventDefault()
                                event.stopPropagation()
                            }}
                            onDragOver={(event) => {
                                event.preventDefault()
                                event.stopPropagation()
                            }}
                            onDragLeave={(event) => {
                                event.preventDefault()
                                event.stopPropagation()
                            }}
                            onDrop={(event) => {
                                event.preventDefault()
                                event.stopPropagation()
                                const dataTransfer = event.dataTransfer
                                const files = dataTransfer.files
                                if (files.length && files.length === 1 && files[0].type.includes('image')) {
                                    this.onChange(files[0])
                                }
                            }}
                            >
                                <div className="form-image-empty-title">
                                    Click to upload an image.<br/>
                                    Or drag and drop your image here.
                                </div>
                                <div className="form-image-empty-icon">
                                    <ImageRounded />
                                </div>
                            </div>
                        </>
                        : <>
                            <div className="form-image-image" onClick={(event) => {
                                let found: boolean = false
                                const findParent = (root: Element) => {
                                    if (root.classList.contains('form-image-image-buttons')) {
                                        found = true
                                    }
                                    if (root.parentElement) {
                                        findParent(root.parentElement)
                                    }
                                }
                                findParent(event.target as Element)
                                if (found) {
                                    return
                                }
                                if (this.inputRef.current) {
                                    this.inputRef.current.click()
                                }
                            }}>
                                <div className="form-image-image-background">
                                    <img src={`${this.props.value}`} alt="Uploaded image" />
                                </div>
                                <div className="form-image-image-buttons">
                                    <FormButton variant="error" onClick={() => {
                                        this.props.onChange(null)
                                    }}>
                                        Delete
                                    </FormButton>
                                </div>
                            </div>
                        </>
                    }
                </div>
            </div>
        )
    }

    /**
     * Whenever the input changes value
     */
    private onChange = (file: File) => {
        const reader = new FileReader()

        reader.readAsDataURL(file)
        reader.onload = () => {
            const canvas = document.createElement('canvas')
            const context = canvas.getContext('2d')
            if (context === null) {
                if (this.props.onFail) {
                    this.props.onFail()
                }
                return
            }

            const image = new Image()
            image.setAttribute('crossOrigin', 'anonymous')
            image.src = reader.result as string
            image.onload = () => {
                const width = this.props.desiredWidth 
                    ? this.props.desiredWidth
                    : image.width
                const height = this.props.desiredWidth
                    ? image.height * (this.props.desiredWidth / image.width)
                    : image.height
                canvas.width = width
                canvas.height = height
                context.drawImage(image, 0, 0, width, height)
                this.props.onChange(canvas.toDataURL('base64'))
            }
            image.onerror = (error) => {
                if (this.props.onFail) {
                    this.props.onFail()
                }
                console.error(error)
            }
        }
        reader.onerror = (error) => {
            if (this.props.onFail) {
                this.props.onFail()
            }
            console.error(error)
        }
    }
}