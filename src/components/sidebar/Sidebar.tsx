import { AccountCircle, Category, ChevronLeft, Close, Contacts, Dashboard, Menu, Palette, Settings, VpnKey } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import App from '../../App'
import Device from '../../core/Device'
import { Route } from '../../enum/Route'
import Storage from '../../storage/Storage'
import Translations from '../../translations/Translations'
import SidebarItem from './item/SidebarItem'
import './sidebar.scss'

/**
 * The props
 */
interface SidebarProps {
    /**
     * Whether or not the sidebar is visible
     */
    visible: boolean

    /**
     * The current route
     */
    currentRoute: {
        route: Route
        previous?: {
            route: Route
            variables?: {[key: string]: string}
        }
    } | null
}

/**
 * The state
 */
interface SidebarState {
    /**
     * Whether the sidebar is open
     */
    open: boolean
}

/**
 * The sidebar
 * @author Stan Hurks
 */
export default class Sidebar extends React.Component<SidebarProps, SidebarState> {
    /**
     * Toggle the sidebar
     */
    public static toggle: Subject<boolean> = new Subject()
    private subscriptionToggle: Subscription | null = null

    constructor(props: any) {
        super(props)

        this.state = {
            open: false
        }
    }
    
    public componentDidMount = () => {
        this.subscriptionToggle = Sidebar.toggle.subscribe((open) => {
            this.setState({
                open
            }, () => {
                if (window.innerWidth <= Device.mobileResolution) {
                    if (this.state.open) {
                        App.setBodyScroll.next(true)
                    } else {
                        App.setBodyScroll.next(false)
                    }
                }
            })
        })
        window.addEventListener('resize', this.onResize)
    }

    public componentWillUnmount = () => {
        this.subscriptionToggle?.unsubscribe()
        window.removeEventListener('resize', this.onResize)
    }
    
    public render = () => {
        return (
            <>
                <div className={classNames({
                    'sidebar': true,
                    'open': this.state.open,
                    'visible': this.props.visible
                })} onMouseEnter={() => {
                    if (window.innerWidth <= Device.mobileResolution) {
                        return
                    }
                    this.setState({
                        open: true
                    })
                }} onMouseLeave={() => {
                    if (window.innerWidth <= Device.mobileResolution) {
                        return
                    }
                    this.setState({
                        open: false
                    })
                }}>
                    <div className="sidebar-top">
                        <SidebarItem icon="It" label="Invotation" onClick={() => {
                            window.open('https://invotation.com', '_blank')
                        }} />
                        <SidebarItem icon={(<Dashboard />)} label="Dashboard" route={Route.DASHBOARD} currentRoute={this.props.currentRoute?.route} />
                        <SidebarItem icon={(
                            <svg xmlns="http://www.w3.org/2000/svg" enableBackground="new 0 0 24 24" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0,0h24v24H0V0z" fill="none"/><g><path d="M19.5,3.5L18,2l-1.5,1.5L15,2l-1.5,1.5L12,2l-1.5,1.5L9,2L7.5,3.5L6,2v14H3v3c0,1.66,1.34,3,3,3h12c1.66,0,3-1.34,3-3V2 L19.5,3.5z M19,19c0,0.55-0.45,1-1,1s-1-0.45-1-1v-3H8V5h11V19z"/><rect height="2" width="6" x="9" y="7"/><rect height="2" width="2" x="16" y="7"/><rect height="2" width="6" x="9" y="10"/><rect height="2" width="2" x="16" y="10"/></g></svg>
                        )} label="Invoices" childItems={[
                            {
                                icon: 'I',
                                label: 'Invoices',
                                route: Route.INVOICES,
                                currentRoute: this.props.currentRoute?.route
                            },
                            {
                                icon: 'E',
                                label: 'Expenses',
                                route: Route.EXPENSES,
                                currentRoute: this.props.currentRoute?.route
                            }
                        ]} />
                        <SidebarItem icon={(
                            <svg xmlns="http://www.w3.org/2000/svg" enableBackground="new 0 0 24 24" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><g><rect fill="none" height="24" width="24"/></g><g><g><path d="M9,4v1.38c-0.83-0.33-1.72-0.5-2.61-0.5c-1.79,0-3.58,0.68-4.95,2.05l3.33,3.33h1.11v1.11c0.86,0.86,1.98,1.31,3.11,1.36 V15H6v3c0,1.1,0.9,2,2,2h10c1.66,0,3-1.34,3-3V4H9z M7.89,10.41V8.26H5.61L4.57,7.22C5.14,7,5.76,6.88,6.39,6.88 c1.34,0,2.59,0.52,3.54,1.46l1.41,1.41l-0.2,0.2c-0.51,0.51-1.19,0.8-1.92,0.8C8.75,10.75,8.29,10.63,7.89,10.41z M19,17 c0,0.55-0.45,1-1,1s-1-0.45-1-1v-2h-6v-2.59c0.57-0.23,1.1-0.57,1.56-1.03l0.2-0.2L15.59,14H17v-1.41l-6-5.97V6h8V17z"/></g></g></svg>
                        )} label="Quotations" route={Route.QUOTATIONS} currentRoute={this.props.currentRoute?.route} />
                        <SidebarItem icon={(<Palette />)} label="Templates" route={Route.TEMPLATES} currentRoute={this.props.currentRoute?.route} />
                        <SidebarItem icon={(<Contacts />)} label="Clients" route={Route.CLIENTS} currentRoute={this.props.currentRoute?.route} />
                        <SidebarItem icon={(<Category />)} label="Items" route={Route.ITEMS} currentRoute={this.props.currentRoute?.route} />
                    </div>
                    <div className="sidebar-bottom">
                        <SidebarItem icon={(<AccountCircle />)} label="Account" childItems={[
                            {
                                icon: 'B',
                                label: 'Businesses',
                                route: Route.ACCOUNT_BUSINESSES,
                                currentRoute: this.props.currentRoute?.route
                            },
                            {
                                icon: 'S',
                                label: 'Subscription',
                                route: Route.ACCOUNT_SUBSCRIPTION,
                                currentRoute: this.props.currentRoute?.route
                            },
                            {
                                icon: 'A',
                                label: 'Account settings',
                                route: Route.ACCOUNT_SETTINGS,
                                currentRoute: this.props.currentRoute?.route
                            }
                        ]} />
                        <SidebarItem icon={(<Settings />)} label="Settings" route={Route.SETTINGS} currentRoute={this.props.currentRoute?.route} />
                        <SidebarItem icon={(<VpnKey />)} label="Logout" onClick={() => {
                            Storage.reset()
                            App.redirect.next({
                                route: Route.LOGIN
                            })
                        }}/>
                    </div>
                </div>
                <div className={classNames({
                    'sidebar-background': true,
                    'open': this.state.open
                })}></div>
                <div className={classNames({
                    'sidebar-top-mobile': true,
                    'visible': this.props.visible
                })}>
                    <div className="sidebar-top-mobile-left" onClick={() => {
                        if (this.props.currentRoute && this.props.currentRoute.previous) {
                            App.redirect.next(this.props.currentRoute.previous)
                        }
                    }}>
                        {
                            this.props.currentRoute && this.props.currentRoute.previous
                            &&
                            <ChevronLeft />
                        }
                    </div>
                    <div className="sidebar-top-mobile-center">
                        {
                            this.state.open
                            ? null
                            : (
                                this.props.currentRoute
                                ? Translations.translations.title(this.props.currentRoute.route)
                                : 'Invotation'
                            )
                        }
                    </div>
                    <div className="sidebar-top-mobile-right" onClick={() => {
                        this.setState({
                            open: !this.state.open
                        }, () => {
                            if (this.state.open) {
                                App.setBodyScroll.next(true)
                            } else {
                                App.setBodyScroll.next(false)
                            }
                        })
                    }}>
                        {
                            this.state.open
                            ? <Close />
                            : <Menu />
                        }
                    </div>
                </div>
            </>
        )
    }

    /**
     * The handler for the resize event
     */
    private onResize = () => {
        if (window.innerWidth > Device.mobileResolution) {
            App.setBodyScroll.next(false)
        }
    }
}