import { ChevronLeft } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import App from '../../../App'
import Device from '../../../core/Device'
import { Route } from '../../../enum/Route'
import Sidebar from '../Sidebar'

/**
 * The props
 */
interface SidebarItemProps {
    /**
     * The icon to display
     */
    icon: JSX.Element | string
    
    /**
     * The label to display
     */
    label: string

    /**
     * The route associated
     */
    route?: Route

    /**
     * The current route of the application
     */
    currentRoute?: Route | null

    /**
     * The action on click
     */
    onClick?: () => void

    /**
     * The child items
     */
    childItems?: Array<SidebarItemProps>
}

/**
 * The state
 */
interface SidebarItemState {
    /**
     * Whether or not the menu is open
     */
    menuOpen: boolean
}

/**
 * A sidebar item
 * @author Stan Hurks
 */
export default class SidebarItem extends React.Component<SidebarItemProps, SidebarItemState> {
    constructor(props: any) {
        super(props)

        this.state = {
            menuOpen: false
        }
    }

    public render = () => {
        return (
            <>
                <div className={classNames({
                    'sidebar-item': true,
                    'menu-open': this.state.menuOpen,
                    'active': this.props.currentRoute && this.props.route
                        && this.props.currentRoute.startsWith(this.props.route)
                })} onClick={() => {
                    if (this.props.childItems && this.props.childItems.length) {
                        this.setState({
                            menuOpen: !this.state.menuOpen
                        })
                    } else if(this.props.route) {
                        App.redirect.next({
                            route: this.props.route
                        })
                        if (window.innerWidth <= Device.mobileResolution) {
                            Sidebar.toggle.next(false)
                        }
                    } else if (this.props.onClick) {
                        this.props.onClick()
                        if (window.innerWidth <= Device.mobileResolution) {
                            Sidebar.toggle.next(false)
                        }
                    }
                }}>
                    <div className="sidebar-item-left">
                        {this.props.icon}
                    </div><div className="sidebar-item-right">
                        {this.props.label}
                        {
                            this.props.childItems && this.props.childItems.length
                            &&
                            <div className="sidebar-item-right-icon">
                                <ChevronLeft />
                            </div>
                        }
                    </div>
                </div>

                {
                    this.props.childItems && this.props.childItems.length
                    &&
                    <div className="sidebar-item-menu" style={{
                        maxHeight: this.state.menuOpen
                            ? 75 * this.props.childItems.length
                            : 0
                    }}>
                        {
                            this.props.childItems.map((item, index) => (
                                <SidebarItem {...item} key={index} />
                            ))
                        }
                    </div>
                }
            </>
        )
    }
}