import { Error } from '@material-ui/icons'
import React from 'react'
import Translations from '../../../translations/Translations'
import FormButton from '../../form-button/FormButton'
import Modal from '../Modal'

interface InfoModalProps {
    /**
     * The title to display
     */
    title: string

    /**
     * The content to display
     */
    content: string|JSX.Element

    /**
     * The button to display
     */
    button: JSX.Element
}

/**
 * A modal for displaying information
 * @author Stan Hurks
 */
export default class InfoModal extends React.Component<InfoModalProps> {

    public render = () => {
        return (
            <div className="modal-child">
                <div className="modal-child-icon">
                    <Error />
                </div>
                <div className="modal-child-title">
                    {
                        this.props.title
                    }
                </div>
                <div className="modal-child-paragraph">
                    {
                        this.props.content
                    }
                </div>
                <div className="modal-child-buttons">
                    <div className="modal-child-buttons-left">
                        <FormButton variant="white" onClick={() => {
                            Modal.unmount.next()
                        }}>
                            {Translations.translations.components.modals.info.cancel}
                        </FormButton>
                    </div>
                    <div className="modal-child-buttons-right">
                        {
                            this.props.button
                        }
                    </div>
                </div>
            </div>
        )
    }
}