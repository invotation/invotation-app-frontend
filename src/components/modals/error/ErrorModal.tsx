import { Warning } from '@material-ui/icons'
import React from 'react'
import Translations from '../../../translations/Translations'
import FormButton from '../../form-button/FormButton'
import Modal from '../Modal'

interface ErrorModalProps {
    /**
     * The title to display
     */
    title: string

    /**
     * The content to display
     */
    content: string|JSX.Element
}

/**
 * A modal for displaying errors.
 * @author Stan Hurks
 */
export default class ErrorModal extends React.Component<ErrorModalProps> {

    public render = () => {
        return (
            <div className="modal-child">
                <div className="modal-child-icon error">
                    <Warning />
                </div>
                <div className="modal-child-title">
                    {
                        this.props.title
                    }
                </div>
                <div className="modal-child-paragraph">
                    {
                        this.props.content
                    }
                </div>
                <div className="modal-child-buttons">
                    <div className="modal-child-buttons-left"></div>
                    <div className="modal-child-buttons-right">
                        <FormButton variant="error" onClick={() => {
                            Modal.unmount.next()
                        }}>
                            {Translations.translations.components.modals.error.okay}
                        </FormButton>
                    </div>
                </div>
            </div>
        )
    }
}