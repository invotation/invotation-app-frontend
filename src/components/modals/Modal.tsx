import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import App from '../../App'
import './modal.scss'

interface ModalState {
    /**
     * The current modal
     */
    currentModal: JSX.Element | null

    /**
     * Whether or not the current modal is closing
     */
    closing: boolean
}

/**
 * A default component for all modals in the app.
 * @author Stan Hurks
 */
export default class Modal extends React.Component<any, ModalState> {
    /**
     * The amount of ms for the animation of the modal
     */
    private static readonly animationTimeMs: number = 300

    /**
     * Mounts a modal
     */
    public static mount: Subject<JSX.Element> = new Subject()
    private subscriptionMount: Subscription|null = null

    /**
     * Unmounts a modal
     */
    public static unmount: Subject<void> = new Subject()
    private subscriptionUnmount: Subscription|null = null

    constructor(props: any) {
        super(props)

        this.state = {
            currentModal: null,
            closing: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionMount = Modal.mount.subscribe((currentModal) => {
            if (this.state.closing) {
                return
            }
            App.setBodyScroll.next(true)
            this.setState({
                currentModal
            })
        })
        this.subscriptionUnmount = Modal.unmount.subscribe(() => {
            App.setBodyScroll.next(false)
            this.setState({
                closing: true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        currentModal: null,
                        closing: false
                    })
                }, Modal.animationTimeMs)
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionUnmount?.unsubscribe()
        this.subscriptionMount?.unsubscribe()
    }

    public render = () => {
        return (
            <div className={classNames({
                'modal': true,
                'show': this.state.currentModal !== null && !this.state.closing
            })}>
                <div className={classNames({
                    'modal-content': true,
                    'show': this.state.currentModal !== null,
                    'hide': this.state.closing
                })}>
                    {
                        this.state.currentModal
                    }
                </div>
            </div>
        )
    }
}