import React from 'react'
import classNames from 'classnames'
import './form-button.scss'

export interface FormButtonProps {
    variant?: 'primary' | 'dark' | 'white' | 'error' | 'light'
    type?: 'submit'
    disabled?: boolean
    onClick?: () => void
    link?: string
    startAdornment?: JSX.Element
    endAdornment?: JSX.Element
}

export interface FormButtonState {
    isMouseDown: boolean
    isHovered: boolean
}

/**
 * Represents a button.
 * @author Anonymous
 */
export default class FormButton extends React.Component<FormButtonProps, FormButtonState> {
    constructor(props: any) {
        super(props)
        this.state = {
            isMouseDown: false,
            isHovered: false
        }
    }
    
    public render = () => {
        return (
            <div className="form-button"
                onMouseDown={() => this.setState({isMouseDown: true})}
                onMouseUp={() => this.setState({isMouseDown: false})}
                onMouseOver={() => this.setState({isHovered: true})}
                onMouseOut={() => this.setState({isHovered: false})}
                >
                {
                    this.props.onClick
                    ?
                    <button
                        className={classNames({
                            'form-button-button': true,
                            'mousedown': this.state.isMouseDown,
                            ['variant-' + (this.props.variant || 'primary')]: true
                        })}
                        disabled={this.props.disabled}
                        onClick={() => {
                            if (this.props.disabled) {
                                return
                            }
                            if (this.props.onClick) {
                                this.props.onClick()
                            }
                        }}
                        type={this.props.type || 'button'}>
                        {
                            this.props.startAdornment
                            &&
                            <div className="form-button-button-start-adornment">
                                {
                                    this.props.startAdornment
                                }
                            </div>
                        }
                        {
                            this.props.children
                        }
                        {
                            this.props.endAdornment
                            &&
                            <div className="form-button-button-end-adornment">
                                {
                                    this.props.endAdornment
                                }
                            </div>
                        }
                    </button>
                    :
                    <a className={classNames({
                        'form-button-button': true,
                        'mousedown': this.state.isMouseDown,
                        ['variant-' + (this.props.variant || 'primary')]: true
                    })}
                    href={this.props.link}>
                    {
                        this.props.startAdornment
                        &&
                        <div className="form-button-button-start-adornment">
                            {
                                this.props.startAdornment
                            }
                        </div>
                    }
                    {
                        this.props.children
                    }
                    {
                        this.props.endAdornment
                        &&
                        <div className="form-button-button-end-adornment">
                            {
                                this.props.endAdornment
                            }
                        </div>
                    }
                    </a>
                }
            </div>
        )
    }
}