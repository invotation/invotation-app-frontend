import classNames from 'classnames'
import React from 'react'
import './form-input.scss'

/**
 * The props
 */
interface FormInputProps {
    /**
     * The value for the form input
     */
    value: string | number

    /**
     * The type of the input
     */
    type?: 'text' | 'password' | 'email' | 'number'

    /**
     * Whether or not the input is disabled
     */
    disabled?: boolean

    /**
     * The placeholder
     */
    placeholder?: string

    /**
     * Whenever the value changes
     */
    onChange: (value: string) => void

    /**
     * Whenever the form input blurs
     */
    onBlur?: () => void

    /**
     * Whenever the user presses the enter key
     */
    onEnter?: () => void

    /**
     * The start adornment
     */
    startAdornment?: JSX.Element

    /**
     * The end adornment
     */
    endAdornment?: JSX.Element

    /**
     * Whether or not to not ignore lastpass
     */
    useLastPass?: boolean
}

/**
 * The state
 */
interface FormInputState {
    
    /**
     * Whether or not the element has focus
     */
    focus: boolean
}

/**
 * An input for a form.
 * @author Stan Hurks
 */
export default class FormInput extends React.Component<FormInputProps, FormInputState> {
    constructor(props: any) {
        super(props)

        this.state = {
            focus: false
        }
    }

    public render = () => {
        return (
            <div className={classNames({
                'form-input': true,
                'focus': this.state.focus,
                'start-adornment': this.props.startAdornment,
                'end-adornment': this.props.endAdornment,
                'disabled': this.props.disabled
            })}>
                {
                    this.props.startAdornment
                    &&
                    <div className="form-input-start-adornment">
                        {
                            this.props.startAdornment
                        }
                    </div>
                }
                <input
                    value={this.props.value}
                    type={this.props.type || 'text'}
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    className="form-input-input"
                    onChange={(event) => {
                        this.props.onChange(event.target.value)
                    }} 
                    onKeyUp={(event) => {
                        if (event.key === 'Enter' && this.props.onEnter) {
                            this.props.onEnter()
                        }
                    }}
                    onFocus={() => {
                        this.setState({
                            focus: true
                        })
                    }}
                    onBlur={() => {
                        this.setState({
                            focus: false
                        })
                        if (this.props.onBlur) {
                            this.props.onBlur()
                        }
                    }}
                    data-lpignore={
                        this.props.useLastPass
                        ? 'false'
                        : 'true'
                    }
                    />
                {
                    this.props.endAdornment
                    &&
                    <div className="form-input-end-adornment">
                        {
                            this.props.endAdornment
                        }
                    </div>
                }
            </div>
        )
    }
}