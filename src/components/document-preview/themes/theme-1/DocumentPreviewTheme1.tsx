import React, { CSSProperties } from 'react'
import { BusinessBase } from '../../../../backend/entities/BusinessBase'
import { ClientBase } from '../../../../backend/entities/ClientBase'
import { DocumentPreviewDocument, DocumentPreviewProps } from '../../DocumentPreview'
import './document-preview-theme-1.scss'

/**
 * The props
 */
interface DocumentPreviewTheme1Props {
    items: Array<{
        description: string
        quantity: number
        unit: string
        price: number
        vat: number
    }>

    /**
     * The business
     */
    business: BusinessBase

    /**
     * The client
     */
    client: ClientBase

    /**
     * The document data
     */
    document: DocumentPreviewDocument
}

/**
 * A theme for the document preview.
 * @author Stan Hurks
 */
export default class DocumentPreviewTheme1 extends React.Component<DocumentPreviewTheme1Props & DocumentPreviewProps> {
    /**
     * The styles for the table cells by index
     */
    private tableCellStyles: Array<CSSProperties> = [
        {
            width: 200,
            textAlign: 'left'
        },
        {
            width: 70,
            textAlign: 'left'
        },
        {
            width: 70,
            textAlign: 'left'
        },
        {
            width: 125,
            textAlign: 'left'
        },
        {
            width: 70,
            textAlign: 'left'
        },
        {
            width: 'auto',
            textAlign: 'right'
        }
    ]

    /**
     * The styles for the total table cells
     */
    private totalTableCellStyles: Array<CSSProperties> = [
        {
            width: '50%',
            fontWeight: 500,
            textAlign: 'left'
        },
        {
            width: '50%',
            textAlign: 'right'
        }
    ]
    
    public render = () => {
        return (
            <div className="document-preview-theme-1">
                <div className="document-preview-theme-1-top" style={{
                    background: this.props.primaryColor,
                    color: this.props.primaryTextColor
                }}>
                    <div className="document-preview-theme-1-top-table" style={{
                        width: '100%'
                    }}>
                        <div className="document-preview-theme-1-top-table-left" style={{
                            width: 'auto'
                        }}>
                            {
                                this.props.document.logoUrl
                                &&
                                <div className="document-preview-theme-1-top-table-left-logo">
                                    <img className="img" src={this.props.document.logoUrl} alt="Logo" />
                                </div>
                            }
                            <div className="document-preview-theme-1-top-table-left-details" style={{
                                ...!this.props.document.logoUrl && {
                                    marginTop: 0
                                }
                            }}>
                                {this.props.client.name}<br/><br/>
                                {
                                    this.props.client.street1
                                    &&
                                    <>
                                        {this.props.client.street1}<br/>
                                    </>
                                }
                                {
                                    this.props.client.street2
                                    &&
                                    <>
                                        {this.props.client.street2}<br/>
                                    </>
                                }
                                {
                                    this.props.client.zipCode && this.props.client.city
                                    &&
                                    <>
                                        {this.props.client.zipCode} {this.props.client.city}<br/>
                                    </>
                                }
                                {
                                    this.props.client.provinceState
                                    &&
                                    <>
                                        {this.props.client.provinceState}<br/>
                                    </>
                                }
                                {this.props.client.country}<br/>
                                <br/>
                                Invoice number: {this.props.document.number}<br/>
                                Invoice date: {this.props.document.documentDate}<br/>
                                Due date: {this.props.document.dueDate}
                            </div>
                        </div>
                        <div className="document-preview-theme-1-top-table-right" style={{
                            width: 300
                        }}>
                            <div className="document-preview-theme-1-top-table-right-title">
                                INVOICE
                            </div>
                            <div className="document-preview-theme-1-top-table-right-details">
                                {this.props.business.name}<br/>
                                <br/>
                                {
                                    this.props.business.street1
                                    &&
                                    <>
                                        {this.props.business.street1}<br/>
                                    </>
                                }
                                {
                                    this.props.business.street2
                                    &&
                                    <>
                                        {this.props.business.street2}<br/>
                                    </>
                                }
                                {
                                    this.props.business.zipCode && this.props.business.city
                                    &&
                                    <>
                                        {this.props.business.zipCode} {this.props.business.city}<br/>
                                    </>
                                }
                                {
                                    this.props.business.provinceState
                                    &&
                                    <>
                                        {this.props.business.provinceState}<br/>
                                    </>
                                }
                                {this.props.business.country}<br/>
                                {
                                    (this.props.business.phone || this.props.business.email || this.props.business.website)
                                    &&
                                    <>
                                        <br/>
                                        {
                                            this.props.business.phone
                                            &&
                                            <>
                                                {this.props.business.phone}<br/>
                                            </>
                                        }
                                        {
                                            this.props.business.email
                                            &&
                                            <>
                                                {this.props.business.email}<br/>
                                            </>
                                        }
                                        {
                                            this.props.business.website
                                            &&
                                            <>
                                                {this.props.business.website}<br/>
                                            </>
                                        }
                                    </>
                                }
                                {
                                    (this.props.business.cocNumber || this.props.business.vatNumber)
                                    &&
                                    <>
                                        <br/>
                                        {
                                            this.props.business.cocNumber
                                            &&
                                            <>
                                                CoC: {this.props.business.cocNumber}<br/>
                                            </>
                                        }
                                        {
                                            this.props.business.vatNumber
                                            &&
                                            <>
                                                VAT: {this.props.business.vatNumber}<br/>
                                            </>
                                        }
                                    </>
                                }
                                {
                                    (this.props.business.bankCode || this.props.business.accountNumber)
                                    &&
                                    <>
                                        <br/>
                                        {
                                            (this.props.business.accountNumber)
                                            &&
                                            <>
                                                {this.props.business.accountNumber}<br/>
                                            </>
                                        }
                                        {
                                            (this.props.business.bankCode)
                                            &&
                                            <>
                                                {this.props.business.bankCode}
                                            </>
                                        }
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="document-preview-theme-1-bottom">
                    <div className="document-preview-theme-1-bottom-table" style={{
                        width: '100%'
                    }}>
                        <div className="document-preview-theme-1-bottom-table-row-top" style={{
                            background: this.props.darkColor,
                            color: this.props.darkTextColor
                        }}>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[0]}>
                                Description
                            </div>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[1]}>
                                Quantity
                            </div>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[2]}>
                                Unit
                            </div>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[3]}>
                                Price
                            </div>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[4]}>
                                VAT
                            </div>
                            <div className="document-preview-theme-1-bottom-table-row-top-cell" style={this.tableCellStyles[5]}>
                                Total
                            </div>
                        </div>
                        {
                            this.props.items.map((item, index) => (
                                <div className="document-preview-theme-1-bottom-table-row" key={index}>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[0]}>
                                        {item.description}
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[1]}>
                                        {item.quantity}
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[2]}>
                                        {item.unit}
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[3]}>
                                        $ {item.price.toFixed(2)}
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[4]}>
                                        {item.vat.toFixed(1)}%
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-row-cell" style={this.tableCellStyles[5]}>
                                        $ {(item.price + item.price * (item.vat / 100)).toFixed(2)}
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                    <div className="document-preview-theme-1-bottom-table-total">
                        <div className="document-preview-theme-1-bottom-table-total-left" style={{width: '50%'}}>
                            {
                                this.props.document.signatureUrl
                                &&
                                <img className="img" src={this.props.document.signatureUrl} alt="Logo" />
                            }
                        </div>
                        <div className="document-preview-theme-1-bottom-table-total-right" style={{width: '50%'}}>
                            <div className="document-preview-theme-1-bottom-table-total-right-top" style={{
                                background: this.props.darkColor,
                                color: this.props.darkTextColor
                            }}>
                                Cumulatives
                            </div>
                            <div className="document-preview-theme-1-bottom-table-total-right-table" style={{width: '100%'}}>
                                <div className="document-preview-theme-1-bottom-table-total-right-table-row">
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[0]}>
                                        Subtotal
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[1]}>
                                        $ {
                                            this.props.items.reduce((x, y) => x + y.price, 0).toFixed(2)
                                        }
                                    </div>
                                </div>
                                <div className="document-preview-theme-1-bottom-table-total-right-table-row">
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[0]}>
                                        VAT
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[1]}>
                                        $ {
                                            this.props.items.reduce((x, y) => x + (y.price * y.vat / 100), 0).toFixed(2)
                                        }
                                    </div>
                                </div>
                                <div className="document-preview-theme-1-bottom-table-total-right-table-row">
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[0]}>
                                        Total
                                    </div>
                                    <div className="document-preview-theme-1-bottom-table-total-right-table-row-cell" style={this.totalTableCellStyles[1]}>
                                        $ {
                                            this.props.items.reduce((x, y) => x + y.price + (y.vat / 100 * y.price), 0).toFixed(2)
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="document-preview-theme-1-bottom-table-total-right-pay">
                                <a className="document-preview-theme-1-bottom-table-total-right-pay-button" style={{
                                    background: this.props.primaryColor,
                                    color: this.props.primaryTextColor
                                }} href="">Pay now</a>
                            </div>
                        </div>
                    </div>
                    <div className="document-preview-theme-1-bottom-pay-to" style={{
                        background: this.props.lightColor,
                        color: this.props.lightTextColor
                    }}>
                        This invoice needs to be paid within 7 days to the bank account listed at the top.
                    </div>
                </div>
            </div>
        )
    }
}