import React from 'react'
import { BusinessBase } from '../../backend/entities/BusinessBase'
import { ClientBase } from '../../backend/entities/ClientBase'
import './document-preview.scss'
import DocumentPreviewTheme1 from './themes/theme-1/DocumentPreviewTheme1'

/**
 * All misc document information
 */
export interface DocumentPreviewDocument {
    /**
     * The document number
     */
    number: number

    /**
     * The document date
     */
    documentDate: string

    /**
     * The due date
     */
    dueDate: string

    /**
     * The logo URL
     */
    logoUrl: string | null

    /**
     * The signature URL
     */
    signatureUrl: string | null
}

/**
 * The props
 */
export interface DocumentPreviewProps {
    /**
     * The primary color
     */
    primaryColor: string

    /**
     * Text color on primary
     */
    primaryTextColor: string
 
    /**
     * The dark color
     */
    darkColor: string

    /**
     * Text color on dark
     */
    darkTextColor: string
 
    /**
     * The light color
     */
    lightColor: string
 
    /**
     * Text color on light
     */
    lightTextColor: string

    /**
     * The items in the invoice/quotation
     */
    items?: Array<{
        /**
         * The description
         */
        description: string

        /**
         * The quantity
         */
        quantity: number

        /**
         * The unit
         */
        unit: string
        
        /**
         * The price excl. VAT
         */
        price: number

        /**
         * The VAT percentage
         */
        vat: number
    }>

    /**
     * The business
     */
    business?: BusinessBase

    /**
     * The client
     */
    client?: ClientBase

    /**
     * The document data
     */
    document?: DocumentPreviewDocument

    /**
     * The ID for the template to choose
     */
    templateId: number
}

/**
 * The state
 */
interface DocumentPreviewState {
    /**
     * The scale for the document on the webpage
     */
    scale: number
}

/**
 * The preview for a document.
 * @author Stan Hurks
 */
export default class DocumentPreview extends React.Component<DocumentPreviewProps, DocumentPreviewState> {
    /**
     * The dummy items in the document
     */
    public static readonly dummyItems = [
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        },
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        },
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        },
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        },
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        },
        {
            description: 'PC screen',
            quantity: 1,
            unit: 'pc',
            price: 165.25,
            vat: 21
        }
    ]

    /**
     * The dummy business
     */
    public static readonly dummyBusiness: BusinessBase = {
        name: 'Test B.V.',
        country: 'Nederland',
        street1: 'Doctorskampstraat 2A',
        street2: 'Unit 2738',
        zipCode: '5222AM',
        city: '\'s-Hertogenbosch',
        provinceState: 'Noord-Brabant',
        cocNumber: '328932892',
        vatNumber: 'NL2909302390B01',
        email: 'john@doe.com',
        phone: '+31625328757',
        website: 'https://johndoe.com',
        bankCode: 'RABONL2U',
        accountNumber: 'NL07RABO0304872356'
    } as any

    /**
     * The dummy client
     */
    public static readonly dummyClient: ClientBase = {
        name: 'John Doe',
        email: 'john@doe.com',
        country: 'Nederland',
        street1: 'Teststraat 61',
        street2: 'Unit 2389',
        zipCode: '4323AA',
        city: 'Waalwijk',
        provinceState: 'Noord-Brabant'
    } as any

    /**
     * The dummy document data
     */
    public static readonly dummyDocument: DocumentPreviewDocument = {
        number: 232,
        documentDate: '12/3/2021',
        dueDate: '19/3/2021',
        logoUrl: null,
        signatureUrl: null
    }

    /**
     * The reference to the root element
     */
    private rootRef: React.RefObject<HTMLDivElement> = React.createRef()
    
    /**
     * The ref to the PDF container
     */
    private pdfRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            scale: 0
        }
    }

    public componentDidMount = () => {
        window.addEventListener('resize', this.calculateScale)
        this.calculateScale()
        this.generateHTMLAndCSS()
    }

    public componentDidUpdate = () => {
        this.calculateScale()
    }

    public componentWillUnmount = () => {
        window.removeEventListener('resize', this.calculateScale)
    }

    public render = () => {
        return (
            <div className="document-preview" ref={this.rootRef}>
                <div className="document-preview-container" style={{
                    zoom: this.state.scale,
                    
                    ...this.state.scale === 0 && {
                        opacity: 0
                    }
                }}>
                    <div className="document-preview-container-pdf" ref={this.pdfRef}>
                        {
                            (
                                !this.props.templateId
                                ||
                                this.props.templateId === 1
                                ||
                                this.props.templateId < 1
                                ||
                                this.props.templateId > 1
                            )
                            &&
                            <DocumentPreviewTheme1 {...this.props}
                                items={this.props.items || DocumentPreview.dummyItems}
                                business={this.props.business || DocumentPreview.dummyBusiness}
                                client={this.props.client || DocumentPreview.dummyClient}
                                document={this.props.document || DocumentPreview.dummyDocument} />
                        }
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Calculates the scale
     */
    private calculateScale = () => {
        const calculate = () => {
            if (!this.rootRef.current) {
                return
            }
            const el = this.rootRef.current
            const nonScaledWidth = 747
            const scaledWidth = el.offsetWidth
            this.setState({
                scale: Math.min(1, scaledWidth / nonScaledWidth)
            })
        }
        setTimeout(() => {
            calculate()
        }, 300)
    }

    /**
     * Generates HTML and CSS
     */
    private generateHTMLAndCSS = () => {
        if (!this.pdfRef.current) {
            return
        }
        
        // Get the default styles
        const div = document.createElement('div')
        div.innerHTML = 'Test'
        document.body.appendChild(div)
        const defaultComputedStyle = getComputedStyle(div)

        // Create a default style map
        const defaultStyle: {[key: string]: string} = {}
        for (let i = 0; i < defaultComputedStyle.length; i++) {
            const propertyName = defaultComputedStyle.item(i)
            const value = defaultComputedStyle.getPropertyValue(propertyName)
            defaultStyle[propertyName] = value
        }

        document.body.removeChild(div)

        // Get all styles by comparing all CSS rules to the default styles and storing the difference.
        const allStyles: {[selector: string]: {[cssPropertyName: string]: string}} = {}
        const generateStyleForElement = (element: Element): {[key: string]: string} => {
            const computedStyle = getComputedStyle(element)
            const style: {[key: string]: string} = {}
            const ignoreProperties = [
                'block-size',
                'caret-color',
                'column-rule-color',
                'inline-size',
                'outline-color',
                'perspective-origin',
                'transform-origin',
                '-webkit-text-emphasis-color',
                '-webkit-text-fill-color',
                '-webkit-text-stroke-color'
            ]
            const borderProperties = [
                'border-block-end-width',
                'border-block-start-width',
                'border-bottom-width',
                'border-inline-end-width',
                'border-inline-start-width',
                'border-left-width',
                'border-right-width',
                'border-top-width'
            ]

            for (let i = 0; i < computedStyle.length; i++) {
                const propertyName = computedStyle.item(i)
                const value = computedStyle.getPropertyValue(propertyName)
                if (defaultStyle[propertyName] === value) {
                    continue
                }
                if (ignoreProperties.includes(propertyName)) {
                    continue
                }
                if (propertyName === 'height' || propertyName === 'width') {
                    continue
                }
                if (propertyName.startsWith('border') && propertyName !== 'border-radius') {
                    let found = false
                    for (const borderProperty of borderProperties) {
                        if (computedStyle.getPropertyValue(borderProperty) !== '0px') {
                            found = true
                            break
                        }
                    }
                    if (!found) {
                        continue
                    }
                }
                if (element.classList.contains('document-preview-container-pdf') && propertyName.startsWith('padding')) {
                    continue
                }
                style[propertyName] = value
            }

            return style
        }

        // Loop through all child elements of the pdf container
        const uglifiedSelectors: {[selector: string]: string} = {}
        const loopThroughParentAndChildElementsAndStoreStyles = (rootElement: Element, selectorPrefix: string|null) => {
            let selector = selectorPrefix === null ? '' : selectorPrefix + ' >'
            for (let i = 0; i < rootElement.classList.length; i++) {
                let className = rootElement.classList.item(i)
                if (className === null) {
                    continue
                }
                let uglifiedClassName = Object.keys(uglifiedSelectors).includes(className)
                    ? uglifiedSelectors[className]
                    : 'a' + Object.keys(uglifiedSelectors).length
                if (!Object.keys(uglifiedSelectors).includes(className)) {
                    uglifiedSelectors[className] = uglifiedClassName
                }
                selector += `.${uglifiedClassName}`
            }

            allStyles[selector] = generateStyleForElement(rootElement)
            
            for (let i = 0; i < rootElement.children.length; i ++) {
                const child = rootElement.children.item(i)
                if (child !== null) {
                    loopThroughParentAndChildElementsAndStoreStyles(child, selector)
                }
            }
        }
        loopThroughParentAndChildElementsAndStoreStyles(this.pdfRef.current, null)

        // Convert the all styles object to CSS
        let css = '<style>\n'
        css += '\t* {\n'
        css += '\t\tmargin: 0;\n'
        css += '\t\tpadding: 0;\n'
        css += '\t\tfont-family: Roboto, sans-serif;\n'
        css += '\t}\n'

        for (const selector of Object.keys(allStyles)) {
            css += '\t' + selector + ' {\n'
            for (const cssProperty of Object.keys(allStyles[selector])) {
                css += `\t\t${cssProperty}: ${allStyles[selector][cssProperty]};\n`
            }
            css += '\t}\n'
        }
        css += '</style>'

        // Generate the HTML
        let html = '<link rel="preconnect" href="https://fonts.gstatic.com">\n'
            + '<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">\n'
            + this.pdfRef.current.outerHTML

        // Uglify HTML
        for (const selector of Object.keys(uglifiedSelectors)) {
            const uglifiedSelector = uglifiedSelectors[selector]
            html = html.replaceAll(`class="${selector}"`, `class="${uglifiedSelector}"`)
        }

        // The HTML and CSS combined
        const htmlCSS = css + '\n' + html

        console.log(htmlCSS)
    }
}