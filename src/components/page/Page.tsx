import React from 'react'
import './page.scss'

/**
 * The main template for a page.
 * @author Stan Hurks
 */
export default class Page extends React.Component {

    public render = () => {
        return (
            <div className="page">
                {
                    this.props.children
                }
            </div>
        )
    }
}