import { ArrowBack, TransferWithinAStationSharp } from '@material-ui/icons'
import React from 'react'
import App from '../../App'
import Device from '../../core/Device'
import { Route } from '../../enum/Route'
import Translations from '../../translations/Translations'
import FormButton from '../form-button/FormButton'
import './page-title.scss'

/**
 * The props
 */
interface PageTitleProps {
    /**
     * The route
     */
    route: Route

    /**
     * The previous link
     */
    previous?: {
        route: Route
        variables?: {[key: string]: string}
    }

    /**
     * The content on the right
     */
    right?: JSX.Element
}

interface PageTitleState {
    /**
     * Whether or not the client is on mobile
     */
    isMobile: boolean
}

/**
 * The page title
 * @author Stan Hurks
 */
export default class PageTitle extends React.Component<PageTitleProps, PageTitleState> {
    constructor(props: any) {
        super(props)
        this.state = {
            isMobile: window.innerWidth <= Device.mobileResolution
        }
    }

    public componentDidMount = () => {
        window.addEventListener('resize', this.onResize)
    }

    public componentWillUnmount = () => {
        window.removeEventListener('resize', this.onResize)
    }
    
    public render = () => {
        return (
            <div className="page-title">
                <div className="page-title-top">
                    {
                        this.props.previous
                        &&
                        !this.state.isMobile
                        &&
                        <div className="page-title-top-return">
                            <FormButton variant="white" onClick={() => {
                                App.redirect.next(this.props.previous)
                            }}>
                                <ArrowBack />
                            </FormButton>
                        </div>
                    }
                    {
                        !this.state.isMobile
                        &&
                        <div className="page-title-top-title">
                            {
                                Translations.translations.title(this.props.route)
                            }
                        </div>
                    }
                    {
                        this.props.right
                        &&
                        <div className="page-title-top-right">
                            {
                                this.props.right
                            }
                        </div>
                    }
                </div>
                {
                    !this.state.isMobile
                    &&
                    <div className="page-title-bottom">
                        <div className="page-title-bottom-line"></div>
                    </div>
                }
            </div>
        )
    }

    /**
     * Whenever the user resizes
     */
    private onResize = () => {
        this.setState({
            isMobile: window.innerWidth <= Device.mobileResolution
        })
    }
}