import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import GraphQL from '../../backend/GraphQL'
import RadialProgress from '../radial-progress/RadialProgress'
import './loader.scss'

interface LoaderState {
    /**
     * The percentage
     */
    percentage: number

    /**
     * Whether or not the loader is enabled
     */
    enabled: boolean

    /**
     * Whether or not the loader is closing
     */
    closing: boolean

    /**
     * Whether or not the loader is completely hidden
     */
    hidden: boolean
}

/**
 * The loader component, used when API calls are being loaded.
 * @author Stan Hurks
 */
export default class Loader extends React.Component<any, LoaderState> {
    
    /**
     * The animation time in ms.
     */
    private static animationTimeMs: number = 300

    /**
     * Enable the loader
     */
    public static enable: Subject<void> = new Subject()
    private subscriptionEnable: Subscription | null = null

    /**
     * Disable the loader
     */
    public static disable: Subject<void> = new Subject()
    private subscriptionDisable: Subscription | null = null

    /**
     * A subscription to GraphQL's progress percentage subject
     */
    private subscriptionRequestProgressPercentage: Subscription | null = null

    constructor(props: any) {
        super(props)

        this.state = {
            percentage: 0,
            enabled: false,
            closing: false,
            hidden: true
        }
    }

    public componentDidMount = () => {
        this.subscriptionEnable = Loader.enable.subscribe(() => {
            this.setState({
                enabled: true,
                hidden: false
            })
        })
        this.subscriptionDisable = Loader.disable.subscribe(() => {
            this.setState({
                closing: true
            }, () => {
                setTimeout(() => {
                    this.setState({
                        enabled: false,
                        closing: false,
                        hidden: true
                    })
                }, Loader.animationTimeMs)
            })
        })
        this.subscriptionRequestProgressPercentage = GraphQL.requestProgressPercentage.subscribe((percentage) => {
            this.setState({
                percentage
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionEnable?.unsubscribe()
        this.subscriptionDisable?.unsubscribe()
        this.subscriptionRequestProgressPercentage?.unsubscribe()
    }
    
    public render = () => {
        return (
            <div className={classNames({
                'loader': true,
                'show': this.state.enabled && !this.state.closing,
                'hidden': this.state.hidden
            })}>
                <div className={classNames({
                    'loader-content': true,
                    'show': this.state.enabled,
                    'hide': this.state.closing
                })}>
                    <div className="loader-content-inner">
                        <RadialProgress
                            size={100}
                            percentage={this.state.percentage} />
                    </div>
                </div>
            </div>
        )
    }
}