import { Close, MoreHoriz, MoreVert } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import App from '../../App'
import Device from '../../core/Device'
import AbsolutePositioning from '../absolute-positioning/AbsolutePositioning'
import FormButton from '../form-button/FormButton'
import './menu.scss'

/**
 * The props
 */
interface MenuProps {
    /**
     * The variant of the button
     */
    variant?: 'primary' | 'dark' | 'white' | 'error' | 'light'

    /**
     * A label to display
     */
    label?: string

    /**
     * Whether or not the button is disabled
     */
    disabled?: boolean
    
    /**
     * The menu options
     */
    options: Array<{
        label: string
        color?: 'primary' | 'white' | 'dark' | 'error'
        onClick: () => void
        hide?: boolean
    }>
}

/**
 * The state
 */
interface MenuState {
    /**
     * The ID of the menu
     */
    id: number

    /**
     * Whether or not the menu is open
     */
    menuOpen: boolean

    /**
     * Whether the select has been initialized
     */
    initialized: boolean

    /**
     * The previous dimensions
     */
    previousDimensions: {
        width: number
        height: number
    }
}

/**
 * A menu.
 * @author Stan Hurks
 */
export default class Menu extends React.Component<MenuProps, MenuState> {   
    /**
     * Closes all form selects except for the one with the given ID
     */
    public static closeAllButSkipId: Subject<number> = new Subject()
    private subscriptionCloseAllButSkipId: Subscription | null = null 
    
    /**
     * The ref to the element of the container
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            id: AbsolutePositioning.counter ++,
            menuOpen: false,
            initialized: false,
            previousDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }

    public componentDidMount = () => {
        this.subscriptionCloseAllButSkipId = Menu.closeAllButSkipId.subscribe((skipId) => {
            if (this.state.id === skipId) {
                return
            }
            this.setState({
                menuOpen: false,
                initialized: false
            })
        })
        window.addEventListener('resize', this.onResize)
    }

    public componentWillUnmount = () => {
        this.subscriptionCloseAllButSkipId?.unsubscribe()
        window.removeEventListener('resize', this.onResize)
    }
    
    public render = () => {
        return (
            <>
                <div className="menu" ref={this.ref}>
                    <FormButton disabled={this.props.disabled} variant={this.props.variant || 'white'} onClick={() => {
                        this.toggleMenu()
                    }}>
                        {
                            this.props.label
                            &&
                            this.props.label + ' '
                        }
                        {
                            !this.state.menuOpen
                            ? <MoreHoriz />
                            : <Close />
                        }
                    </FormButton>
                </div>
            </>
        )
    }

    /**
     * Render the menu
     */
    private renderMenu = () => {
        return (
            <div className="menu-content">
                {
                    this.props.options.filter((v) => !v.hide).map((option, index) => (
                        <div className={classNames({
                            'menu-content-entry': true,
                            [`color-${option.color || 'white'}`]: true
                        })} key={index} onClick={() => {
                            this.toggleMenu(() => {
                                option.onClick()
                            })
                        }}>
                            {
                                option.label
                            }
                        </div>
                    ))
                }
            </div>
        )
    }

    /**
     * Get the content for the absolute positioning component
     */
    private getAbsolutePositioningContent = () => {
        if (window.innerWidth <= Device.mobileResolution) {
            return (
                <div className={classNames({
                    'menu-mobile-menu': true,
                    'menu-open': this.state.menuOpen
                })}>
                    <div className="menu-mobile-menu-close">
                        <FormButton onClick={() => {
                            this.toggleMenu()
                        }} variant="white">
                            <Close />
                        </FormButton>
                    </div>
                    <div className="menu-mobile-menu-content">
                        {
                            this.renderMenu()
                        }
                    </div>
                </div>
            )
        }
        return (
            <div className={classNames({
                'menu-menu': true,
                'menu-open': this.state.menuOpen,
                'initialized': this.state.initialized
            })}>
                {
                    this.renderMenu()
                }
            </div>
        )
    }

    /**
     * Whenever the client resizes the window
     */
    private onResize = () => {
        const currentlyMobile: boolean = window.innerWidth <= Device.mobileResolution
        const wasMobile: boolean = this.state.previousDimensions.width <= Device.mobileResolution
        if (currentlyMobile !== wasMobile) {
            this.setState({
                menuOpen: false,
                initialized: false
            })
        }
        this.setState({
            previousDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        })
    }

    /**
     * Toggle the menu
     */
    private toggleMenu = (callback?: () => void) => {
        this.setState({
            menuOpen: !this.state.menuOpen,
            initialized: true
        }, () => {
            if (this.state.menuOpen && this.ref.current) {
                AbsolutePositioning.mount.next({
                    id: this.state.id,
                    element: this.ref.current,
                    elementAnchor: 'bottom-right',
                    content: this.getAbsolutePositioningContent()
                })
            } else {
                AbsolutePositioning.unmount.next()
                AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
            }
            if (callback) {
                callback()
            }
        })
    }
}