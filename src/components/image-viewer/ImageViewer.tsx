import { Close } from '@material-ui/icons'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import App from '../../App'
import './image-viewer.scss'

interface ImageViewerState {
    /**
     * The image src
     */
    imageSrc: string | null

    /**
     * Whether or not the image is landscape
     */
    landscape: boolean
}

/**
 * The image viewer
 */
export default class ImageViewer extends React.Component<any, ImageViewerState> {
    /**
     * Sets the image src
     */
    public static setImageSrc: Subject<string> = new Subject()
    private subscriptionSetImageSrc: Subscription | null = null
    
    constructor(props: any) {
        super(props)

        this.state = {
            imageSrc: null,

            landscape: false
        }
    }
    
    public componentDidMount = () => {
        this.subscriptionSetImageSrc = ImageViewer.setImageSrc.subscribe((imageSrc) => {
            this.setState({
                imageSrc
            })
            if (imageSrc === null) {
                App.setBodyScroll.next(false)
            } else {
                App.setBodyScroll.next(true)
            }
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionSetImageSrc?.unsubscribe()
    }

    public render = () => {
        return (
            <div className="image-viewer">
                {
                    this.state.imageSrc
                    &&
                    <div className="image-viewer-content">
                        <div className="image-viewer-content-close" onClick={() => {
                            this.setState({
                                imageSrc: null
                            })
                            App.setBodyScroll.next(false)
                        }}>
                            <Close />
                        </div>

                        <img src={this.state.imageSrc} alt="Foto" draggable={false} onLoad={(event) => {
                            const width = (event.target as any).offsetWidth
                            const height = (event.target as any).offsetHeight
                            this.setState({
                                landscape: width >= height
                            })
                        }} style={{
                            ...this.state.landscape
                            ? {
                                width: '100%',
                                height: 'auto'
                            }
                            : {
                                width: 'auto',
                                height: '100%'
                            }
                        }} />
                    </div>
                }
            </div>
        )
    }
}