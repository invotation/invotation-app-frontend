import classNames from 'classnames'
import React from 'react'
import './form-group.scss'

interface FormGroupProps {
    /**
     * The label for the group
     */
    label: string
    
    /**
     * Whether or not the field is required
     */
    required?: boolean
}

interface FormGroupState {
    /**
     * Whether a menu is open
     */
    openMenu: boolean
}

/**
 * A component used for grouping form components
 * @author Stan Hurks
 */
export default class FormGroup extends React.Component<FormGroupProps, FormGroupState> {
    private contentRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            openMenu: false
        }
    }

    public render = () => {
        return (
            <div className="form-group">
                <div className={classNames({
                    'form-group-label': true,
                    'required': this.props.required
                })}
                onClick={() => {
                    if (!this.contentRef.current) {
                        return
                    }
                    const input = this.contentRef.current.querySelector('input')
                    if (input) {
                        input.focus()
                    }
                }}>
                    {
                        this.props.label
                    }
                    {
                        this.props.required
                        && ' *'
                    }
                </div><div className="form-group-content" ref={this.contentRef}>
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}