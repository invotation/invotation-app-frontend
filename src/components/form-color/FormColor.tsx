import { Close } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import Device from '../../core/Device'
import { Theme } from '../../core/theme/Theme'
import AbsolutePositioning from '../absolute-positioning/AbsolutePositioning'
import ColorPicker from '../color-picker/ColorPicker'
import FormButton from '../form-button/FormButton'
import './form-color.scss'

interface FormColorProps {
    /**
     * A CSS rgb() string of the color in the picker
     */
    color: string

    /**
     * The place to anchor
     */
    elementAnchor?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right'

    /**
     * Whenever the color changes
     */
    onChange: (color: string) => void
}

interface FormColorState {
    /**
     * The ID of the form color
     */
    id: number

    /**
     * Whether the menu is open
     */
    menuOpen: boolean

    /**
     * Whether the menu has been initialized
     */
    menuInitialized: boolean
}

/**
 * A color picker in a form.
 * @author Stan Hurks
 */
export default class FormColor extends React.Component<FormColorProps, FormColorState> {    
    /**
     * Closes all form selects except for the one with the given ID
     */
    public static closeAllButSkipId: Subject<number> = new Subject()
    private subscriptionCloseAllButSkipId: Subscription | null = null 

    /**
     * The animation time in ms
     */
    private animationTimeMs: number = 300

    /**
     * The ref to the element of the container
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            id: AbsolutePositioning.counter ++,
            menuOpen: false,
            menuInitialized: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionCloseAllButSkipId = FormColor.closeAllButSkipId.subscribe((skipId) => {
            if (this.state.id === skipId) {
                return
            }
            this.setState({
                menuOpen: false
            }, () => {
                if (this.state.menuInitialized) {
                    setTimeout(() => {
                        this.setState({
                            menuInitialized: false
                        })
                    }, this.animationTimeMs)
                }
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionCloseAllButSkipId?.unsubscribe()
    }

    public render = () => {
        return (
            <>
                <div className="form-color" ref={this.ref}>
                    <div className={classNames({
                        'form-color-content': true,
                        'initialized': this.state.menuInitialized
                    })} onClick={(event) => {
                        let found: boolean = false
                        const findParent = (root: Element) => {
                            if (root.classList.contains('form-color-content-menu') || root.classList.contains('form-select-menu-mobile')) {
                                found = true
                            }
                            if (root.parentElement) {
                                findParent(root.parentElement)
                            }
                        }
                        findParent(event.target as Element)
                        if (found) {
                            return
                        }
                        if (!this.state.menuOpen) {
                            FormColor.closeAllButSkipId.next(this.state.id)
                        }
                        this.setState({
                            menuOpen: !this.state.menuOpen,
                            menuInitialized: true
                        }, () => {
                            if (this.state.menuOpen && this.ref.current) {
                                AbsolutePositioning.mount.next({
                                    id: this.state.id,
                                    element: this.ref.current,
                                    elementAnchor: this.props.elementAnchor || 'bottom-right',
                                    content: this.getAbsolutePositioningContent()
                                })
                            } else {
                                AbsolutePositioning.unmount.next()
                                AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                setTimeout(() => {
                                    this.setState({
                                        menuInitialized: false
                                    }, () => {
                                        AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                    })
                                }, this.animationTimeMs)
                            }
                        })
                    }}>
                        <div className="form-color-content-left" style={{
                            background: this.props.color
                        }}>
                        </div>
                        <div className="form-color-content-right">
                            {
                                this.props.color
                            }
                        </div>
                        
                    </div>
                </div>
            </>
        )
    }

    private renderColorMenu = () => {
        return (
            <div className="form-color-menu-content">
                <ColorPicker initialColor={Theme.serializeToColor(this.props.color)} onChange={(color) => {
                    this.props.onChange(`rgb(${color.r}, ${color.g}, ${color.b})`)
                }} />

                <div className="form-color-menu-content-right">
                    <FormButton variant="dark" onClick={() => {
                        this.setState({
                            menuOpen: false,
                            menuInitialized: true
                        }, () => {
                            if (!this.state.menuOpen) {
                                AbsolutePositioning.unmount.next()
                                AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                setTimeout(() => {
                                    this.setState({
                                        menuInitialized: false
                                    }, () => {
                                        AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                    })
                                }, this.animationTimeMs)
                            }
                        })
                    }}>
                        <Close />
                    </FormButton>
                </div>
            </div>
        )
    }

    /**
     * Get the content for the absolute positioning context
     */
    private getAbsolutePositioningContent = () => {
        if (window.innerWidth <= Device.mobileResolution) {
            return (
                <div className={classNames({
                    'form-color-menu-mobile': true,
                    'show': this.state.menuOpen,
                    'hide': !this.state.menuOpen,
                    'initialized': this.state.menuInitialized
                })}>
                    <div className="form-color-menu-mobile-background">
                    </div>
                    <div className="form-color-menu-mobile-content">
                        {
                            (this.state.menuOpen || this.state.menuInitialized)
                            &&
                            this.renderColorMenu()
                        }
                    </div>
                </div>
            )
        }
        return (
            <div className={classNames({
                'form-color-content-menu': true,
                'show': this.state.menuOpen,
                'hide': !this.state.menuOpen,
                'initialized': this.state.menuInitialized
            })}>
                {
                    (this.state.menuOpen || this.state.menuInitialized)
                    &&
                    this.renderColorMenu()
                }
            </div>
        )
    }
}