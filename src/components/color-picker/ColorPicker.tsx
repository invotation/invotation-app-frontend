import React from 'react'
import Expressions from '../../core/Expressions'
import { Color } from '../../core/theme/models/Color'
import { Theme } from '../../core/theme/Theme'
import Translations from '../../translations/Translations'
import FormGroup from '../form-group/FormGroup'
import FormInput from '../form-input/FormInput'
import './color-picker.scss'

/**
 * The props
 */
interface ColorPickerProps {
    /**
     * The initial color
     */
    initialColor: Color

    /**
     * On change
     */
    onChange: (color: Color) => void
}

/**
 * The state
 */
interface ColorPickerState {
    /**
     * The color
     */
    color: Color

    /**
     * The background color
     */
    backgroundColor: Color

    /**
     * The rgb colors in the form
     */
    rgb: {
        r: string
        g: string
        b: string
    }

    /**
     * The hex color in the form
     */
    hex: string

    /**
     * The config for the hue slider
     */
    hue: {
        selected: boolean
        coordinates: {
            x: number
        }
    }
 
    /**
     * The config for the colors
     */
    colors: {
        selected: boolean
        coordinates: {
            x: number
            y: number
        }
    }
}

/**
 * Represents a color picker.
 * @author Stan Hurks
 */
export default class ColorPicker extends React.Component<ColorPickerProps, ColorPickerState> {     
    private hueRef: React.RefObject<HTMLDivElement> = React.createRef()
    private colorsRef: React.RefObject<HTMLDivElement> = React.createRef()
    
    constructor(props: any) {
        super(props)

        this.state = {
            color: this.props.initialColor,
            backgroundColor: {
                r: 0,
                g: 0,
                b: 0,
                a: 255
            },
            rgb: {
                r: this.props.initialColor.r.toString(),
                g: this.props.initialColor.g.toString(),
                b: this.props.initialColor.b.toString()
            },
            hex: Theme.convertColorToHex(this.props.initialColor),
            hue: {
                selected: false,
                coordinates: {
                    x: 0
                }
            },
            colors: {
                selected: false,
                coordinates: {
                    x: 0,
                    y: 0
                }
            }
        }
    }

    public componentDidMount = () => {
        document.addEventListener('mousemove', this.onMouseMove)
        document.addEventListener('touchmove', this.onTouchMove)
        document.addEventListener('mouseup', this.onMouseUp)
        document.addEventListener('touchend', this.onMouseUp)
        this.initializeColor(this.props.initialColor)
    }

    public componentWillUnmount = () => {
        document.removeEventListener('mousemove', this.onMouseMove)
        document.removeEventListener('touchmove', this.onTouchMove)
        document.removeEventListener('mouseup', this.onMouseUp)
        document.removeEventListener('touchend', this.onMouseUp)
    }

    public render = () => {
        return (
            <div className="color-picker">
                <div className="color-picker-top">
                    <div className="color-picker-top-gradient" 
                        style={{
                            backgroundColor: `rgb(${this.state.backgroundColor.r}, ${this.state.backgroundColor.g}, ${this.state.backgroundColor.b})`
                        }}
                        ref={this.colorsRef}
                        onMouseDown={(event) => {
                            const {clientX, clientY} = event
                            this.setState({
                                colors: {
                                    ...this.state.colors,
                                    selected: true
                                }
                            })
                            setTimeout(() => {
                                this.performMouseMove(clientX, clientY)
                            })
                        }}
                        onTouchStart={(event) => {
                            if (event.touches.length !== 1) {
                                return
                            }
                            const {clientX, clientY} = event.touches.item(0)
                            this.setState({
                                colors: {
                                    ...this.state.colors,
                                    selected: true
                                }
                            })
                            setTimeout(() => {
                                this.performMouseMove(clientX, clientY)
                            })
                        }}
                        >
                        <div className="color-picker-top-gradient-background-1"></div>
                        <div className="color-picker-top-gradient-background-2"></div>
                        <div className="color-picker-top-gradient-picker" style={{
                                top: `${this.state.colors.coordinates.y * 100}%`,
                                left: `${this.state.colors.coordinates.x * 100}%`,
                                backgroundColor: `rgb(${this.state.color.r}, ${this.state.color.g}, ${this.state.color.b})`
                            }}
                            onMouseDown={() => this.setState({
                                colors: {
                                    ...this.state.colors,
                                    selected: true
                                }
                            })}
                            />
                    </div>
                </div><div className="color-picker-bottom">
                    <div className="color-picker-bottom-gradient"
                        ref={this.hueRef}
                        onMouseDown={(event) => {
                            const {clientX, clientY} = event
                            this.setState({
                                hue: {
                                    ...this.state.hue,
                                    selected: true
                                }
                            })
                            setTimeout(() => {
                                this.performMouseMove(clientX, clientY)
                            })
                        }}
                        onTouchStart={(event) => {
                            if (event.touches.length !== 1) {
                                return
                            }
                            const {clientX, clientY} = event.touches.item(0)
                            this.setState({
                                hue: {
                                    ...this.state.hue,
                                    selected: true
                                }
                            })
                            setTimeout(() => {
                                this.performMouseMove(clientX, clientY)
                            })
                        }}>
                        <div className="color-picker-bottom-gradient-picker"
                            style={{
                                left: `${this.state.hue.coordinates.x * 100}%`,
                                backgroundColor: `rgb(${this.state.backgroundColor.r}, ${this.state.backgroundColor.g}, ${this.state.backgroundColor.b})`
                            }}
                            onMouseDown={() => this.setState({
                                hue: {
                                    ...this.state.hue,
                                    selected: true
                                }
                            })}></div>
                    </div>
                    <div className="color-picker-bottom-fields">
                        <div className="color-picker-bottom-fields-top">
                            <div className="color-picker-bottom-fields-top-color">
                                <FormGroup label={Translations.translations.components.colorPicker.rgb.r}>
                                    <FormInput value={this.state.rgb.r} onChange={(color) => {
                                        if (!/^[0-9]{0,3}$/g.test(color)) {
                                            return
                                        }
                                        this.setState({
                                            rgb: {
                                                ...this.state.rgb,
                                                r: color.length
                                                    ? Math.max(0, Math.min(255, Number(color))).toString()
                                                    : color
                                            },
                                            color: {
                                                ...this.state.color,
                                                r: Math.max(0, Math.min(255, Number(color)))
                                            }
                                        }, () => {
                                            this.initializeColor(this.state.color)
                                        })
                                    }} />
                                </FormGroup>
                            </div>
                            <div className="color-picker-bottom-fields-top-color">
                                <FormGroup label={Translations.translations.components.colorPicker.rgb.g}>
                                    <FormInput value={this.state.rgb.g} onChange={(color) => {
                                        if (!/^[0-9]{0,3}$/g.test(color)) {
                                            return
                                        }
                                        this.setState({
                                            rgb: {
                                                ...this.state.rgb,
                                                g: color.length
                                                    ? Math.max(0, Math.min(255, Number(color))).toString()
                                                    : color
                                            },
                                            color: {
                                                ...this.state.color,
                                                g: Math.max(0, Math.min(255, Number(color)))
                                            }
                                        }, () => {
                                            this.initializeColor(this.state.color)
                                        })
                                    }} />
                                </FormGroup>
                            </div>
                            <div className="color-picker-bottom-fields-top-color">
                                <FormGroup label={Translations.translations.components.colorPicker.rgb.b}>
                                    <FormInput value={this.state.rgb.b} onChange={(color) => {
                                        if (!/^[0-9]{0,3}$/g.test(color)) {
                                            return
                                        }
                                        this.setState({
                                            rgb: {
                                                ...this.state.rgb,
                                                b: color.length
                                                    ? Math.max(0, Math.min(255, Number(color))).toString()
                                                    : color
                                            },
                                            color: {
                                                ...this.state.color,
                                                b: Math.max(0, Math.min(255, Number(color)))
                                            }
                                        }, () => {
                                            this.initializeColor(this.state.color)
                                        })
                                    }} />
                                </FormGroup>
                            </div>
                        </div>
                        <div className="color-picker-bottom-fields-bottom">
                            <FormGroup label={Translations.translations.components.colorPicker.hex}>
                                <FormInput value={this.state.hex} onChange={(hex) => {
                                    let stateHex = hex.toUpperCase()
                                    if (hex.length <= 1) {
                                        stateHex = '#'
                                    }
                                    else if (hex.length > 7) {
                                        stateHex = stateHex.substring(0, 7)
                                    } 
                                    else {
                                        let hexSubstring = stateHex.substring(1)
                                        if (!/^([0-9]|[a-f]|[A-F]){0,6}$/g.test(hexSubstring)) {
                                            return
                                        }
                                    }
                                    this.setState({
                                        hex: stateHex
                                    }, () => {
                                        if (Expressions.hexColor.test(this.state.hex)) {
                                            this.initializeColor(Theme.convertHexToColor(this.state.hex), true)
                                        }
                                    })
                                }} />
                            </FormGroup>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    private onMouseUp = () => {
        this.setState({
            hue: {
                ...this.state.hue,
                selected: false
            },
            colors: {
                ...this.state.colors,
                selected: false
            }
        })
    }

    private onMouseMove = (event: MouseEvent) => {
        this.performMouseMove(event.clientX, event.clientY)
    }

    private onTouchMove = (event: TouchEvent) => {
        if (event.touches.length !== 1) {
            return
        }
        const {clientX, clientY} = event.touches.item(0)!
        this.performMouseMove(clientX, clientY)
    }

    private performMouseMove = (clientX: number, clientY: number) => {
        if (!this.state.hue.selected && !this.state.colors.selected) return
        if (!this.hueRef.current || !this.colorsRef.current) return

        if (this.state.hue.selected) {
            const element = this.hueRef.current
            const startPosition = element.getBoundingClientRect().left
            const mousePosition = clientX
            const deltaPosition = mousePosition - startPosition
            const deltaCoordinate = deltaPosition / element.getBoundingClientRect().width

            this.setState({
                hue: {
                    ...this.state.hue,
                    coordinates: {
                        ...this.state.hue.coordinates,
                        x: Math.min(1, Math.max(0, deltaCoordinate))
                    }
                }
            })
        } else if (this.state.colors.selected) {
            const element = this.colorsRef.current
            const startPosition = {
                x: element.getBoundingClientRect().left,
                y: element.getBoundingClientRect().top
            }
            const mousePosition = {
                x: clientX,
                y: clientY
            }
            const deltaPosition = {
                x: mousePosition.x - startPosition.x,
                y: mousePosition.y - startPosition.y
            }
            const deltaCoordinate = {
                x: deltaPosition.x / element.getBoundingClientRect().width,
                y: deltaPosition.y / element.getBoundingClientRect().height
            }

            this.setState({
                colors: {
                    ...this.state.colors,
                    coordinates: {
                        ...this.state.colors.coordinates,
                        x: Math.min(1, Math.max(0, deltaCoordinate.x)),
                        y: Math.min(1, Math.max(0, deltaCoordinate.y))
                    }
                }
            })
        }

        this.setState({
            backgroundColor: this.getHueColor(),
            color: this.getColor(),
            rgb: {
                r: this.getColor().r.toString(),
                g: this.getColor().g.toString(),
                b: this.getColor().b.toString()
            },
            hex: Theme.convertColorToHex(this.getColor())
        }, () => {
            if (this.props.onChange) {
                this.props.onChange(this.state.color)
            }
        })
    }

    /**
     * Get the hue color
     */
    private getHueColor = (): Color => {
        const colorMatrix = [
            [255, 0, 0],
            [255, 255, 0],
            [0, 255, 0],
            [0, 255, 255],
            [0, 0, 255],
            [255, 0, 255],
            [255, 0, 0]
        ]
        const percentage = this.state.hue.coordinates.x * 100
        const firstIndex = Math.floor(percentage / (100 / (colorMatrix.length - 1)))
        const pastFirstIndex = percentage / (100 / (colorMatrix.length - 1)) - firstIndex
        const amountFirstColor = 1 - pastFirstIndex
        const lastIndex = firstIndex === colorMatrix.length - 1
            ? colorMatrix.length - 1
            : firstIndex + 1

        const colorA = colorMatrix[firstIndex]
        const colorB = colorMatrix[lastIndex]

        if (amountFirstColor === 1) {
            return {
                r: colorA[0],
                g: colorA[1],
                b: colorA[2],
                a: 255
            }
        }

        return {
            r: (colorA[0] * amountFirstColor + colorB[0] * pastFirstIndex),
            g: (colorA[1] * amountFirstColor + colorB[1] * pastFirstIndex),
            b: (colorA[2] * amountFirstColor + colorB[2] * pastFirstIndex),
            a: 255
        }
    }

    /**
     * Get the color
     */
    private getColor = (): Color => {
        const base = this.getHueColor()

        const intensity = this.state.colors.coordinates.x
        const baseWithIntensity: Color = {
            r: base.r * intensity + 255 * (1 - intensity),
            g: base.g * intensity + 255 * (1 - intensity),
            b: base.b * intensity + 255 * (1 - intensity),
            a: 255
        }

        const fadeToBlackFactor = this.state.colors.coordinates.y
        const color: Color = {
            r: baseWithIntensity.r * (1 - fadeToBlackFactor),
            g: baseWithIntensity.g * (1 - fadeToBlackFactor),
            b: baseWithIntensity.b * (1 - fadeToBlackFactor),
            a: 255
        }

        return {
            r: Math.round(color.r),
            g: Math.round(color.g),
            b: Math.round(color.b),
            a: Math.round(color.a)
        }
    }

    /**
     * Initialize the color
     */
    private initializeColor = (color: Color, fromHex?: boolean) => {
        const hsv = Theme.convertColorToHSV(color)

        this.setState({
            hue: {
                ...this.state.hue,
                coordinates: {
                    ...this.state.hue.coordinates,
                    x: hsv.h / 360
                }
            },
            colors: {
                ...this.state.colors,
                coordinates: {
                    ...this.state.colors.coordinates,
                    x: hsv.s / 100,
                    y: 1 - (hsv.v / 100)
                }
            },
            hex: fromHex 
                ? this.state.hex
                : Theme.convertColorToHex(color)
        }, () => {
            this.setState({
                color: this.getColor(),
                backgroundColor: this.getHueColor()
            }, () => {
                this.props.onChange(this.state.color)
            })
        })
    }
}