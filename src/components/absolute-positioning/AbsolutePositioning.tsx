import classNames from 'classnames'
import React from 'react'
import { Subject, Subscription } from 'rxjs'
import App from '../../App'
import Device from '../../core/Device'
import FormColor from '../form-color/FormColor'
import FormSelect from '../form-select/FormSelect'
import Menu from '../menu/Menu'
import './absolute-positioning.scss'

/**
 * The options for RxJS
 */
interface AbsolutePositioningOptions {
    /**
     * The ID of the attached component
     */
    id: number

    /**
     * The element to follow
     */
    element: Element

    /**
     * The place to anchor
     */
    elementAnchor: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right'

    /**
     * The content to display in absolute positioning
     */
    content: JSX.Element
}

/**
 * The state
 */
interface AbsolutePositioningState {
    /**
     * The element to render
     */
    options: AbsolutePositioningOptions | null

    /**
     * The position of the element
     */
    position: {
        top: number
        left: number
    } | null

    /**
     * Whether or not the content is closing
     */
    closing: boolean

    /**
     * Whether or not the mobile view is enabled
     */
    isMobile: boolean

    /**
     * Whether or not this is the first calculation for the element
     */
    firstCalculation: boolean
}

/**
 * A component to enable absolute positioning without worrying about the z-index.
 * @author Stan Hurks
 */
export default class AbsolutePositioning extends React.Component<any, AbsolutePositioningState> {
    /**
     * The counter for ID's
     */
    public static counter: number = 0
    
    /**
     * The time of animation
     */
    private static animationTimeMs: number = 300
    
    /**
     * Mount an element in the component
     */
    public static mount: Subject<AbsolutePositioningOptions> = new Subject()
    private subscriptionMount: Subscription | null = null
    
    /**
     * Update the content
     */
    public static updateContent: Subject<JSX.Element> = new Subject()
    private subscriptionUpdateContent: Subscription | null = null

    /**
     * Unmount the current element in the component
     */
    public static unmount: Subject<void> = new Subject()
    private subscriptionUnmount: Subscription | null = null

    /**
     * The ref to the container
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)
        this.state = {
            options: null,
            position: null,
            closing: false,
            isMobile: window.innerWidth <= Device.mobileResolution,
            firstCalculation: true
        }
    }

    public componentDidMount = () => {
        this.subscriptionMount = AbsolutePositioning.mount.subscribe((options) => {
            FormSelect.closeAllButSkipId.next(options.id)
            FormColor.closeAllButSkipId.next(options.id)
            Menu.closeAllButSkipId.next(options.id)
            App.setBodyScroll.next(true)

            this.setState({
                options,
                firstCalculation: true
            }, () => {
                this.calculatePosition()
            })
        })
        this.subscriptionUnmount = AbsolutePositioning.unmount.subscribe(() => {
            App.setBodyScroll.next(false)
            this.closeContent()
        })
        this.subscriptionUpdateContent = AbsolutePositioning.updateContent.subscribe((content) => {
            this.setState({
                options: Object.assign(this.state.options || {}, {
                    content
                }) as any
            }, () => {
                this.calculatePosition()
            })
        })
        window.addEventListener('resize', this.calculatePosition)
    }

    public componentWillUnmount = () => {
        this.subscriptionMount?.unsubscribe()
        this.subscriptionUnmount?.unsubscribe()
        this.subscriptionUpdateContent?.unsubscribe()
        window.removeEventListener('resize', this.calculatePosition)
    }
    
    public render = () => {
        return (
            <>
                {
                    this.state.options
                    &&
                    <div className={classNames({
                        'absolute-positioning': true,
                        'closing': this.state.closing
                    })} style={{
                        ...this.state.isMobile
                        ? {
                            position: 'fixed',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: '100%',
                            zIndex: 10000000
                        }
                        : (
                            this.state.position
                            ? {
                                top: this.state.position.top,
                                left: this.state.position.left
                            }
                            : {
                                top: -10000,
                                left: -10000
                            }
                        )
                    }} ref={this.ref}>
                        {
                            this.state.options.content
                        }
                    </div>
                }
            </>
        )
    }

    /**
     * Calculates the position of the component
     */
    private calculatePosition = () => {
        if (!this.ref.current || !this.state.options || !this.state.options.element) {
            return
        }
        const isMobile = window.innerWidth <= Device.mobileResolution
        if (isMobile !== this.state.isMobile && !this.state.firstCalculation) {
            this.setState({
                isMobile
            }, () => {
                this.closeContent()
            })
            return
        }

        let top = this.state.options.element.getBoundingClientRect().top + window.scrollY
        let left = this.state.options.element.getBoundingClientRect().left

        const contentWidth = (this.ref.current.children[0] as any).offsetWidth
        const contentHeight = (this.ref.current.children[0] as any).offsetHeight

        if (this.state.options.elementAnchor === 'bottom-left') {
            top += this.state.options.element.getBoundingClientRect().height
        } else if (this.state.options.elementAnchor === 'bottom-right') {
            top += this.state.options.element.getBoundingClientRect().height
            left += this.state.options.element.getBoundingClientRect().width
                - contentWidth
        } else if (this.state.options.elementAnchor === 'top-left') {
            top -= contentHeight
        } else if (this.state.options.elementAnchor === 'top-right') {
            top -= contentHeight
            left += this.state.options.element.getBoundingClientRect().width
                - contentWidth
        }

        this.setState({
            position: {
                top,
                left
            },
            isMobile,
            firstCalculation: false
        })
    }

    /**
     * Closes the content
     */
    private closeContent = () => {
        App.setBodyScroll.next(false)
        this.setState({
            closing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    closing: false,
                    options: null,
                    position: null
                })
            }, AbsolutePositioning.animationTimeMs)
        })
    }
}