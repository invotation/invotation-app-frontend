import { Check } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import './form-checkbox.scss'

interface FormCheckboxProps {
    /**
     * The value
     */
    value: boolean

    /**
     * Whenever the value changes
     */
    onChange: (value: boolean) => void
}

/**
 * A checkbox in a form
 * @author Stan Hurks
 */
export default class FormCheckbox extends React.Component<FormCheckboxProps> {

    public render = () => {
        return (
            <div className="form-checkbox">
                <div className={classNames({
                    'form-checkbox-checkbox': true,
                    'checked': this.props.value
                })} onClick={() => {
                    this.props.onChange(!this.props.value)
                }}>
                    {
                        this.props.value
                        &&
                        <Check />
                    }
                </div>
                {
                    this.props.children
                    &&
                    <div className="form-checkbox-label" onClick={(event) => {
                        const element = event.target as Element
                        if (element.tagName === 'A') {
                            return
                        }
                        this.props.onChange(!this.props.value)
                    }}>
                        {
                            this.props.children
                        }
                    </div>
                }
            </div>
        )
    }
}