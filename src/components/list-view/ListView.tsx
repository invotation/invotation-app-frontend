import { ArrowLeft, MoreHoriz, Search } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import FormButton from '../form-button/FormButton'
import FormCheckbox from '../form-checkbox/FormCheckbox'
import FormInput from '../form-input/FormInput'
import './list-view.scss'

interface ListViewProps {
    /**
     * The data
     */
    data: any[]

    /**
     * The labels to display
     */
    columns: Array<{
        /**
         * The property name on the data object
         */
        getValue: (object: any) => string

        /**
         * The property name on the object
         */
        propertyName: string

        /**
         * The CSS width or number in pixels
         */
        width: string|number

        /**
         * The label to display on top
         */
        label: string
    }>

    /**
     * The UUID's to be pinned at the top
     */
    topPinnedUuids?: string[]

    /**
     * Get the actions for the selection
     */
    getActionsForSelection?: (selectionUuids: string[]) => JSX.Element

    /**
     * Get the actions for a UUID
     */
    getActionsForUuid: (uuid: string) => JSX.Element

    /**
     * Whenever the sort changes
     */
    onChangeSort: (propertyName: string, sortDirection: 'asc' | 'desc') => void

    /**
     * Whenever the search changes
     */
    onChangeSearch: (search: string) => void

    /**
     * Whether the rows are selectable
     */
    selectable?: boolean

    /**
     * The pagination
     */
    pagination?: {
        /**
         * The page
         */
        currentPage: number

        /**
         * The last page
         */
        lastPage: number
    
        /**
         * Whenever the page changes
         */
        onChangePage: (pageNumber: number) => void
    }
}

/**
 * The state
 */
interface ListViewState {
    /**
     * The search query
     */
    search: string

    /**
     * The sort index
     */
    sortIndex: number

    /**
     * The sort direction
     */
    sortDirection: 'asc' | 'desc'

    /**
     * The current page
     */
    currentPage: string

    /**
     * The selection of UUIDs
     */
    selection: string[]
}

/**
 * A list view for a given entity.
 * @author Stan Hurks
 */
export default class ListView extends React.Component<ListViewProps, ListViewState> {
    constructor(props: any) {
        super(props)

        this.state = {
            search: '',
            sortIndex: 0,
            sortDirection: 'asc',
            currentPage: props.pagination
                ? props.pagination.currentPage
                : '',
            selection: []
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            this.props.onChangeSort(this.props.columns[this.state.sortIndex].propertyName, this.state.sortDirection)
        })
    }

    public componentDidUpdate = () => {
        if (this.state.selection.filter((v) => !this.props.data.map((u) => u.uuid).includes(v)).length) {
            const selection: string[] = []
            for (const item of this.state.selection) {
                if (this.props.data.map((v) => v.uuid).includes(item)) {
                    selection.push(item)
                }
            }
            this.setState({
                selection
            })
        }
    }
    
    public render = () => {
        return (
            <div className="list-view">
                <div className="list-view-top">
                    <div className="list-view-top-table">
                        <div className="list-view-top-table-left">
                            <div className="list-view-top-table-left-search">
                                <FormInput value={this.state.search} onChange={(search) => {
                                    this.setState({
                                        search
                                    })
                                }} onBlur={() => {
                                    this.props.onChangeSearch(this.state.search)
                                }} endAdornment={(<Search />)} placeholder="Search..." />
                            </div>
                        </div>
                        {
                            this.props.selectable
                            &&
                            this.props.getActionsForSelection
                            &&
                            <div className="list-view-top-table-right">
                                {
                                    this.props.getActionsForSelection(this.state.selection)   
                                }
                            </div>
                        }
                    </div>
                </div>
                <div className="list-view-content">
                    <div className="list-view-content-content" style={{
                        ...!this.props.pagination && {
                            borderRadius: '0 0 6px 6px'
                        }
                    }}>
                        <div className="list-view-content-content-table">
                            <div className="list-view-content-content-table-head">
                                {
                                    this.props.selectable
                                    &&
                                    <div className="list-view-content-content-table-head-column" style={{
                                        width: 30
                                    }}>
                                        <FormCheckbox value={this.areAllColumnsSelected()} onChange={(selectAll) => {
                                            if (selectAll) {
                                                this.setState({
                                                    selection: this.props.data.map((v) => v.uuid)
                                                })
                                            } else {
                                                this.setState({
                                                    selection: []
                                                })
                                            }
                                        }} />
                                    </div>
                                }
                                {
                                    this.props.columns.map((column, i) => (
                                        <div className={classNames({
                                            'list-view-content-content-table-head-column': true,
                                            'selected': i === this.state.sortIndex,
                                            'asc': this.state.sortDirection === 'asc',
                                            'desc': this.state.sortDirection === 'desc'
                                        })} key={column.propertyName} style={{
                                            width: column.width
                                        }} onClick={() => {
                                            if (this.state.sortIndex === i) {
                                                this.setState({
                                                    sortDirection: this.state.sortDirection === 'asc'
                                                        ? 'desc'
                                                        : 'asc',
                                                    currentPage: '1'
                                                })
                                            } else {
                                                this.setState({
                                                    sortIndex: i,
                                                    sortDirection: 'asc',
                                                    currentPage: '1'
                                                })
                                            }
                                            setTimeout(() => {
                                                this.props.onChangeSort(column.propertyName, this.state.sortDirection)
                                            })
                                        }}>
                                            {
                                                column.label
                                            }
                                            {
                                                i === this.state.sortIndex
                                                &&
                                                <ArrowLeft />
                                            }
                                        </div>
                                    ))
                                }
                                <div className="list-view-content-content-table-head-column">
                                </div>
                            </div>
                            {
                                this.props.data
                                .sort((x, y) => {
                                    if (this.props.topPinnedUuids && (this.props.topPinnedUuids.includes(x.uuid) || this.props.topPinnedUuids?.includes(y.uuid))) {
                                        if (this.props.topPinnedUuids.includes(x.uuid) && !this.props.topPinnedUuids.includes(y.uuid)) {
                                            return -1
                                        }
                                        else if (this.props.topPinnedUuids.includes(y.uuid) && !this.props.topPinnedUuids.includes(x.uuid)) {
                                            return 1
                                        }
                                        return 0
                                    }
                                    return 0
                                })
                                .map((object, i) => (
                                    <div className="list-view-content-content-table-content-row" key={i}>
                                        {
                                            this.props.selectable
                                            &&
                                            <div className="list-view-content-content-table-content-row-column">
                                                <FormCheckbox value={this.state.selection.includes(object.uuid)} onChange={(selected) => {
                                                    if (selected) {
                                                        this.setState({
                                                            selection: this.state.selection.concat([object.uuid])
                                                        })
                                                    } else {
                                                        this.setState({
                                                            selection: this.state.selection.filter((v) => v !== object.uuid)
                                                        })
                                                    }
                                                }} />
                                            </div>
                                        }
                                        {
                                            this.props.columns.map((column) => (
                                                <div className="list-view-content-content-table-content-row-column" key={i + column.propertyName} style={{
                                                    width: column.width,
                                                    minWidth: column.width
                                                }}>
                                                    {
                                                        column.getValue(object) || 'N/A'
                                                    }
                                                </div>
                                            ))
                                        }
                                        <div className="list-view-content-content-table-content-row-column">
                                            {
                                                this.props.getActionsForUuid(object.uuid)
                                            }
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className="list-view-content-overlay">
                        <div className="list-view-content-overlay-top">
                        </div>
                        <div className="list-view-content-overlay-bottom">
                            {
                                this.props.data.map((object, i) => (
                                    <div className="list-view-content-overlay-bottom-row" key={i}>
                                        {
                                            this.props.getActionsForUuid(object.uuid)
                                        }
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
                {
                    this.props.pagination
                    &&
                    this.props.pagination.lastPage > 1
                    &&
                    <div className="list-view-pagination">
                        <div className="list-view-pagination-content">
                            {
                                this.props.pagination.currentPage !== 1
                                &&
                                <>
                                    <FormButton onClick={() => {
                                        this.setCurrentPage(1)
                                    }}>
                                        1
                                    </FormButton>
                                    <MoreHoriz />
                                </>
                            }
                            {
                                this.props.pagination.currentPage >= 3
                                &&
                                <FormButton onClick={() => {
                                    this.setCurrentPage(this.props.pagination!.currentPage - 1)
                                }}>
                                    {
                                        this.props.pagination.currentPage - 1
                                    }
                                </FormButton>
                            }
                            <div className="input">
                                <FormInput value={String(this.state.currentPage)} onChange={(currentPage) => {
                                    if (!/^[0-9]+$/g.test(currentPage) && currentPage !== '') {
                                        return
                                    }
                                    this.setState({
                                        currentPage: currentPage === '' ? '' : String(Number(currentPage))
                                    })
                                }} onBlur={() => {
                                    const currentPage = Math.min(this.props.pagination!.lastPage, Math.max(1, Number(this.state.currentPage)))
                                    this.setState({
                                        currentPage: String(currentPage)
                                    })
                                    if (currentPage === this.props.pagination!.currentPage) {
                                        return
                                    }
                                    this.props.pagination!.onChangePage(currentPage)
                                }} />
                            </div>
                            {
                                this.props.pagination.currentPage <= this.props.pagination.lastPage - 2
                                &&
                                <FormButton onClick={() => {
                                    this.setCurrentPage(this.props.pagination!.currentPage + 1)
                                }}>
                                    {
                                        this.props.pagination.currentPage + 1
                                    }
                                </FormButton>
                            }
                            {
                                this.props.pagination.currentPage !== this.props.pagination.lastPage
                                &&
                                <>
                                    <MoreHoriz />
                                    <FormButton onClick={() => {
                                        this.setCurrentPage(this.props.pagination!.lastPage)
                                    }}>
                                        {
                                            this.props.pagination.lastPage
                                        }
                                    </FormButton>
                                </>
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }

    /**
     * Checks whether all columns are selected
     */
    private areAllColumnsSelected = (): boolean => {
        return this.state.selection.length > 0
            && this.props.data
                .map((v) => v.uuid)
                .map((v) => !this.state.selection.includes(v))
                .filter((v) => v)
                .length === 0
    }

    /**
     * Set the current page
     */
    private setCurrentPage = (currentPage: number) => {
        this.setState({
            currentPage: String(currentPage)
        }, () => {
            this.props.pagination!.onChangePage(currentPage)
        })
    }
}