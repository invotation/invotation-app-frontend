import './form-select.scss'
import { ChevronLeft, Close, Search, Subject } from '@material-ui/icons'
import React from 'react'
import classNames from 'classnames'
import FormButton from '../form-button/FormButton'
import Device from '../../core/Device'
import FormInput from '../form-input/FormInput'
import AbsolutePositioning from '../absolute-positioning/AbsolutePositioning'
import { Subject as RxJSSubject, Subscription } from 'rxjs'

/**
 * The props
 */
interface FormSelectProps {
    /**
     * Includes a search bar
     */
    search?: boolean

    /**
     * Includes an add button for a new entity
     */
    addButton?: JSX.Element

    /**
     * The default label for the select
     */
    label: string

    /**
     * The value of the selected item
     */
    value: string

    /**
     * The options in the menu
     */
    options: Array<{
        label: string
        value: string
    }>

    /**
     * Whenever the value changes
     */
    onChange: (value: string) => void
}

/**
 * The state
 */
interface FormSelectState {
    /**
     * The ID of the form select
     */
    id: number

    /**
     * Whether or not the menu is open
     */
    menuOpen: boolean

    /**
     * Whether the select has been initialized
     */
    initialized: boolean

    /**
     * The previous dimensions
     */
    previousDimensions: {
        width: number
        height: number
    }

    /**
     * The search string
     */
    search: string
}

/**
 * A select in a form.
 * @author Stan Hurks
 */
export default class FormSelect extends React.Component<FormSelectProps, FormSelectState> {
    /**
     * Closes all form selects except for the one with the given ID
     */
    public static closeAllButSkipId: RxJSSubject<number> = new RxJSSubject()
    private subscriptionCloseAllButSkipId: Subscription | null = null

    /**
     * The ref to the element of the container
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            id: AbsolutePositioning.counter ++,
            menuOpen: false,
            initialized: false,
            previousDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            },
            search: ''
        }
    }

    public componentDidMount = () => {
        this.subscriptionCloseAllButSkipId = FormSelect.closeAllButSkipId.subscribe((skipId) => {
            if (skipId === this.state.id) {
                return
            }
            this.setState({
                menuOpen: false,
                initialized: false
            })
        })
        window.addEventListener('resize', this.onResize)
    }

    public componentWillUnmount = () => {
        window.removeEventListener('resize', this.onResize)
        this.subscriptionCloseAllButSkipId?.unsubscribe()
    }

    public render = () => {
        return (
            <>
                <div className="form-select" ref={this.ref} onClick={(event) => {
                    let found: boolean = false
                    const findParent = (root: Element) => {
                        if (root.classList.contains('form-select-menu') || root.classList.contains('form-select-menu-mobile')) {
                            found = true
                        }
                        if (root.parentElement) {
                            findParent(root.parentElement)
                        }
                    }
                    findParent(event.target as Element)
                    if (found) {
                        return
                    }
                    this.setState({
                        menuOpen: !this.state.menuOpen,
                        initialized: true
                    }, () => {
                        if (this.state.menuOpen && this.ref.current) {
                            AbsolutePositioning.mount.next({
                                id: this.state.id,
                                element: this.ref.current,
                                elementAnchor: 'bottom-left',
                                content: this.getAbsolutePositioningContent()
                            })
                        } else {
                            AbsolutePositioning.unmount.next()
                            AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                        }
                    })
                }}>
                    <div className="form-select-label">
                        {
                            this.props.value && this.props.options.filter((o) => o.value === this.props.value).length
                            ? this.props.options.filter((o) => o.value === this.props.value)[0].label
                            : this.props.label
                        }
                    </div>

                    <div className={classNames({
                        'form-select-icon': true,
                        'menu-open': this.state.menuOpen
                    })}>
                        <ChevronLeft />
                    </div>
                </div>
            </>
        )
    }

    /**
     * Renders the options
     * @param classNamePrefix The class name prefix
     */
    private renderOptions = (classNamePrefix: string) => {
        return (
            <>
                {
                    (this.props.search || this.props.addButton)
                    &&
                    <div className={classNames({
                        [`${classNamePrefix}-top`]: true
                    })}>
                        <div className={classNames({
                            [`${classNamePrefix}-top-left`]: true
                        })}>
                            <FormInput value={this.state.search} onChange={(search) => {
                                this.setState({
                                    search
                                }, () => {
                                    AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                })
                            }} endAdornment={(
                                <Search />
                            )} />
                        </div>
                        {
                            this.props.addButton
                            &&
                            <div className={classNames({
                                [`${classNamePrefix}-top-right`]: true
                            })}>
                                {
                                    this.props.addButton
                                }
                            </div>
                        }
                    </div>
                }
                {
                    (this.props.search || this.props.addButton)
                    &&
                    <div className={classNames({
                        [`${classNamePrefix}-hr`]: true
                    })}>
                        <div className={classNames({
                            [`${classNamePrefix}-hr-hr`]: true
                        })}></div>
                    </div>
                }
                <div className={classNames({
                    [`${classNamePrefix}-options`]: true
                })}>
                    {
                        this.props.options
                        .filter((v) => {
                            if (this.state.search === '') {
                                return true
                            }
                            else {
                                return v.label.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
                            }
                        })
                        .map((option) => (
                            <div className={classNames({
                                [`${classNamePrefix}-options-option`]: true,
                                'selected': option.value === this.props.value
                            })} key={option.value} onClick={() => {
                                this.props.onChange(option.value)
                                this.setState({
                                    menuOpen: false
                                }, () => {
                                    AbsolutePositioning.unmount.next()
                                    AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                })
                            }}>
                                {
                                    option.label
                                }
                            </div>
                        ))
                    }
                </div>
            </>
        )
    }

    /**
     * Get the content for the AbsolutePositioning component.
     */
    private getAbsolutePositioningContent = (): JSX.Element => {
        if (window.innerWidth <= Device.mobileResolution) {
            return (
                <div className={classNames({
                    'form-select-menu-mobile': true,
                    'menu-open': this.state.menuOpen
                })}>
                    <div className="form-select-menu-mobile-container">
                        <div className="form-select-menu-mobile-container-top-right">
                            <FormButton variant="white" onClick={() => {
                                this.setState({
                                    menuOpen: false
                                }, () => {
                                    AbsolutePositioning.unmount.next()
                                    AbsolutePositioning.updateContent.next(this.getAbsolutePositioningContent())
                                })
                            }}>
                                <Close />
                            </FormButton>
                        </div>

                        <div className="form-select-menu-mobile-container-content">
                            {
                                this.renderOptions('form-select-menu-mobile-container-content')
                            }
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className={classNames({
                'form-select-menu': true,
                'menu-open': this.state.menuOpen,
                'initialized': this.state.initialized,
                'full-size': this.props.addButton || this.props.search
            })}>
                {
                    this.renderOptions('form-select-menu')
                }
            </div>
        )
    }

    /**
     * Whenever the client resizes the window
     */
    private onResize = () => {
        const currentlyMobile: boolean = window.innerWidth <= Device.mobileResolution
        const wasMobile: boolean = this.state.previousDimensions.width <= Device.mobileResolution
        if (currentlyMobile !== wasMobile) {
            this.setState({
                menuOpen: false,
                initialized: false
            })
        }
        this.setState({
            previousDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        })
    }
}