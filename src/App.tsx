import React from 'react'
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch,
} from 'react-router-dom'
import {
    Route as AppRoute
} from './enum/Route'
import './app.scss'
import { Subject, Subscription } from 'rxjs'
import ImageViewer from './components/image-viewer/ImageViewer'
import LoginPage from './pages/login/LoginPage'
import { Locale } from './enum/Locale'
import Storage from './storage/Storage'
import Translations from './translations/Translations'
import Modal from './components/modals/Modal'
import ErrorModal from './components/modals/error/ErrorModal'
import GraphQL from './backend/GraphQL'
import Loader from './components/loader/Loader'
import DashboardPage from './pages/dashboard/DashboardPage'
import Sidebar from './components/sidebar/Sidebar'
import classNames from 'classnames'
import AccountBusinessesDashboard from './pages/account/businesses/dashboard/AccountBusinessesDashboard'
import TemplateEditPage from './pages/templates/edit/TemplateEditPage'
import { ResponseCode } from './backend/responses/base/ResponseCode'
import { UserBase } from './backend/entities/UserBase'
import { SubscriptionBase } from './backend/entities/SubscriptionBase'
import StorageDataModelUser from './storage/data-model/entity/StorageDataModelUser'
import AccountBusinessesBusinessProfilePage from './pages/account/businesses/business-profile/AccountBusinessesBusinessProfilePage'
import InfoModal from './components/modals/info/InfoModal'
import FormButton from './components/form-button/FormButton'
import AccountBusinessesPaymentOptionsPage from './pages/account/businesses/payment-options/AccountBusinessesPaymentOptionsPage'
import AbsolutePositioning from './components/absolute-positioning/AbsolutePositioning'
import TemplateDashboardPage from './pages/templates/dashboard/TemplateDashboardPage'
import ClientDashboardPage from './pages/clients/dashboard/ClientDashboardPage'
import ClientEditPage from './pages/clients/edit/ClientEditPage'
import ItemDashboardPage from './pages/items/dashboard/ItemDashboardPage'
import ItemEditPage from './pages/items/edit/ItemEditPage'
import Device from './core/Device'

interface AppState {
    /**
     * The redirect URL
     */
    redirect: string | null

    /**
     * Whether there are unsaved changes in the page
     */
    unsavedChanges: boolean

    /**
     * The body scroll
     */
    bodyScroll: {

        /**
         * The scroll top upon freeze
         */
        scrollTop: number
    } | null

    /**
     * Whether or not the sidebar is visible
     */
    sidebarVisible: boolean

    /**
     * The current route
     */
    currentRoute: {
        route: AppRoute
        previous?: {
            route: AppRoute
            variables?: {[key: string]: string}
        }
    } | null
}

/**
 * The main app entry point.
 * @author Stan Hurks
 */
export default class App extends React.Component<any, AppState> {
    /**
     * Redirect to another route
     */
    public static redirect: Subject<{route: AppRoute, variables?: {[key: string]: string}}> = new Subject()
    private subscriptionRedirect: Subscription | null = null

    /**
     * Whether there are unsaved changes
     */
    public static unsavedChanges: Subject<boolean> = new Subject()
    private subscriptionUnsavedChanges: Subscription | null = null

    /**
     * Redirect to another route
     */
    public static setCurrentRoute: Subject<{
        route: AppRoute
        previous?: {
            route: AppRoute
            variables?: {[key: string]: string}
        }
    }> = new Subject()
    private subscriptionSetCurrentRoute: Subscription | null = null

    /**
     * Update the user and subscription
     */
    public static updateUserAndSubscription: Subject<void> = new Subject()
    private subscriptionUpdateUserAndSubscription: Subscription | null = null

    /**
     * Set the body scroll
     */
    public static setBodyScroll: Subject<boolean> = new Subject()
    private subscriptionSetBodyScroll: Subscription | null = null

    /**
     * Set the locale
     */
    public static setLocale: Subject<Locale> = new Subject()
    private subscriptionSetLocale: Subscription | null = null

    /**
     * Subscriptions for all default API response codes
     */
    private subscriptionApiUnreachable: Subscription | null = null
    private subscriptionUnauthorized: Subscription | null = null
    private subscriptionForbidden: Subscription | null = null
    private subscriptionInternalServerError: Subscription | null = null
    private subscriptionTooManyRequests: Subscription | null = null
    private subscriptionInvalidCode: Subscription | null = null
    private subscriptionInvalidCredentials: Subscription | null = null

    constructor(props: any) {
        super(props)
        this.state = {
            redirect: null,
            unsavedChanges: false,
            bodyScroll: null,
            sidebarVisible: false,
            currentRoute: null
        }
    }

    public componentDidMount = () => {
        this.subscriptionRedirect = App.redirect.subscribe((redirect) => {
            const performRedirect = () => {
                let url = redirect.route as string
                if (redirect.variables) {
                    for (const variableName of Object.keys(redirect.variables)) {
                        url = url.replace(`:${variableName}`, redirect.variables[variableName])
                    }
                }
                if (redirect.route !== window.location.pathname) {
                    window.history.pushState('', '', url)
                    this.setState({
                        redirect: url,
                        unsavedChanges: false
                    }, () => {
                        this.setState({redirect: null})
                        AbsolutePositioning.unmount.next()
                    })
                }
            }
            if (!this.state.unsavedChanges) {
                performRedirect()
            } else {
                Modal.mount.next((
                    <InfoModal
                        title={Translations.translations.components.app.unsavedChangesModal.title} 
                        content={Translations.translations.components.app.unsavedChangesModal.content} 
                        button={(
                            <FormButton variant="primary" onClick={() => {
                                performRedirect()
                                Modal.unmount.next()
                            }}>
                                {
                                    Translations.translations.components.app.unsavedChangesModal.button
                                } 
                            </FormButton>
                        )} />
                ))
            }
        })
        this.subscriptionUnsavedChanges = App.unsavedChanges.subscribe((unsavedChanges) => {
            this.setState({
                unsavedChanges
            })
        })
        this.subscriptionSetCurrentRoute = App.setCurrentRoute.subscribe((route) => {
            this.setState({
                currentRoute: route,
                sidebarVisible: route.route !== AppRoute.LOGIN
            })
        })
        this.subscriptionSetBodyScroll = App.setBodyScroll.subscribe((bodyScroll) => {
            if (window.innerWidth > Device.mobileResolution) {
                if (this.state.bodyScroll) {
                    const top = this.state.bodyScroll.scrollTop
                    this.setState({
                        bodyScroll: null
                    }, () => {
                        window.scroll({
                            top
                        })
                    })
                }
                return
            }
            if (bodyScroll) {
                this.setState({
                    bodyScroll: {
                        scrollTop: window.scrollY
                    }
                }, () => {
                    window.scroll({
                        top: 0
                    })
                })
            } else if (this.state.bodyScroll) {
                const top = this.state.bodyScroll.scrollTop
                this.setState({
                    bodyScroll: null
                }, () => {
                    window.scroll({
                        top
                    })
                })
            }
        })
        this.subscriptionUpdateUserAndSubscription = App.updateUserAndSubscription.subscribe(() => {
            GraphQL.query<{
                me: {
                    code: ResponseCode,
                    data: {
                        user: UserBase,
                        subscription: SubscriptionBase
                    }
                }
            }>(`
                query {
                    me {
                        code
                        data {
                            user {
                                uuid
                                activated
                                email
                                locale
                                defaultBusiness {
                                    uuid
                                }
                            }
                            subscription {
                                uuid
                                type
                            }
                        }
                    }
                }
            `).then((data) => {
                StorageDataModelUser.initialize(data.data.me.data.user)
            })
        })
        this.subscriptionSetLocale = App.setLocale.subscribe((locale) => {
            Storage.data.locale = locale as Locale
            Translations.initialize()
            this.forceUpdate()
        })
        this.subscriptionApiUnreachable = GraphQL.apiUnreachable.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.apiUnreachable.title}
                    content={Translations.translations.api.errors.apiUnreachable.content}
                    />
            ))
        })
        this.subscriptionApiUnreachable = GraphQL.apiUnreachable.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.apiUnreachable.title}
                    content={Translations.translations.api.errors.apiUnreachable.content}
                    />
            ))
        })
        this.subscriptionUnauthorized = GraphQL.unauthorized.subscribe(() => {
            Storage.reset()
            App.redirect.next({
                route: AppRoute.LOGIN
            })
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.unauthorized.title}
                    content={Translations.translations.api.errors.unauthorized.content}
                    />
            ))
        })
        this.subscriptionForbidden = GraphQL.forbidden.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.forbidden.title}
                    content={Translations.translations.api.errors.forbidden.content}
                    />
            ))
        })
        this.subscriptionInternalServerError = GraphQL.internalServerError.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.internalServerError.title}
                    content={Translations.translations.api.errors.internalServerError.content}
                    />
            ))
        })
        this.subscriptionTooManyRequests = GraphQL.tooManyRequests.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.tooManyRequests.title}
                    content={Translations.translations.api.errors.tooManyRequests.content}
                    />
            ))
        })
        this.subscriptionInvalidCode = GraphQL.invalidCode.subscribe(() => {
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.invalidCode.title}
                    content={Translations.translations.api.errors.invalidCode.content}
                    />
            ))
        })
        this.subscriptionInvalidCredentials = GraphQL.invalidCredentials.subscribe(() => {
            console.log(Translations.currentLanguage)
            Modal.mount.next((
                <ErrorModal
                    title={Translations.translations.api.errors.invalidCredentials.title}
                    content={Translations.translations.api.errors.invalidCredentials.content}
                    />
            ))
        })

        if (Storage.data.session.uuid) {
           App.updateUserAndSubscription.next()
        }

        document.addEventListener('click', this.onDocumentClick)
        window.onbeforeunload = this.onBeforeUnload
    }

    public componentWillUnmount = () => {
        this.subscriptionRedirect?.unsubscribe()
        this.subscriptionUnsavedChanges?.unsubscribe()
        this.subscriptionSetCurrentRoute?.unsubscribe()
        this.subscriptionSetBodyScroll?.unsubscribe()
        this.subscriptionSetBodyScroll?.unsubscribe()
        this.subscriptionUpdateUserAndSubscription?.unsubscribe()
        this.subscriptionSetLocale?.unsubscribe()
        this.subscriptionApiUnreachable?.unsubscribe()
        this.subscriptionUnauthorized?.unsubscribe()
        this.subscriptionForbidden?.unsubscribe()
        this.subscriptionInternalServerError?.unsubscribe()
        this.subscriptionTooManyRequests?.unsubscribe()
        this.subscriptionInvalidCode?.unsubscribe()
        this.subscriptionInvalidCredentials?.unsubscribe()

        document.removeEventListener('click', this.onDocumentClick)
        window.onbeforeunload = null
    }

    public render = () => {
		return (
            <>
                <ImageViewer />

                <Modal />

                <Loader />

                <AbsolutePositioning />

                <Sidebar visible={this.state.sidebarVisible} currentRoute={this.state.currentRoute} />

                <div className="app" style={{
                    ...this.state.bodyScroll
                    && {
                        transform: 'translateY(-' + this.state.bodyScroll.scrollTop + 'px)',
                        OTransform: 'translateY(-' + this.state.bodyScroll.scrollTop + 'px)',
                        MozTransform: 'translateY(-' + this.state.bodyScroll.scrollTop + 'px)',
                        MsTransform: 'translateY(-' + this.state.bodyScroll.scrollTop + 'px)',
                        position: 'fixed'
                    }
                }}>
                    <div className={classNames({
                        'app-page': true,
                        'visible': this.state.sidebarVisible
                    })}>
                        <Router>
                            <Switch>
                                {
                                    this.state.redirect
                                    &&
                                    <Redirect to={this.state.redirect} />
                                }

                                {/* Login page */}
                                <Route exact path={AppRoute.LOGIN} render={(params) => this.isAuthorized()
                                    ? (<Redirect to={AppRoute.DASHBOARD} />)
                                    : (<LoginPage {...params} />)
                                } />

                                {/* Reset password */}
                                <Route exact path={AppRoute.RESET_PASSWORD} render={(params) => (
                                    <LoginPage {...params} currentForm={'reset-password'} />
                                )} />

                                {/* Dashboard page */}
                                <Route exact path={AppRoute.DASHBOARD} render={() => this.isAuthorized()
                                    ? (<DashboardPage />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Items dashboard page */}
                                <Route exact path={AppRoute.ITEMS} render={() => this.isAuthorized()
                                    ? (<ItemDashboardPage />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Items edit/add page */}
                                <Route exact path={AppRoute.ITEMS_EDIT} render={(params) => this.isAuthorized()
                                    ? (<ItemEditPage {...params} />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Clients dashboard page */}
                                <Route exact path={AppRoute.CLIENTS} render={() => this.isAuthorized()
                                    ? (<ClientDashboardPage />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Clients edit page */}
                                <Route exact path={AppRoute.CLIENTS_EDIT} render={(params) => this.isAuthorized()
                                    ? (<ClientEditPage {...params} />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Account businesses dashboard page */}
                                <Route exact path={AppRoute.ACCOUNT_BUSINESSES} render={() => this.isAuthorized()
                                    ? (<AccountBusinessesDashboard />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Account business business profile page */}
                                <Route exact path={AppRoute.ACCOUNT_BUSINESSES_PROFILE} render={(params) => this.isAuthorized()
                                    ? (<AccountBusinessesBusinessProfilePage {...params} />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Account business business profile page */}
                                <Route exact path={AppRoute.ACCOUNT_BUSINESSES_PAYMENTS} render={(params) => this.isAuthorized()
                                    ? (<AccountBusinessesPaymentOptionsPage {...params} />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Template dashboard page */}
                                <Route exact path={AppRoute.TEMPLATES} render={() => this.isAuthorized()
                                    ? (<TemplateDashboardPage />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Template edit page */}
                                <Route exact path={AppRoute.TEMPLATES_EDIT} render={(params) => this.isAuthorized()
                                    ? (<TemplateEditPage {...params} />)
                                    : (<Redirect to={AppRoute.LOGIN} />)
                                } />

                                {/* Redirect when not found */}
                                <Route render={() => (
                                    <Redirect to={AppRoute.LOGIN} />
                                )} />
                            </Switch>
                        </Router>
                    </div>
                </div>
            </>
		)
    }

    /**
     * Checks whether the client is authorized
     */
    private isAuthorized = (): boolean => {
        return Boolean(Storage.data.session.uuid)
    }

    /**
     * Whenever the document is clicked
     */
    private onDocumentClick = (event: Event) => {
        this.preventAnchorClick(event)
    }

    /**
     * Before the page unloads
     */
    private onBeforeUnload = () => {
        if (this.state.unsavedChanges) {
            return Translations.translations.components.app.beforeUnload
        }
    }
    
    /**
     * Prevents that clicking an a[href] changes the window location.
     * 
     * First check if the link starts with '/'.
     * 
     * Then if so go to that route in the same window.
     * 
     * If not set the target to _blank and open the URL in a new window
     */
    private preventAnchorClick = (event: Event) => {
        if (event.target === null) {
            return false
        }

        let element = (event.target as Element)
        if (!element) {
            return false
        }
        
        const loopThroughElementAndParentsUntilHrefIsFound = (element: Element) => {
            if (element.hasAttribute('href')) {
                const url = element.getAttribute('href')
    
                if (url == null) {
                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()
                    return false
                }

                if (Object.keys(AppRoute).map((v) => (AppRoute as any)[v as any]).filter((v) => v.startsWith(url.substring(0, url.indexOf('#') === -1 ? url.length - 1 : url.indexOf('#')))).length) {
                    if (url.indexOf('#') === -1) {
                        window.scroll({
                            top: 0
                        })
                    } else {
                        window.scroll({
                            top: 0
                        })

                        setTimeout(() => {
                            const element = document.querySelector(url.substring(url.indexOf('#')))
                            const scrollY = element ? element.getBoundingClientRect().top : 0

                            window.scroll({
                                top: scrollY
                            })
                        })
                    }
                    
                    window.history.pushState('', '', url.substring(0, url.indexOf('#') === -1 ? url.length : url.indexOf('#')))
                    App.redirect.next({
                        route: url.substring(0, url.indexOf('#') === -1 ? url.length : url.indexOf('#')) as AppRoute
                    })

                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()

                    return false
                }
            }
        }
        loopThroughElementAndParentsUntilHrefIsFound(element)
    }
}