import { Locale } from '../../enum/Locale'
import StorageDataModelSession from './entity/StorageDataModelSession'

/**
 * The data model for the storage functionality.
 * @author Anonymous
 */
export default class StorageDataModel {

    /**
     * The session of the user
     */
    public session: StorageDataModelSession = new StorageDataModelSession()

    /**
     * The locale of the application
     */
    public locale: Locale = (navigator.language || (navigator as any)['userLanguage']).indexOf('nl-') !== -1 ? Locale.NL_NL : Locale.EN_GB
}
