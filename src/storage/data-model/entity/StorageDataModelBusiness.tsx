import { BusinessBase } from "../../../backend/entities/BusinessBase"
import Storage from "../../Storage"

/**
 * Represents a business in the storage.
 * @author Stan Hurks
 */
export default class StorageDataModelBusiness {
    /**
     * The UUID of the business
     */
    uuid: string|null = null

    public static initialize = (business: BusinessBase) => {
        Storage.data.session.user.business.uuid = business.uuid
    }
}