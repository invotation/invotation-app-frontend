import { UserBase } from "../../../backend/entities/UserBase"
import Storage from "../../Storage"
import { Locale } from "../../../enum/Locale"
import StorageDataModelBusiness from "./StorageDataModelBusiness"

/**
 * Represents the user in the storage.
 * @author Anonymous
 */
export default class StorageDataModelUser {
    /**
     * The UUID of the user
     */
    uuid: string|null = null

    /**
     * The e-mailaddress of the user
     */
    email: string|null = null

    /**
     * The locale of the user
     */
    locale: Locale|null = null

    /**
     * Whether or not the user has activated the account
     */
    activated: boolean|null = null

    /**
     * The business uuid
     */
    business: StorageDataModelBusiness = new StorageDataModelBusiness()

    public static initialize(user: UserBase) {
        Storage.data.session.user.uuid = user.uuid
        Storage.data.session.user.activated = user.activated
        Storage.data.session.user.email = user.email
        Storage.data.session.user.locale = user.locale
        StorageDataModelBusiness.initialize(user.defaultBusiness)
    }
}