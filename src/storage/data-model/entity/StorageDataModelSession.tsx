import StorageDataModelUser from './StorageDataModelUser'
import { SessionBase } from '../../../backend/entities/SessionBase'
import Storage from '../../Storage'

/**
 * The storage data model for the `Session`.
 * 
 * @author Anonymous
 */
export default class StorageDataModelSession {
    /**
     * The session uuid
     */
    public uuid: string|null = null

    /**
     * The user bound to the session
     */
    public user: StorageDataModelUser = new StorageDataModelUser()

    public static initialize(session: SessionBase) {
        Storage.data.session.uuid = session.uuid
        
        StorageDataModelUser.initialize(session.user)
    }
}
