import StorageDataModel from "./data-model/StorageDataModel"
import { Objects } from "../core/Objects"
import { Subject } from "rxjs"

/**
 * The storage functionality.
 * @author Anonymous
 */
export default class Storage {
    /**
     * Whenever the storage data has been updated
     */
    public static updated: Subject<void> = new Subject()
    
    /**
     * The key in which the data model is stored
     */
    private static key: string = 'data'

    /**
     * The storage data.
     */
    public static data: StorageDataModel = new StorageDataModel() 

    /**
     * Initialize the storage functionality
     */
    public static initialize = () => {
        const localStorageData = localStorage.getItem(Storage.key)
        const sessionStorageData = sessionStorage.getItem(Storage.key)
        if (localStorageData || (!localStorageData && !sessionStorageData)) {
            Storage.initializeStorageType(localStorage)
        } else {
            Storage.initializeStorageType(sessionStorage)
        }
    }

    /**
     * Reset the storage data model to its initial values
     */
    public static reset = () => {
        localStorage.removeItem(Storage.key)
        sessionStorage.removeItem(Storage.key)
        Storage.data = new StorageDataModel()
        Storage.serializeDataModel()
    }

    /**
     * Move the storage data from one storage location to another
     */
    public static moveStorageData = () => {
        const localStorageData = localStorage.getItem(Storage.key)
        const sessionStorageData = sessionStorage.getItem(Storage.key)
        if (localStorageData !== null)  {
            sessionStorage.setItem(Storage.key, localStorageData)
            localStorage.removeItem(Storage.key)
        } else if (sessionStorageData !== null) {
            localStorage.setItem(Storage.key, sessionStorageData)
            sessionStorage.removeItem(Storage.key)
        }
    }

    /**
     * Initialize a storage type
     */
    private static initializeStorageType = (storage: typeof localStorage) => {
        const data = storage.getItem(Storage.key)
        if (data !== null) {
            const newData: any = JSON.parse(data)
            const recurseAddNewProperties = (dataRoot: any, root: any) => {
                for (const property of Object.keys(root)) {
                    if (Objects.isObject(root[property])) {
                        if (dataRoot[property] === undefined) {
                            dataRoot[property] = root[property]
                        } else {
                            recurseAddNewProperties(dataRoot[property], root[property])
                        }
                    } else {
                        if (dataRoot[property] === undefined) {
                            dataRoot[property] = root[property]
                        }
                    }
                }
            }
            recurseAddNewProperties(newData, Storage.data)
            const recurseRemoveProperties = (dataRoot: any, root: any) => {
                for (const property of Object.keys(dataRoot)) {
                    if (Objects.isObject(dataRoot[property])) {
                        if (root[property] === undefined) {
                            delete dataRoot[property]
                        } else {
                            recurseRemoveProperties(dataRoot[property], root[property])
                        }
                    } else {
                        if (root[property] === undefined) {
                            delete dataRoot[property]
                        }
                    }
                }
            }
            recurseRemoveProperties(newData, Storage.data)
            Storage.data = newData
        }
        Storage.serializeDataModel()
        Storage.deserializeDataModel()
    }

    /**
     * Serializes the `Storage.data` to be used by the application
     */
    private static serializeDataModel = () => {
        const recurse = (root: any) => {
            for (const property of Object.keys(root)) {
                if (Objects.isObject(root[property])) {
                    recurse(root[property])
                } else {
                    root[`_${property}`] = root[property]
                    delete root[property]
                    Object.defineProperty(root, property, {
                        get: function() {
                            return this[`_${property}`]
                        },
                        set: function(value: any) {
                            this[`_${property}`] = value
                            Storage.deserializeDataModel()
                            Storage.updated.next()
                        }
                    })
                }
            }
        }
        recurse(Storage.data)
    }

    /**
     * Deserializes the `Storage.data` to be saved in storage
     */
    private static deserializeDataModel = () => {
        let dataModel: StorageDataModel = Storage.data
        const recurse = (dataModelRoot: any) => {
            let deserializedObject: any = {}
            for (const property of Object.keys(dataModelRoot)) {
                if (property.startsWith('_')) {
                    deserializedObject[property.substring(1)] = dataModelRoot[property]
                } else {
                    deserializedObject[property] = recurse(dataModelRoot[property])
                }
            }
            return deserializedObject
        }
        let deserializedDataModel: any = recurse(dataModel)

        const localStorageData = localStorage.getItem(Storage.key)
        const sessionStorageData = sessionStorage.getItem(Storage.key)
        if (localStorageData || (!localStorageData && !sessionStorageData)) {
            localStorage.setItem(Storage.key, JSON.stringify(deserializedDataModel))
        } else {
            sessionStorage.setItem(Storage.key, JSON.stringify(deserializedDataModel))
        }
    }
}