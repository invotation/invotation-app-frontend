import Storage from '../storage/Storage'
import { TranslationsENGB } from './EN_GB/TranslationsENGB'
import { Locale } from '../enum/Locale'
import { Subject } from 'rxjs'
import moment from 'moment'
import { TranslationsNLNL } from './NL_NL/TranslationsNLNL'

/**
 * The available languages in the application
 */
export const availableLanguages: {[key in Locale]: {
    name: string
    lang: string
    moment: string
    translations: typeof TranslationsENGB
}} = {
    NL_NL: {
        name: 'Nederlands',
        lang: 'nl',
        moment: 'nl',
        translations: TranslationsNLNL
    },
    EN_GB: {
        name: 'English',
        lang: 'en',
        moment: 'en-gb',
        translations: TranslationsENGB
    }
}

/**
 * The manager for all translation related functionality and functionality that depends on
 * translations. E.g.: Document meta tags and title.
 * 
 * @author Stan Hurks
 */
export default abstract class Translations {
    /**
     * Whenever the locale changes
     */
    public static localeChanged: Subject<Locale> = new Subject()

    /**
     * All translations
     */
    public static translations: typeof TranslationsENGB = TranslationsENGB

    /**
     * The current language used by the application
     */
    public static currentLanguage: Locale = Storage.data.locale

    /**
     * Initialize the translation manager
     */
    public static initialize = () => {
        const currentLocale = Translations.currentLanguage

        // Set the current language based on local storage or location GET parameter
        const langGetParameter: string|null = new URL(document.location.href).searchParams.get('lang')
        Translations.currentLanguage = Storage.data.session.user.locale
            || Storage.data.locale
            || langGetParameter

        // Set the translations
        Translations.translations = availableLanguages[Translations.currentLanguage].translations

        // Set moment
        const localization = require(`moment/locale/${availableLanguages[Translations.currentLanguage].moment}`)
        moment.updateLocale(availableLanguages[Translations.currentLanguage].moment, localization)

        // Locale changed
        if (currentLocale !== Translations.currentLanguage) {
            Translations.localeChanged.next(Translations.currentLanguage)
        }
    }
}