import { Route } from "../../enum/Route"
import { TranslationsENGB } from "../EN_GB/TranslationsENGB"

/**
 * All translations for locale `nl_NL`.
 * @author Stan Hurks
 */
export const TranslationsNLNL: typeof TranslationsENGB = {
    api: {
        errors: {
            apiUnreachable: {
                title: 'Server onbereikbaar',
                content: 'De server is momenteel niet bereikbaar, gelieve deze actie later opnieuw te proberen.'
            },
            unauthorized: {
                title: 'Sessie ongeldig',
                content: 'Je sessie is ongeldig, we hebben je uitgelogd.'
            },
            forbidden: {
                title: 'Verboden',
                content: 'De actie die je probeert uit te voeren is verboden.'
            },
            internalServerError: {
                title: 'Server fout',
                content: 'Iets ging fout in de server, gelieve het later nogmaals te proberen.'
            },
            tooManyRequests: {
                title: 'Te veel requests',
                content: 'Je hebt deze actie te vaak achter elkaar uit proberen te voeren, gelieve het later opnieuw te proberen.'
            },
            invalidCode: {
                title: 'Ongeldige code',
                content: 'De code die je hebt ingevoerd is ongeldig.'
            },
            invalidCredentials: {
                title: 'Ongeldige inloggegevens',
                content: 'Het emailadres en het wachtwoord komen niet overeen.'
            }
        },
        mutations: {
            register: {
                emailExists: {
                    title: 'E-mail bestaat al',
                    content: 'Het gegeven e-mailadres bestaat al.'
                }
            },
            resetPassword: {
                emailNotInUse: {
                    title: 'E-mail niet in gebruik',
                    content: 'Het gegeven e-mailadres is niet in gebruik.'
                },
                ok: {
                    title: 'Bekijk je inbox',
                    content: 'Je hebt een e-mail ontvangen met een link om je wachtwoord te resetten.',
                    button: 'Oké'
                }
            },
            performResetPassword: {
                emailNotInUse: {
                    title: 'E-mail niet in gebruik',
                    content: 'Het gegeven e-mailadres is niet in gebruik.'
                },
                ok: {
                    title: 'Wachtwoord gereset',
                    content: 'Je wachtwoord is gereset.',
                    button: 'Oké'
                }
            }
        }
    },
    components: {
        app: {
            beforeUnload: 'Er zijn niet opgeslagen wijzigingen, weet je zeker dat je de pagina wilt verlaten?',
            unsavedChangesModal: {
                title: 'Niet opgeslagen wijzigingen',
                content: 'Er zijn niet opgeslagen wijzigingen, weet je zeker dat je de pagina wilt verlaten?',
                button: 'Doorgaan'
            }
        },
        colorPicker: {
            rgb: {
                r: 'Rood',
                g: 'Groen',
                b: 'Blauw',
                a: 'Alpha'
            },
            hex: 'Hex'
        },
        formImage: {
            biggerThan5MB: {
                modal: {
                    title: 'Afbeelding te groot',
                    content: 'De afbeelding is groter dan 5MB, probeer een andere.',
                    button: 'Oké'
                }
            }
        },
        modals: {
            info: {
                cancel: 'Annuleren'
            },
            error: {
                okay: 'Oké'
            }
        }
    },
    pages: {
        account: {
            businesses: {
                dashboard: {
                    delete: {
                        label: 'Verwijderen',
                        modal: {
                            title: 'Verwijder bedrijf',
                            content: (invoices: number, quotations: number, templates: number): string => {
                                const i = invoices === 1
                                    ? 'factuur'
                                    : 'facturen'
                                const q = quotations === 1
                                    ? 'offerte'
                                    : 'offertes'
                                const t = templates === 1
                                    ? 'template'
                                    : 'templates'
                                return `Weet je zeker dat je dit bedrijf wilt verwijderen? Als je dit doet verwijder je ook ${invoices} ${i}, ${quotations} ${q} en ${templates} ${t}.`
                            },
                            button: 'Verwijder'
                        }
                    }
                }
            }
        },
        clients: {
            dashboard: {
                delete: {
                    label: 'Verwijderen',
                    modal: {
                        title: (clients: number) => `Verwijder ${clients === 1 ? 'klant' : 'klanten'}`,
                        content: (clients: number, invoices: number, quotations: number) => {
                            const i = invoices === 1
                                ? 'factuur'
                                : 'facturen'
                            const q = quotations === 1
                                ? 'offerte'
                                : 'offertes'
                            const c = clients === 1
                                ? 'deze klant'
                                : 'deze klanten'
                            return `Weet je zeker dat je ${c} wilt verwijderen? Als je dit doet verwijder je ook ${invoices} ${i} en ${quotations} ${q}.`
                        },
                        button: 'Verwijder'
                    }
                }
            }
        },
        items: {
            dashboard: {
                delete: {
                    label: 'Verwijderen',
                    modal: {
                        title: (items: number) => `Verwijder ${items === 1 ? 'item' : 'items'}`,
                        content: (items: number) => {
                            const i = items === 1
                                ? 'dit item'
                                : 'deze items'
                            return `Weet je zeker dat je ${i} wilt verwijderen? Wanneer het verwijderd is kun je het nooit meer terug krijgen.`
                        },
                        button: 'Verwijder'
                    }
                }
            }
        },
        login: {
            forms: {
                login: {
                    email: {
                        label: 'Email'
                    },
                    password: {
                        label: 'Wachtwoord'
                    },
                    forgotPassword: {
                        label: 'Wachtwoord vergeten?'
                    },
                    registerButton: 'Probeer 14 dagen gratis',
                    loginButton: 'Login'
                },
                forgotPassword: {
                    email: {
                        label: 'Email'
                    },
                    resetPasswordButton: 'Wachtwoord resetten'
                },
                resetPassword: {
                    email: {
                        label: 'Email'
                    },
                    code: {
                        label: 'Code'
                    },
                    password: {
                        label: 'Wachtwoord'
                    },
                    resetPasswordButton: 'Wijzig wachtwoord'
                },
                register: {
                    email: {
                        label: 'Email'
                    },
                    password: {
                        label: 'Wachtwoord'
                    },
                    termsAndConditions: (link: string) => (
                        <span>Ik accepteer de <a href={link}>Algemene voorwaarden</a>.</span>
                    ),
                    registerButton: 'Probeer 14 dagen gratis'
                }
            }
        },
        templates: {
            dashboard: {
                delete: {
                    label: 'Verwijderen',
                    modal: {
                        title: (templates: number) => `Verwijder ${templates === 1 ? 'template' : 'templates'}`,
                        content: (templates: number, invoices: number, quotations: number) => {
                            const i = invoices === 1
                                ? 'factuur'
                                : 'facturen'
                            const q = quotations === 1
                                ? 'offerte'
                                : 'offertes'
                            const t = templates === 1
                                ? 'deze template'
                                : 'deze templates'
                            return `Weet je zeker dat je ${t} wilt verwijderen? Als je dit doet verwijder je ook ${invoices} ${i} en ${quotations} ${q}.`
                        },
                        button: 'Verwijder'
                    }
                }
            }
        },
    },
    title: (route: Route): string => {
        switch (route) {
            case Route.DASHBOARD:
                return 'Dashboard'
            case Route.INVOICES:
                return 'Facturen'
            case Route.INVOICES_ADD:
                return 'Nieuwe factuur'
            case Route.INVOICES_EDIT:
                return 'Factuur aanpassen'
            case Route.EXPENSES:
                return 'Uitgaven'
            case Route.EXPENSES_ADD:
                return 'Nieuwe uitgave'
            case Route.EXPENSES_EDIT:
                return 'Uitgave aanpassen'
            case Route.QUOTATIONS:
                return 'Offertes'
            case Route.QUOTATIONS_ADD:
                return 'Nieuwe offerte'
            case Route.QUOTATIONS_EDIT:
                return 'Offerte aanpassen'
            case Route.TEMPLATES:
                return 'Templates'
            case Route.TEMPLATES_ADD:
                return 'Nieuwe template'
            case Route.TEMPLATES_EDIT:
                return 'Template aanpassen'
            case Route.CLIENTS:
                return 'Klanten'
            case Route.CLIENTS_ADD:
                return 'Nieuwe klant'
            case Route.CLIENTS_EDIT:
                return 'Klant aanpassen'
            case Route.ITEMS:
                return 'Items'
            case Route.ITEMS_ADD:
                return 'Nieuw item'
            case Route.ITEMS_EDIT:
                return 'Item aanpassen'
            case Route.ACCOUNT_BUSINESSES:
                return 'Bedrijven'
            case Route.ACCOUNT_BUSINESSES_PROFILE:
                return 'Bedrijfsprofiel'
            case Route.ACCOUNT_BUSINESSES_PAYMENTS:
                return 'Betalingen'
            case Route.ACCOUNT_SUBSCRIPTION:
                return 'Abonnement'
            case Route.ACCOUNT_SETTINGS:
                return 'Account instellingen'
            case Route.SETTINGS:
                return 'Instellingen'
            case Route.RESET_PASSWORD:
            case Route.LOGIN:
                return ''
        }
    }
}