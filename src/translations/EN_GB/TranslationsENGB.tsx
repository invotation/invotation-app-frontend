import { Route } from "../../enum/Route";

/**
 * All translations for locale `en_GB`.
 * @author Stan Hurks
 */
export const TranslationsENGB = {
    api: {
        errors: {
            apiUnreachable: {
                title: 'Server unreachable',
                content: 'The server is currently unreachable, please try this action again later.'
            },
            unauthorized: {
                title: 'Session invalid',
                content: 'Your session is invalid, we have logged you out.'
            },
            forbidden: {
                title: 'Forbidden',
                content: 'The action you tried to perform is forbidden.'
            },
            internalServerError: {
                title: 'Server error',
                content: 'Something went wrong in the server, please try this action again later.'
            },
            tooManyRequests: {
                title: 'Too many requests',
                content: 'You have tried to perform this action too many times in a short time, please try it again later.'
            },
            invalidCode: {
                title: 'Invalid code',
                content: 'The code you have provided is invalid.'
            },
            invalidCredentials: {
                title: 'Invalid credentials',
                content: 'The email and password you have provided do not match.'
            }
        },
        mutations: {
            register: {
                emailExists: {
                    title: 'E-mail already exists',
                    content: 'The given e-mailaddress already exists.'
                }
            },
            resetPassword: {
                emailNotInUse: {
                    title: 'E-mail not in use',
                    content: 'The given e-mailaddress is not in use.'
                },
                ok: {
                    title: 'Check your inbox',
                    content: 'You have received an e-mail with a link to reset your password.',
                    button: 'Okay'
                }
            },
            performResetPassword: {
                emailNotInUse: {
                    title: 'E-mail not in use',
                    content: 'The given e-mailaddress is not in use.'
                },
                ok: {
                    title: 'Password resetted',
                    content: 'Your password has successfully been resetted.',
                    button: 'Okay'
                }
            }
        }
    },
    components: {
        app: {
            beforeUnload: 'There are unsaved changes, are you sure you want to close the page?',
            unsavedChangesModal: {
                title: 'Unsaved changes',
                content: 'There are unsaved changes, are you sure you want to leave to another page?',
                button: 'Continue'
            }
        },
        colorPicker: {
            rgb: {
                r: 'Red',
                g: 'Green',
                b: 'Blue',
                a: 'Alpha'
            },
            hex: 'Hex'
        },
        formImage: {
            biggerThan5MB: {
                modal: {
                    title: 'Image too big',
                    content: 'The image is bigger than 5MB, please try another one.',
                    button: 'Okay'
                }
            }
        },
        modals: {
            info: {
                cancel: 'Cancel'
            },
            error: {
                okay: 'Okay'
            }
        }
    },
    pages: {
        account: {
            businesses: {
                dashboard: {
                    delete: {
                        label: 'Delete',
                        modal: {
                            title: 'Delete business',
                            content: (invoices: number, quotations: number, templates: number): string => {
                                const i = invoices === 1
                                    ? 'invoice'
                                    : 'invoices'
                                const q = quotations === 1
                                    ? 'quotation'
                                    : 'quotations'
                                const t = templates === 1
                                    ? 'template'
                                    : 'templates'
                                return `Are you sure you want to delete this business? It will also delete ${invoices} ${i}, ${quotations} ${q} and ${templates} ${t}.`
                            },
                            button: 'Delete'
                        }
                    }
                }
            }
        },
        clients: {
            dashboard: {
                delete: {
                    label: 'Delete',
                    modal: {
                        title: (clients: number) => `Delete ${clients === 1 ? 'client' : 'clients'}`,
                        content: (clients: number, invoices: number, quotations: number) => {
                            const i = invoices === 1
                                ? 'invoice'
                                : 'invoices'
                            const q = quotations === 1
                                ? 'quotation'
                                : 'quotations'
                            const c = clients === 1
                                ? 'this client'
                                : 'these clients'
                            return `Are you sure you want to delete ${c}? It will also delete ${invoices} ${i} and ${quotations} ${q}.`
                        },
                        button: 'Delete'
                    }
                }
            }
        },
        items: {
            dashboard: {
                delete: {
                    label: 'Delete',
                    modal: {
                        title: (items: number) => `Delete ${items === 1 ? 'item' : 'items'}`,
                        content: (items: number) => {
                            const i = items === 1
                                ? 'this item'
                                : 'these items'
                            return `Are you sure you want to delete ${i}? Once deleted, you can never get it back.`
                        },
                        button: 'Delete'
                    }
                }
            }
        },
        login: {
            forms: {
                login: {
                    email: {
                        label: 'Email'
                    },
                    password: {
                        label: 'Password'
                    },
                    forgotPassword: {
                        label: 'Forgot password?'
                    },
                    registerButton: 'Try 14 days free',
                    loginButton: 'Login'
                },
                forgotPassword: {
                    email: {
                        label: 'Email'
                    },
                    resetPasswordButton: 'Reset password'
                },
                resetPassword: {
                    email: {
                        label: 'Email'
                    },
                    code: {
                        label: 'Code'
                    },
                    password: {
                        label: 'Password'
                    },
                    resetPasswordButton: 'Change password'
                },
                register: {
                    email: {
                        label: 'Email'
                    },
                    password: {
                        label: 'Password'
                    },
                    termsAndConditions: (link: string) => (
                        <span>I agree to the <a href={link}>Terms and Conditions</a>.</span>
                    ),
                    registerButton: 'Try 14 days free'
                }
            }
        },
        templates: {
            dashboard: {
                delete: {
                    label: 'Delete',
                    modal: {
                        title: (templates: number) => `Delete ${templates === 1 ? 'template' : 'templates'}`,
                        content: (templates: number, invoices: number, quotations: number) => {
                            const i = invoices === 1
                                ? 'invoice'
                                : 'invoices'
                            const q = quotations === 1
                                ? 'quotation'
                                : 'quotations'
                            const t = templates === 1
                                ? 'this template'
                                : 'these templates'
                            return `Are you sure you want to delete ${t}? It will also delete ${invoices} ${i} and ${quotations} ${q}.`
                        },
                        button: 'Delete'
                    }
                }
            }
        },
    },
    title: (route: Route): string => {
        switch (route) {
            case Route.DASHBOARD:
                return 'Dashboard'
            case Route.INVOICES:
                return 'Invoices'
            case Route.INVOICES_ADD:
                return 'Add invoice'
            case Route.INVOICES_EDIT:
                return 'Edit invoice'
            case Route.EXPENSES:
                return 'Expenses'
            case Route.EXPENSES_ADD:
                return 'Add expense'
            case Route.EXPENSES_EDIT:
                return 'Edit expense'
            case Route.QUOTATIONS:
                return 'Quotations'
            case Route.QUOTATIONS_ADD:
                return 'Add quotation'
            case Route.QUOTATIONS_EDIT:
                return 'Edit quotation'
            case Route.TEMPLATES:
                return 'Templates'
            case Route.TEMPLATES_ADD:
                return 'Add template'
            case Route.TEMPLATES_EDIT:
                return 'Edit template'
            case Route.CLIENTS:
                return 'Clients'
            case Route.CLIENTS_ADD:
                return 'Add client'
            case Route.CLIENTS_EDIT:
                return 'Edit client'
            case Route.ITEMS:
                return 'Items'
            case Route.ITEMS_ADD:
                return 'Add item'
            case Route.ITEMS_EDIT:
                return 'Edit item'
            case Route.ACCOUNT_BUSINESSES:
                return 'Businesses'
            case Route.ACCOUNT_BUSINESSES_PROFILE:
                return 'Business profile'
            case Route.ACCOUNT_BUSINESSES_PAYMENTS:
                return 'Payments'
            case Route.ACCOUNT_SUBSCRIPTION:
                return 'Subscriptions'
            case Route.ACCOUNT_SETTINGS:
                return 'Account settings'
            case Route.SETTINGS:
                return 'Settings'
            case Route.RESET_PASSWORD:
            case Route.LOGIN:
                return ''
        }
    }
}