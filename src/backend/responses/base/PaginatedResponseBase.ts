import { DataResponseBase } from "./DataResponseBase"

/**
 * The default response with pagination.
 * @author Anonymous
 */
export interface PaginatedResponseBase<T> extends DataResponseBase<T> {
    /**
     * The current page number
     */
    page?: number

    /**
     * The amount of records to take per page
     */
    take?: number

    /**
     * The last page
     */
    lastPage?: number

    /**
     * The total count of entities
     */
    total?: number
}