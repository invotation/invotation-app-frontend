import { ResponseBase } from "./ResponseBase"

/**
 * The base for an entity response.
 * @author Anonymous
 */
export interface DataResponseBase<TEntity> extends ResponseBase {
    data?: TEntity
}