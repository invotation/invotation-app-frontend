import { ResponseCode } from "./ResponseCode"

/**
 * The base for the response of all queries and mutations.
 * @author Anonymous
 */
export interface ResponseBase {
    /**
     * All possible errors
     */
    code: ResponseCode
}