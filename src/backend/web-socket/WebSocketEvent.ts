/**
 * All web socket events
 * @author Anonymous
 */
export enum WebSocketEvent {
    USER_CHANGE = 'user/change',
    APP_CHANGE = 'app/change',
    APP_NEW = 'app/new',
    APP_REMOVE = 'app/remove',
    APP_USER_CHANGE = 'app-user/change',
    APP_USER_PONG = 'app-user/pong',
    APP_USER_NEW = 'app-user/new',
    APP_USER_REMOVE = 'app-user/remove',
    APP_USER_GROUP_CHANGE = 'app-user-group/change',
    APP_USER_GROUP_NEW = 'app-user-group/new',
    APP_USER_GROUP_REMOVE = 'app-user-group/remove',
    APP_USER_INVITE_CHANGE = 'app-user-invite/change',
    APP_USER_INVITE_NEW = 'app-user-invite/new',
    APP_USER_INVITE_REMOVE = 'app-user-invite/remove',
    APP_USER_CHAT_MESSAGE_NEW = 'app-user-chat-message/new',
    CHAT_USER_CHANGE = 'chat-user/change',
    APP_COMPONENT_CHANGE = 'app-component/change',
    APP_COMPONENT_NODE_CHANGE = 'app-component/node/change'
}