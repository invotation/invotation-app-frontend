import { Locale } from "../../enum/Locale"
import { BusinessBase } from "./BusinessBase"
import { ClientBase } from "./ClientBase"
import { Entity } from "./models/Entity"
import { QuotationBase } from "./QuotationBase"
import { TemplateBase } from "./TemplateBase"
import { UserBase } from "./UserBase"

/**
 * The base for an invoice
 */
export interface InvoiceBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * The number of the invoice
     */
    invoiceNumber: string

    /**
     * The quotation associated, if any
     */
    quotation: QuotationBase

    /**
     * The client associated
     */
    client: ClientBase

    /**
     * The business associated
     */
    business: BusinessBase

    /**
     * The template used for the invoice
     */
    template: TemplateBase

    /**
     * The date of the invoice
     */
    invoiceDate: Date

    /**
     * The due date
     */
    dueDate: Date

    /**
     * A JSON string containing all items on the invoice
     */
    items: string

    /**
     * Wheter or not to repeat the expense
     */
    intervalRepeat: boolean

    /**
     * Repeat x times per period
     */
    intervalEveryCount: number

    /**
     * Repeat period
     */
    intervalEveryPeriod: 'day' | 'week' | 'month' | 'year'

    /**
     * Whether or not to repeat for ever
     */
    intervalRepeatForever: boolean

    /**
     * The amount of repeats
     */
    intervalRepeatCount: number

    /**
     * The currency of the invoice
     */
    currency: string

    /**
     * The locale of the invoice
     */
    locale: Locale
}