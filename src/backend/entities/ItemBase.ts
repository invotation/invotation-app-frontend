import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * The base for an item
 */
export interface ItemBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * Name of the item
     */
    name: string

    /**
     * Unit of the item, e.g.: pc
     */
    unit: string

    /**
     * Price excluding VAT
     */
    priceExclVAT: number

    /**
     * Vat percentage
     */
    vatPercentage: number
}