import { Locale } from "../../enum/Locale"
import { BusinessBase } from "./BusinessBase"
import { ClientBase } from "./ClientBase"
import { Entity } from "./models/Entity"
import { TemplateBase } from "./TemplateBase"
import { UserBase } from "./UserBase"

/**
 * The base for a quotation.
 * @author Stan Hurks
 */
export interface QuotationBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * The number of the quotation
     */
    quotationNumber: string

    /**
     * The client associated
     */
    client: ClientBase

    /**
     * The business associated
     */
    business: BusinessBase

    /**
     * The template used for the quotation
     */
    template: TemplateBase

    /**
     * The date of the quotation
     */
    quotationDate: Date

    /**
     * The due date
     */
    dueDate: Date

    /**
     * A JSON string containing all items on the quotation
     */
    items: string

    /**
     * Wheter or not to repeat the expense
     */
    intervalRepeat: boolean

    /**
     * Repeat x times per period
     */
    intervalEveryCount: number

    /**
     * Repeat period
     */
    intervalEveryPeriod: 'day' | 'week' | 'month' | 'year'

    /**
     * Whether or not to repeat for ever
     */
    intervalRepeatForever: boolean

    /**
     * The amount of repeats
     */
    intervalRepeatCount: number

    /**
     * The currency of the quotation
     */
    currency: string

    /**
     * The locale of the quotation
     */
    locale: Locale

    /**
     * The link to the signature in storage
     */
    signature: string

    /**
     * The date at which the document was signed
     */
    signedAt: Date

    /**
     * The date at which the document was declined
     */
    declinedAt: Date

    /**
     * The IP used when signing the document
     */
    signatureIp: string

    /**
     * The user agent used when signing the document
     */
    signatureUserAgent: string
}