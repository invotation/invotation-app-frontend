/**
 * Represents a database entity.
 * @author Anonymous
 */
export interface Entity {
    /**
     * The unique identifier for the entity.
     */
    uuid: string
}