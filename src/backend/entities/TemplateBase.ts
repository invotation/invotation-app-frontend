import { BusinessBase } from "./BusinessBase"
import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * The base for a template
 * @author Stan Hurks
 */
export interface TemplateBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * The business associated
     */
    business: BusinessBase

    /**
     * The name of the template
     */
    name: string

    /**
     * The ID of the template used
     */
    templateId: number

    /**
     * The color used as primary
     */
    primaryColor: string

    /**
     * The color used for the text on primary
     */
    primaryTextColor: string

    /**
     * The color used as dark
     */
    darkColor: string

    /**
     * The color used for the text on dark
     */
    darkTextColor: string

    /**
     * The color used as light
     */
    lightColor: string

    /**
     * The color used as text on light
     */
    lightTextColor: string

    /**
     * The link to the logo in storage
     */
    logo: string

    /**
     * The link to the signature in storage
     */
    signature: string
}