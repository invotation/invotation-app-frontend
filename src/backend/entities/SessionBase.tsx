import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * Represents the base of a session, to be seen in the front-end.
 * @author Anonymous
 */
export interface SessionBase extends Entity {
    /**
     * The user bound to the session
     */
    user: UserBase
}