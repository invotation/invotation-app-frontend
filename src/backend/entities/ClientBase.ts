import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * The base for a client
 * @author Stan Hurks
 */
export interface ClientBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * Name of the client
     */
    name: string

    /**
     * Email of the client
     */
    email: string

    /**
     * Country of the client
     */
    country: string
    
    /**
     * Address line 1 of the client
     */
    street1: string

    /**
     * Address line 2 of the client
     */
    street2: string

    /**
     * Zipcode of the client
     */
    zipCode: string

    /**
     * City of the client
     */
    city: string

    /**
     * Province/state of the client
     */
    provinceState: string
}