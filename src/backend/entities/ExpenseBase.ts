import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * The base for an expense
 * @author Stan Hurks
 */
export interface ExpenseBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * The price excluding VAT
     */
    priceExclVAT: number

    /**
     * The percentage of VAT
     */
    vatPercentage: number

    /**
     * The invoice date
     */
    invoiceDate: Date

    /**
     * Wheter or not to repeat the expense
     */
    intervalRepeat: boolean

    /**
     * Repeat x times per period
     */
    intervalEveryCount: number

    /**
     * Repeat period
     */
    intervalEveryPeriod: 'day' | 'week' | 'month' | 'year'

    /**
     * Whether or not to repeat for ever
     */
    intervalRepeatForever: boolean

    /**
     * The amount of repeats
     */
    intervalRepeatCount: number
}