import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * Represents the base of a business
 * @author Stan Hurks
 */
export interface BusinessBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * Name of the business
     */
    name: string

    /**
     * Country of the business
     */
    country: string

    /**
     * Address line 1
     */
    street1: string

    /**
     * Address line 2
     */
    street2: string

    /**
     * The zip code of the business
     */
    zipCode: string

    /**
     * The city of the business
     */
    city: string
    
    /**
     * The province/state of the business
     */
    provinceState: string

    /**
     * The coc number of the business
     */
    cocNumber: string

    /**
     * The vat number of the business
     */
    vatNumber: string

    /**
     * The email of the business
     */
    email: string

    /**
     * The phone number of the business
     */
    phone: string

    /**
     * The website of the businesss
     */
    website: string
    
    /**
     * The bank code
     */
    bankCode: string

    /**
     * The account number of the bank account
     */
    accountNumber: string

    /**
     * Whether or not to accept online payments for invoices
     */
    acceptOnlinePayments: boolean

    /**
     * The public key of mollie
     */
    molliePublicKey: string
}