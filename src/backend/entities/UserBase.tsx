import { Locale } from "../../enum/Locale"
import { BusinessBase } from "./BusinessBase"
import { Entity } from "./models/Entity"

/**
 * Represents a user and its public fields to be seen by the front-end
 * @author Anonymous
 */
export interface UserBase extends Entity {
    /**
     * The e-mailaddress of the user
     */
    email: string

    /**
     * The locale of the user
     */
    locale: Locale

    /**
     * Whether or not the user has activated the account
     */
    activated: boolean

    /**
     * The default business of the user
     */
    defaultBusiness: BusinessBase
}