import { Entity } from "./models/Entity"
import { UserBase } from "./UserBase"

/**
 * The base for a subscription
 * @author Stan Hurks
 */
export interface SubscriptionBase extends Entity {
    /**
     * The user associated
     */
    user: UserBase

    /**
     * The type of subscription
     */
    type: 'free trial' | 'small business' | 'business' | 'large business' | 'enterprise'

    /**
     * The start date
     */
    startDate: Date

    /**
     * The end date
     */
    endDate: Date

    /**
     * The costs excluding VAT
     */
    costs: number

    /**
     * The VAT percentage
     */
    vatPercentage: number

    /**
     * Whether or not the payment is declined
     */
    paymentDeclined: boolean
}