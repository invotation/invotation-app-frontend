import { Subject } from "rxjs"
import { HttpResponse } from "./HttpResponse"
import Storage from "../storage/Storage"
import { Objects } from "../core/Objects"
import { ResponseCode } from "./responses/base/ResponseCode"
import Loader from "../components/loader/Loader"

/**
 * All core functionality to connect the GraphQL server with the front-end.
 * 
 * Also contains subjects for when the API is not available / offline / 500 / 401.
 * 
 * @author Anonymous
 */
export default  class GraphQL {
    /**
     * Perform a GraphQL query
     * @param query the query
     */
    public static query<T = {}>(query: string, headers?: any): Promise<HttpResponse<T>> {
        return this.request({
            query
        }, headers || this.generateHeaders())
    }

    /**
     * Whenever the API is not reachable
     */
    public static readonly apiUnreachable: Subject<void> = new Subject()

    /**
     * Whenever the user is unauthorized
     */
    public static readonly unauthorized: Subject<void> = new Subject()

    /**
     * Whenever the user is forbidden
     */
    public static readonly forbidden: Subject<void> = new Subject()

    /**
     * Whenever the server has an error
     */
    public static readonly internalServerError: Subject<void> = new Subject()

    /**
     * Whenever there has been too many requests to the server
     */
    public static readonly tooManyRequests: Subject<void> = new Subject()

    /**
     * Whenever the user filled in an invalid code
     */
    public static readonly invalidCode: Subject<void> = new Subject()

    /**
     * Whenever the user filled in the wrong credentials
     */
    public static readonly invalidCredentials: Subject<void> = new Subject()

    /**
     * Whenever the progress percentage changes in the requests it will be fired here.
     */
    public static readonly requestProgressPercentage: Subject<number> = new Subject()

    /**
     * The percentages of which every pending XMLHttpRequest is done
     */
    private static readonly requestPercentages: { [index: number]: number } = {}

    /**
     * The HTTP options
     */
    public static options = {
        base: `${window.location.protocol}//${window.location.hostname}:4000`
    }

    /**
     * The amount of requests done
     */
    private static requestCounter: number = 0

    /**
     * Performs a XMLHttpRequest based on a GraphQL query
     * @param payload the payload (GraphQL query syntax)
     * @param headers optional: additional request headers
     */
    private static request = (
        payload: {
            /**
             * The query to perform in GraphQL
             */
            query?: string
        },
        headers?: { [key: string]: string }
    ): Promise<HttpResponse<any>> => {
        Loader.enable.next()
        return new Promise((resolve, reject) => {
            // Initialize the request
            const request = new XMLHttpRequest()
            request.open('POST', GraphQL.options.base)
            request.responseType = 'json'

            // Send the request headers GraphQL asks for
            request.setRequestHeader('Content-Type', 'application/json')

            // Optionally overwrite headers
            if (headers != null) {
                Object.keys(headers).forEach((key) => {
                    request.setRequestHeader(key, headers[key])
                })
            }

            // Initialize the request so a percentage loader can be displayed
            const requestId = GraphQL.requestCounter++
            GraphQL.requestPercentages[requestId] = 0

            // Send the load percentage to the subject
            GraphQL.sendLoadPercentageToSubject()

            // Whenever the request progresses
            request.onprogress = (event: ProgressEvent) => {

                // Update the percentage
                GraphQL.requestPercentages[requestId] = (event.loaded / event.total)

                // Send the load percentage to the subject
                GraphQL.sendLoadPercentageToSubject()
            }

            // Whenever the request is done
            request.onload = () => {

                // Remove the load percentage from the array
                delete GraphQL.requestPercentages[requestId]

                // Send the load percentage to the subject
                GraphQL.sendLoadPercentageToSubject()

                const headersArray = request.getAllResponseHeaders()
                    .split(/[\r\n]+/)
                    .filter((v) => v.length)

                const headers: any = {}
                for (const headersArrayPart of headersArray) {
                    const headersArrayParts = headersArrayPart.split(':').map((v) => v.trim())
                    headers[headersArrayParts[0].toLowerCase()] = headersArrayParts[1]
                }

                if (request.status < 400) {
                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }
                    
                    if (response.data === null && response.errors) {
                        for (const error of response.errors) {
                            if (error.message && error.message.indexOf('Access denied!') !== -1) {
                                GraphQL.unauthorized.next()
                                Loader.disable.next()
                                reject({
                                    data: response,
                                    status: request.status,
                                    headers
                                })
                                return
                            }
                        }
                    } else {
                        for (const property of Object.keys(response.data)) {
                            if (Objects.isObject(response.data[property]) && response.data[property].code !== undefined) {
                                const code: ResponseCode = response.data[property].code
                                switch (code) {
                                    case ResponseCode.FORBIDDEN:
                                        GraphQL.forbidden.next()
                                        break
                                    case ResponseCode.UNAUTHORIZED:
                                        GraphQL.unauthorized.next()
                                        break
                                    case ResponseCode.INTERNAL_SERVER_ERROR:
                                        GraphQL.internalServerError.next()
                                        break
                                    case ResponseCode.TOO_MANY_REQUESTS:
                                        GraphQL.tooManyRequests.next()
                                        break
                                    case ResponseCode.INVALID_CODE:
                                        GraphQL.invalidCode.next()
                                        break
                                    case ResponseCode.INVALID_CREDENTIALS:
                                        GraphQL.invalidCredentials.next()
                                        break
                                }   
                            }
                        }
                    }
                    Loader.disable.next()
                    resolve({
                        data: response.data,
                        status: request.status,
                        headers
                    })
                } else {
                    if (request.status === 401) {
                        GraphQL.unauthorized.next()
                    } else if (request.status === 500) {
                        GraphQL.internalServerError.next()
                    }

                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }

                    Loader.disable.next()
                    // eslint-disable-next-line
                    reject({
                        data: response,
                        status: request.status,
                        headers
                    })
                }
            }

            // When the request errors out
            request.onerror = () => {
                GraphQL.apiUnreachable.next()
                Loader.disable.next()
                reject(request.statusText)
            }

            // Send the request
            request.send(JSON.stringify(payload))
        })
    }

    /**
     * Serializes a string
     * @param string the string to serialize
     */
    public static serializeString = (string: string|null|undefined): string => {
        if (!string) {
            return 'null'
        }
        return `"${string.replace(/"/g, '\\"')}"`
    }

    /**
     * Generate the headers for a http request
     */
    public static generateHeaders = (): any => {
        if (Storage.data.session.uuid) {
            return {
                'Authorization': Storage.data.session.uuid
            }
        }
        return {}
    }

    /**
     * Get the current load percentage for the pending requests
     */
    public static getCurrentLoadPercentage = () => {
        const values = Object.keys(GraphQL.requestPercentages)
            .map((i) => GraphQL.requestPercentages[i as any])
            .filter((p) => p < 1)

        if (values.length === 0) {
            return 100
        }

        return values.reduce((a, b) => a + b, 0) / values.length * 100
    }

    /**
     * Sends the load percentage to the subject
     */
    private static sendLoadPercentageToSubject = () => {
        GraphQL.requestProgressPercentage.next(
            GraphQL.getCurrentLoadPercentage()
        )
    }
}