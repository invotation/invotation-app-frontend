import socketIO from 'socket.io-client'
import Storage from '../storage/Storage'
import { Subject } from 'rxjs'
import { UserBase } from './entities/UserBase'
import { WebSocketEvent } from './web-socket/WebSocketEvent'

/**
 * The Socket.IO based web socket functionality.
 * @author Anonymous
 */
export default class WebSocket {
    /**
     * Whenever the web socket has connected
     */
    public static connected: Subject<void> = new Subject()

    /**
     * Whenever the web socket has disconnected
     */
    public static disconnected: Subject<void> = new Subject()

    /**
     * User entity subjects
     */
    public static userChange: Subject<UserBase> = new Subject()

    /**
     * The socket ID
     */
    public static id: string|null = null

    /**
     * The client-side socket
     */
    public static socket: SocketIOClient.Socket | null = null

    /**
     * Ping with the server to obtain online status
     */
    private static ping: NodeJS.Timeout|null = null

    /**
     * Connect to the Socket.IO server
     */
    public static connect = () => {
        // Close the connection with the web socket manually, if applicable
        if (WebSocket.socket !== null) {
            WebSocket.socket.removeAllListeners()
            WebSocket.socket.close()
            WebSocket.socket = null
            console.log('Websocket listeners removed and connection closed.')
        }

        // Reconnect to the Sock.IO server and the appropriate rooms
        if (Storage.data.session.uuid) {

            // Connect to the server
            if (WebSocket.socket === null) {
                WebSocket.socket = socketIO.connect(`${window.location.protocol}//${window.location.hostname}:4001?token=${Storage.data.session.uuid}`)
                console.log('Websocket connection initialized.')
            }

            // Reconnect when disconnected
            WebSocket.socket.on('disconnect', () => {
                
                // Broadcast
                WebSocket.disconnected.next()

                // Reset the ID
                WebSocket.id = null

                // Clear the ping until reconnect
                if (WebSocket.ping) clearInterval(WebSocket.ping)

                // Attempt to reconnect
                console.log('Websocket disconnected, attempting to reestablish the connection with Socket.IO')
                WebSocket.connect()
            })

            // Connected to the server
            WebSocket.socket.on('connect', () => {

                // Set the ID
                WebSocket.id = WebSocket.socket!.id

                // Receive feedback from server
                WebSocket.socket!.on('message', (data: any) => console.log(data))

                // Join the room for the user
                // TODO

                // Subscribe to all topics and link them with RxJS subjects
                WebSocket.socket!.on(WebSocketEvent.USER_CHANGE, (payload: UserBase) => {
                    WebSocket.userChange.next(payload)
                })

                // Broadcast
                WebSocket.connected.next()
            })
        }
    }
}