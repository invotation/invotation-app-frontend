import React from 'react'
import './login-page.scss'
import background from './background.jpeg'
import classNames from 'classnames'
import Device from '../../core/Device'
import FormButton from '../../components/form-button/FormButton'
import FormGroup from '../../components/form-group/FormGroup'
import FormInput from '../../components/form-input/FormInput'
import { KeyboardBackspace } from '@material-ui/icons'
import FormCheckbox from '../../components/form-checkbox/FormCheckbox'
import FormSelect from '../../components/form-select/FormSelect'
import { Locale } from '../../enum/Locale'
import Storage from '../../storage/Storage'
import Translations from '../../translations/Translations'
import App from '../../App'
import GraphQL from '../../backend/GraphQL'
import Validation from '../../core/Validation'
import { SessionBase } from '../../backend/entities/SessionBase'
import { ResponseCode } from '../../backend/responses/base/ResponseCode'
import { Route } from '../../enum/Route'
import { DataResponseBase } from '../../backend/responses/base/DataResponseBase'
import Modal from '../../components/modals/Modal'
import ErrorModal from '../../components/modals/error/ErrorModal'
import { ResponseBase } from '../../backend/responses/base/ResponseBase'
import InfoModal from '../../components/modals/info/InfoModal'
import { RouteComponentProps } from 'react-router'

/**
 * All form types
 */
type LoginPageFormType = 'login' | 'register' | 'forgot-password' | 'reset-password'

/**
 * The props
 */
interface LoginPageProps {
    /**
     * The initial form
     */
    currentForm?: LoginPageFormType
}

/**
 * The state
 */
interface LoginPageState {
    marginTopForm: number
    initializedMargin: boolean
    currentForm: LoginPageFormType
    nextForm: LoginPageFormType | null
    fromIndex: number | null
    toIndex: number | null
    formData: {
        language: Locale
        email: string
        password: string
        acceptedTermsAndConditions: boolean
    }
    passwordReset: boolean
    resettedPassword: boolean
    registered: boolean
    code: string|null
}

/**
 * The login page
 * 
 * @author Stan Hurks
 */
export default class LoginPage extends React.Component<LoginPageProps & RouteComponentProps<{code: string}>, LoginPageState> {
    private readonly cubeSpeedMS: number = 500

    private formRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            marginTopForm: 0,
            initializedMargin: false,
            currentForm: props.currentForm || 'login',
            nextForm: null,
            fromIndex: null,
            toIndex: null,
            formData: {
                language: Storage.data.locale,
                email: '',
                password: '',
                acceptedTermsAndConditions: false
            },
            passwordReset: false,
            registered: false,
            resettedPassword: false,
            code: null
        }
    }

    public componentDidMount = () => {
        window.addEventListener('resize', this.onResize as any)

        setTimeout(() => {
            const params = this.props.match.params
            if (params && params.code) {
                this.setState({
                    code: params.code
                })
            }

            App.setCurrentRoute.next({
                route: Route.LOGIN
            })
            this.onResize()
        })
    }

    public componentWillUnmount = () => {
        window.removeEventListener('resize', this.onResize as any)
    }

    public render = () => {
        return (
            <div className="login-page" style={{
                backgroundImage: `url(${background})` 
            }}>
                <div className="login-page-locale">
                    <FormSelect label='Select' value={this.state.formData.language} onChange={(language) => {
                        this.setState({
                            formData: {
                                ...this.state.formData,
                                language: language as Locale
                            }
                        })
                        App.setLocale.next(language as Locale)
                    }} options={[
                        {
                            label: 'English',
                            value: Locale.EN_GB
                        },
                        {
                            label: 'Nederlands',
                            value: Locale.NL_NL
                        }
                    ]} />
                </div>

                <div className={classNames({
                    'login-page-form': true,
                    'animation-cube': true,
                    'animation-cube-left-to-right': this.state.toIndex !== null && this.state.fromIndex !== null
                            && this.state.toIndex < this.state.fromIndex,
                    'animation-cube-right-to-left': this.state.toIndex !== null && this.state.fromIndex !== null
                        && this.state.toIndex > this.state.fromIndex
                })} ref={this.formRef} style={{
                    marginTop: this.state.marginTopForm,
                    
                    ...this.state.initializedMargin
                    && {
                        transition: `all ${this.cubeSpeedMS}ms ease-in-out`
                    }
                }}>
                    <div className="cube">
                        {
                            this.renderForm(this.state.currentForm, true)
                        }
                        {
                            this.state.nextForm &&
                            this.renderForm(this.state.nextForm)
                        }
                    </div>
                </div>
            </div>
        )
    }

    private renderForm = (formType: LoginPageFormType, active: boolean = false) => {
        const form = formType === 'login'
            ? this.renderLoginForm()
            : (
                formType === 'register'
                ? this.renderRegisterForm()
                : (
                    formType === 'forgot-password'
                    ? this.renderForgotPasswordForm()
                    : this.renderResetPasswordForm()    
                )
            )
        return (
            <div className={classNames({
                'active': active,
                'inactive': !active
            })}>
                <div className="login-page-form-content">
                    <div className="login-page-form-content-title">
                        <span className="capital">Invo</span>
                        <span className="capital primary">tation</span>
                    </div>
                    <div className="hr primary"></div>
                    <div className="login-page-form-content-content">
                        {
                            form
                        }
                    </div>
                </div>
            </div>
        )
    }

    private renderLoginForm = () => {
        return (
            <div className="form">
                <div className="form-top">
                    <FormGroup label={Translations.translations.pages.login.forms.login.email.label}>
                        <FormInput type="email" useLastPass value={this.state.formData.email} onChange={(email) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    email
                                }
                            })
                        }} />
                    </FormGroup>
                    <FormGroup label={Translations.translations.pages.login.forms.login.password.label}>
                        <FormInput type="password" useLastPass value={this.state.formData.password} onChange={(password) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    password
                                }
                            })
                        }} onEnter={() => {
                            if (!this.validateLogin()) {
                                return
                            }
                            this.login()
                        }} />
                    </FormGroup>
                    <div className="form-top-forgot-password" onClick={() => {
                        this.switchForm('forgot-password')
                    }}>
                        {Translations.translations.pages.login.forms.login.forgotPassword.label}
                    </div>
                </div>
                <div className="form-bottom">
                    <div className="form-bottom-left">
                        <FormButton variant="white" onClick={() => {
                            this.switchForm('register')
                        }}>
                            {Translations.translations.pages.login.forms.login.registerButton}
                        </FormButton>
                    </div>
                    <div className="form-bottom-right">
                        <FormButton variant="primary" disabled={!this.validateLogin()} onClick={() => {
                            this.login()
                        }}>
                            {Translations.translations.pages.login.forms.login.loginButton}
                        </FormButton>
                    </div>
                </div>
            </div>
        )
    }

    private renderForgotPasswordForm = () => {
        return (
            <div className="form">
                <div className="form-top">
                    <FormGroup label={Translations.translations.pages.login.forms.forgotPassword.email.label}>
                        <FormInput type="email" value={this.state.formData.email} onChange={(email) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    email
                                }
                            })
                        }} onEnter={() => {
                            if (!this.validateForgotPassword()) {
                                return
                            }
                            this.forgotPassword()
                        }} />
                    </FormGroup>
                </div>
                <div className="form-bottom">
                    <div className="form-bottom-left">
                        <FormButton variant="white" onClick={() => {
                            this.switchForm('login')
                        }}>
                            <KeyboardBackspace />
                        </FormButton>
                    </div>
                    <div className="form-bottom-right">
                        <FormButton variant="primary" disabled={!this.validateForgotPassword()} onClick={() => {
                            this.forgotPassword()
                        }}>
                            {Translations.translations.pages.login.forms.forgotPassword.resetPasswordButton}
                        </FormButton>
                    </div>
                </div>
            </div>
        )
    }

    private renderResetPasswordForm = () => {
        return (
            <div className="form">
                <div className="form-top">
                    <FormGroup label={Translations.translations.pages.login.forms.resetPassword.code.label}>
                        <FormInput value={this.state.code!} onChange={() => {}} disabled={true} />
                    </FormGroup>
                    <FormGroup label={Translations.translations.pages.login.forms.resetPassword.email.label}>
                        <FormInput type="email" value={this.state.formData.email} onChange={(email) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    email
                                }
                            })
                        }} />
                    </FormGroup>
                    <FormGroup label={Translations.translations.pages.login.forms.resetPassword.password.label}>
                        <FormInput type="password" value={this.state.formData.password} onChange={(password) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    password
                                }
                            })
                        }} onEnter={() => {
                            if (!this.validateResetPassword()) {
                                return
                            }
                            this.resetPassword()
                        }} />
                    </FormGroup>
                </div>
                <div className="form-bottom">
                    <div className="form-bottom-left"></div>
                    <div className="form-bottom-right">
                        <FormButton variant="primary" disabled={!this.validateResetPassword()} onClick={() => {
                            this.resetPassword()
                        }}>
                            {Translations.translations.pages.login.forms.resetPassword.resetPasswordButton}
                        </FormButton>
                    </div>
                </div>
            </div>
        )
    }

    private renderRegisterForm = () => {
        return (
            <div className="form">
                <div className="form-top">
                    <FormGroup label={Translations.translations.pages.login.forms.register.email.label}>
                        <FormInput type="email" value={this.state.formData.email} onChange={(email) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    email
                                }
                            })
                        }} />
                    </FormGroup>
                    <FormGroup label={Translations.translations.pages.login.forms.register.password.label}>
                        <FormInput type="password" value={this.state.formData.password} onChange={(password) => {
                            this.setState({
                                formData: {
                                    ...this.state.formData,
                                    password
                                }
                            })
                        }} onEnter={() => {
                            if (!this.validateRegister()) {
                                return
                            }
                            this.register()
                        }} />
                    </FormGroup>
                    <FormCheckbox value={this.state.formData.acceptedTermsAndConditions} onChange={(acceptedTermsAndConditions) => {
                        this.setState({
                            formData: {
                                ...this.state.formData,
                                acceptedTermsAndConditions
                            }
                        })
                    }}>
                        {Translations.translations.pages.login.forms.register.termsAndConditions('https://invotation.com/terms-and-conditions.pdf')}
                    </FormCheckbox>
                </div>
                <div className="form-bottom">
                    <div className="form-bottom-left">
                        <FormButton variant="white" onClick={() => {
                            this.switchForm('login')
                        }}>
                            <KeyboardBackspace />
                        </FormButton>
                    </div>
                    <div className="form-bottom-right">
                        <FormButton variant="primary" disabled={!this.validateRegister()} onClick={() => {
                            this.register()
                        }}>
                            {Translations.translations.pages.login.forms.register.registerButton}
                        </FormButton>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Handler for resize
     */
    private onResize = () => {
        if (!this.formRef.current) {
            return
        }
        if (window.innerWidth <= Device.mobileResolution) {
            this.setState({
                marginTopForm: 0,
                initializedMargin: false
            })
            return
        }
        const inactive: boolean = this.state.toIndex !== null
        const el = this.formRef.current.querySelector('.' + (inactive ? 'inactive' : 'active') + ' .login-page-form-content')
        if (!el) {
            return
        }
        this.setState({
            marginTopForm: -el.getBoundingClientRect().height / 2
        }, () => {
            if (!this.state.initializedMargin) {
                setTimeout(() => {
                    this.setState({
                        initializedMargin: true
                    })
                })
            }
        })
    }

    /**
     * Switches the form
     * @param formName the form name
     */
    private switchForm = (formName: LoginPageFormType) => {
        this.setState({
            toIndex: formName === 'login'
                ? 0
                : 1,
            fromIndex: formName === 'login'
                ? 1
                : 0,
            nextForm: formName
        }, () => {
            this.onResize()
            setTimeout(() => {
                if (!this.state.nextForm) {
                    return
                }
                this.setState({
                    currentForm: this.state.nextForm,
                    nextForm: null,
                    toIndex: null,
                    fromIndex: null
                })
            }, this.cubeSpeedMS)
        })
    }

    /**
     * Login to the application
     */
    private login = () => {
        if (!this.validateLogin()) {
            return
        }
        GraphQL.query<{
            authorize: {
                code: string
                data: SessionBase
            }
        }>(`
            mutation{
                authorize(input:{
                    email: ${GraphQL.serializeString(this.state.formData.email)},
                    password: ${GraphQL.serializeString(this.state.formData.password)}
                }){
                    code
                    data{
                        uuid
                        user {
                            uuid
                            locale
                            email
                            activated
                        }
                    }
                }
            }
        `).then((data) => {
            if (data.data.authorize.code === ResponseCode.OK) {
                Storage.data.session.uuid = data.data.authorize.data.uuid
                Storage.data.session.user.uuid = data.data.authorize.data.user.uuid
                Storage.data.session.user.locale = data.data.authorize.data.user.locale
                Storage.data.session.user.email = data.data.authorize.data.user.email
                Storage.data.session.user.activated = data.data.authorize.data.user.activated
            
                App.redirect.next({
                    route: Route.DASHBOARD
                })

                App.updateUserAndSubscription.next()
            }
        })
    }

    /**
     * Register
     */
    private register = () => {
        if (!this.validateRegister()) {
            return
        }
        GraphQL.query<{
            register: DataResponseBase<SessionBase>
        }>(`
            mutation {
                register(input:{
                    locale: ${GraphQL.serializeString(this.state.formData.language)},
                    email: ${GraphQL.serializeString(this.state.formData.email)},
                    password: ${GraphQL.serializeString(this.state.formData.password)}
                }){
                    code
                    data{
                        uuid
                        user {
                            uuid
                            locale
                            email
                            activated
                        }
                    }
                }
            }
        `).then((data) => {
            switch (data.data.register.code) {
                case ResponseCode.EMAIL_EXISTS:
                    Modal.mount.next((
                        <ErrorModal
                            title={Translations.translations.api.mutations.register.emailExists.title}
                            content={Translations.translations.api.mutations.register.emailExists.content}
                            />
                    ))
                    break
                case ResponseCode.OK:
                    this.setState({
                        registered: true
                    })

                    Storage.data.session.uuid = data.data.register.data!.uuid
                    Storage.data.session.user.uuid = data.data.register.data!.user.uuid
                    Storage.data.session.user.locale = data.data.register.data!.user.locale
                    Storage.data.session.user.email = data.data.register.data!.user.email
                    Storage.data.session.user.activated = data.data.register.data!.user.activated
                
                    App.redirect.next({
                        route: Route.DASHBOARD
                    })

                    App.updateUserAndSubscription.next()
                    break
            }
        })
    }

    /**
     * Forgot password
     */
    private forgotPassword = () => {
        if (!this.validateForgotPassword()) {
            return
        }
        GraphQL.query<{
            resetPassword: ResponseBase
        }>(`
            mutation {
                resetPassword(input:{
                    email: ${GraphQL.serializeString(this.state.formData.email)}
                }){
                    code
                }
            }
        `).then((data) => {
            switch (data.data.resetPassword.code) {
                case ResponseCode.EMAIL_NOT_IN_USE:
                    Modal.mount.next((
                        <ErrorModal
                            title={Translations.translations.api.mutations.resetPassword.emailNotInUse.title}
                            content={Translations.translations.api.mutations.resetPassword.emailNotInUse.content}
                            />
                    ))
                    break
                case ResponseCode.OK:
                    this.setState({
                        passwordReset: true
                    })
                    Modal.mount.next((
                        <InfoModal
                            title={Translations.translations.api.mutations.resetPassword.ok.title}
                            content={Translations.translations.api.mutations.resetPassword.ok.content}
                            button={(
                                <FormButton variant="primary" onClick={() => {
                                    Modal.unmount.next()
                                }}>
                                    {Translations.translations.api.mutations.resetPassword.ok.button}
                                </FormButton>
                            )}
                            />
                    ))
                    break
            }
        })
    }

    /**
     * Reset the password
     */
    private resetPassword = () => {
        GraphQL.query<{
            performResetPassword: DataResponseBase<SessionBase>
        }>(`
            mutation {
                performResetPassword(input:{
                    email: "${this.state.formData.email}",
                    code: "${this.state.code}",
                    password: "${this.state.formData.password}"
                }){
                    code
                    data{
                        uuid
                        user {
                            uuid
                            locale
                            email
                            activated
                        }
                    }
                }
            }
        `).then((data) => {
            switch (data.data.performResetPassword.code) {
                case ResponseCode.EMAIL_NOT_IN_USE:
                    Modal.mount.next((
                        <ErrorModal
                            title={Translations.translations.api.mutations.performResetPassword.emailNotInUse.title}
                            content={Translations.translations.api.mutations.performResetPassword.emailNotInUse.content}
                            />
                    ))
                    break
                case ResponseCode.OK:
                    Modal.mount.next((
                        <InfoModal
                            title={Translations.translations.api.mutations.performResetPassword.ok.title}
                            content={Translations.translations.api.mutations.performResetPassword.ok.content}
                            button={(
                                <FormButton variant="primary" onClick={() => {
                                    Modal.unmount.next()
                                    
                                    Storage.data.session.uuid = data.data.performResetPassword.data!.uuid
                                    Storage.data.session.user.uuid = data.data.performResetPassword.data!.user.uuid
                                    Storage.data.session.user.locale = data.data.performResetPassword.data!.user.locale
                                    Storage.data.session.user.email = data.data.performResetPassword.data!.user.email
                                    Storage.data.session.user.activated = data.data.performResetPassword.data!.user.activated
                                
                                    App.redirect.next({
                                        route: Route.DASHBOARD
                                    })
    
                                    App.updateUserAndSubscription.next()
                                }}>
                                    {Translations.translations.api.mutations.performResetPassword.ok.button}
                                </FormButton>
                            )}
                            />
                    ))
                    break
            }
        })
    }

    /**
     * Validate the login
     */
    private validateLogin = (): boolean => {
        return this.state.formData.email.length > 0
            && this.state.formData.password.length > 0
            && Validation.validateEmail(this.state.formData.email)
    }

    /**
     * Validate the login
     */
    private validateRegister = (): boolean => {
        return this.state.formData.email.length > 0
            && this.state.formData.password.length > 0
            && Validation.validateEmail(this.state.formData.email)
            && this.state.formData.acceptedTermsAndConditions
            && !this.state.registered
    }

    /**
     * Validate the forgot password
     */
    private validateForgotPassword = (): boolean => {
        return this.state.formData.email.length > 0
            && Validation.validateEmail(this.state.formData.email)
            && !this.state.passwordReset
    }

    /**
     * Validate the reset password
     */
    private validateResetPassword = (): boolean => {
        return this.state.formData.password.length > 0
            && !this.state.passwordReset
            && this.state.formData.email.length > 0
            && Validation.validateEmail(this.state.formData.email)
            && !this.state.resettedPassword
    }
}