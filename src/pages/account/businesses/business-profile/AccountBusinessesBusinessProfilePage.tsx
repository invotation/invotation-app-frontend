import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import App from '../../../../App'
import { BusinessBase } from '../../../../backend/entities/BusinessBase'
import GraphQL from '../../../../backend/GraphQL'
import { DataResponseBase } from '../../../../backend/responses/base/DataResponseBase'
import { ResponseCode } from '../../../../backend/responses/base/ResponseCode'
import FormButton from '../../../../components/form-button/FormButton'
import FormGroup from '../../../../components/form-group/FormGroup'
import FormInput from '../../../../components/form-input/FormInput'
import PageTitle from '../../../../components/page-title/PageTitle'
import Page from '../../../../components/page/Page'
import Validation from '../../../../core/Validation'
import { Route } from '../../../../enum/Route'
import './account-businesses-business-profile-page.scss'

/**
 * The state
 */
interface AccountBusinessesBusinessProfilePageState {
    /**
     * The business
     */
    business: Partial<BusinessBase> | null

    /**
     * The edited business
     */
    editedBusiness: Partial<BusinessBase> | null
}

/**
 * The business profile page.
 * @author Stan Hurks
 */
export default class AccountBusinessesBusinessProfilePage extends React.Component<RouteComponentProps<{businessId: string}>, AccountBusinessesBusinessProfilePageState> {
    /**
     * The current route
     */
    private readonly currentRoute = {
        route: Route.ACCOUNT_BUSINESSES_PROFILE,
        previous: {
            route: Route.ACCOUNT_BUSINESSES
        }
    }
    
    constructor(props: any) {
        super(props)
        this.state = {
            business: null,
            editedBusiness: null
        }
    }

    public componentDidMount = () => {
        const businessId = this.props.match.params.businessId
        if (businessId === 'new') {
            const business = {
                uuid: null,
                name: '',
                country: '',
                street1: '',
                street2: '',
                zipCode: '',
                city: '',
                provinceState: '',
                cocNumber: '',
                vatNumber: '',
                email: '',
                phone: '',
                website: ''
            }
            this.setState({
                business: Object.assign({}, business as any),
                editedBusiness: Object.assign({}, business as any)
            })
        } else {
            this.updateBusiness()
        }

        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
    }

    public render = () => {
        return (
            <div className="account-businesses-business-profile-page">
                {
                    this.state.business !== null
                    && this.state.editedBusiness !== null
                    &&
                    <Page>
                        <PageTitle route={this.currentRoute.route} previous={this.currentRoute.previous} />
                        <div className="row first">
                            <FormGroup label="Business name" required>
                                <FormInput value={this.state.editedBusiness.name!} onChange={(name) => {
                                    this.editBusinessProperty('name', name)
                                }} />
                            </FormGroup>
                            <FormGroup label="Country" required>
                                <FormInput value={this.state.editedBusiness.country!} onChange={(country) => {
                                    this.editBusinessProperty('country', country)
                                }} />
                            </FormGroup>
                            <FormGroup label="Street 1" required>
                                <FormInput value={this.state.editedBusiness.street1!} onChange={(street1) => {
                                    this.editBusinessProperty('street1', street1)
                                }} />
                            </FormGroup>
                            <FormGroup label="Street 2">
                                <FormInput value={this.state.editedBusiness.street2!} onChange={(street2) => {
                                    this.editBusinessProperty('street2', street2)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormGroup label="Zip code">
                                <FormInput value={this.state.editedBusiness.zipCode!} onChange={(zipCode) => {
                                    this.editBusinessProperty('zipCode', zipCode)
                                }} />
                            </FormGroup>
                            <FormGroup label="City" required>
                                <FormInput value={this.state.editedBusiness.city!} onChange={(city) => {
                                    this.editBusinessProperty('city', city)
                                }} />
                            </FormGroup>
                            <FormGroup label="Province/state">
                                <FormInput value={this.state.editedBusiness.provinceState!} onChange={(provinceState) => {
                                    this.editBusinessProperty('provinceState', provinceState)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="hr"></div>
                        <div className="row">
                            <FormGroup label="Trade register">
                                <FormInput value={this.state.editedBusiness.cocNumber!} onChange={(cocNumber) => {
                                    this.editBusinessProperty('cocNumber', cocNumber)
                                }} />
                            </FormGroup>
                            <FormGroup label="VAT number">
                                <FormInput value={this.state.editedBusiness.vatNumber!} onChange={(vatNumber) => {
                                    this.editBusinessProperty('vatNumber', vatNumber)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="hr"></div>
                        <div className="row">
                            <FormGroup label="E-mail" required>
                                <FormInput type="email" value={this.state.editedBusiness.email!} onChange={(email) => {
                                    this.editBusinessProperty('email', email)
                                }} />
                            </FormGroup>
                            <FormGroup label="Phone">
                                <FormInput value={this.state.editedBusiness.phone!} onChange={(phone) => {
                                    this.editBusinessProperty('phone', phone)
                                }} />
                            </FormGroup>
                            <FormGroup label="Website">
                                <FormInput value={this.state.editedBusiness.website!} onChange={(website) => {
                                    this.editBusinessProperty('website', website)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormButton variant="primary" disabled={!this.hasChanges() || !this.validate()} onClick={() => {
                                if (this.state.business!.uuid) {
                                    this.putBusinessProfile()
                                } else {
                                    this.postBusinessProfile()
                                }
                            }}>
                                Save
                            </FormButton>
                        </div>
                    </Page>
                }
            </div>
        )
    }

    /**
     * Update the business
     */
    private updateBusiness = () => {
        const uuid = this.props.match.params.businessId
        GraphQL.query<{
            getBusiness: DataResponseBase<BusinessBase>
        }>(`
            query {
                getBusiness(input:{
                    uuid: ${GraphQL.serializeString(uuid)}
                }) {
                    code
                    data{
                        uuid
                        name
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                        cocNumber
                        vatNumber
                        email
                        phone
                        website
                    }
                }
            }
        `).then((data) => {
            switch (data.data.getBusiness.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        business: data.data.getBusiness.data!,
                        editedBusiness: data.data.getBusiness.data!
                    })
                    break
            }
        })
    }

    /**
     * PUT update the business on the server
     */
    private putBusinessProfile = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            putBusinessProfile: DataResponseBase<BusinessBase>
        }>(`
            mutation {
                putBusinessProfile(input:{
                    uuid: ${GraphQL.serializeString(this.state.editedBusiness!.uuid)},
                    name: ${GraphQL.serializeString(this.state.editedBusiness!.name)},
                    country: ${GraphQL.serializeString(this.state.editedBusiness!.country)},
                    street1: ${GraphQL.serializeString(this.state.editedBusiness!.street1)},
                    street2: ${GraphQL.serializeString(this.state.editedBusiness!.street2)},
                    zipCode: ${GraphQL.serializeString(this.state.editedBusiness!.zipCode)},
                    city: ${GraphQL.serializeString(this.state.editedBusiness!.city)},
                    provinceState: ${GraphQL.serializeString(this.state.editedBusiness!.provinceState)},
                    cocNumber: ${GraphQL.serializeString(this.state.editedBusiness!.cocNumber)},
                    vatNumber: ${GraphQL.serializeString(this.state.editedBusiness!.vatNumber)},
                    email: ${GraphQL.serializeString(this.state.editedBusiness!.email)},
                    phone: ${GraphQL.serializeString(this.state.editedBusiness!.phone)},
                    website: ${GraphQL.serializeString(this.state.editedBusiness!.website)}
                }) {
                    code
                    data{
                        uuid
                        name
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                        cocNumber
                        vatNumber
                        email
                        phone
                        website
                    }
                }
            }
        `).then((data) => {
            switch (data.data.putBusinessProfile.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        business: data.data.putBusinessProfile.data!,
                        editedBusiness: data.data.putBusinessProfile.data!
                    })
                    App.unsavedChanges.next(false)
                    break
            }
        })
    }

    /**
     * POST update the business on the server
     */
     private postBusinessProfile = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            postBusinessProfile: DataResponseBase<BusinessBase>
        }>(`
            mutation {
                postBusinessProfile(input:{
                    name: ${GraphQL.serializeString(this.state.editedBusiness!.name)},
                    country: ${GraphQL.serializeString(this.state.editedBusiness!.country)},
                    street1: ${GraphQL.serializeString(this.state.editedBusiness!.street1)},
                    street2: ${GraphQL.serializeString(this.state.editedBusiness!.street2)},
                    zipCode: ${GraphQL.serializeString(this.state.editedBusiness!.zipCode)},
                    city: ${GraphQL.serializeString(this.state.editedBusiness!.city)},
                    provinceState: ${GraphQL.serializeString(this.state.editedBusiness!.provinceState)},
                    cocNumber: ${GraphQL.serializeString(this.state.editedBusiness!.cocNumber)},
                    vatNumber: ${GraphQL.serializeString(this.state.editedBusiness!.vatNumber)},
                    email: ${GraphQL.serializeString(this.state.editedBusiness!.email)},
                    phone: ${GraphQL.serializeString(this.state.editedBusiness!.phone)},
                    website: ${GraphQL.serializeString(this.state.editedBusiness!.website)}
                }) {
                    code
                    data{
                        uuid
                        name
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                        cocNumber
                        vatNumber
                        email
                        phone
                        website
                    }
                }
            }
        `).then((data) => {
            switch (data.data.postBusinessProfile.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        business: data.data.postBusinessProfile.data!,
                        editedBusiness: data.data.postBusinessProfile.data!
                    })
                    App.unsavedChanges.next(false)
                    break
            }
        })
    }

    /**
     * Edit a business property
     */
    private editBusinessProperty = (propertyName: keyof BusinessBase, value: any) => {
        this.setState({
            editedBusiness: {
                ...this.state.editedBusiness,
                [propertyName]: value
            }
        }, () => {
            if (this.hasChanges()) {
                App.unsavedChanges.next(true)
            } else {
                App.unsavedChanges.next(false)
            }
        })
    }

    /**
     * Checks whether the business has changes
     */
    private hasChanges = (): boolean => {
        return Object.keys(this.state.business!)
            .map((v) => {
                const a = (this.state.editedBusiness as any)[v] || ''
                const b = (this.state.business as any)[v] || ''
                return a !== b
            })
            .filter((v) => v)
            .length > 0
    }

    /**
     * Validates the form
     */
    private validate = (): boolean => {
        return Boolean(
            this.state.editedBusiness!.name && this.state.editedBusiness!.name.length
            && this.state.editedBusiness!.country && this.state.editedBusiness!.country.length
            && this.state.editedBusiness!.street1 && this.state.editedBusiness!.street1.length
            && this.state.editedBusiness!.city && this.state.editedBusiness!.city.length
            && this.state.editedBusiness!.email && this.state.editedBusiness!.email.length
            && Validation.validateEmail(this.state.editedBusiness!.email)
        )
    }
}