import React from 'react'
import { RouteComponentProps } from 'react-router';
import App from '../../../../App';
import { BusinessBase } from '../../../../backend/entities/BusinessBase';
import GraphQL from '../../../../backend/GraphQL';
import { DataResponseBase } from '../../../../backend/responses/base/DataResponseBase';
import { ResponseCode } from '../../../../backend/responses/base/ResponseCode';
import FormGroup from '../../../../components/form-group/FormGroup';
import FormInput from '../../../../components/form-input/FormInput';
import FormSelect from '../../../../components/form-select/FormSelect';
import Page from '../../../../components/page/Page';
import { Route } from '../../../../enum/Route';
import paymentMethods from './payment-methods.png'
import './account-businesses-payment-options-page.scss'
import PageTitle from '../../../../components/page-title/PageTitle';
import FormButton from '../../../../components/form-button/FormButton';

/**
 * The state
 */
 interface AccountBusinessesPaymentOptionsPageState {
    /**
     * The business
     */
    business: Partial<BusinessBase> | null

    /**
     * The edited business
     */
    editedBusiness: Partial<BusinessBase> | null
}

/**
 * The payment options for a business.
 * @author Stan Hurks
 */
export default class AccountBusinessesPaymentOptionsPage extends React.Component<RouteComponentProps<{businessId: string}>, AccountBusinessesPaymentOptionsPageState> {
    /**
     * The current route
     */
    private readonly currentRoute = {
        route: Route.ACCOUNT_BUSINESSES_PAYMENTS,
        previous: {
            route: Route.ACCOUNT_BUSINESSES
        }
    }
    
    constructor(props: any) {
        super(props)
        this.state = {
            business: null,
            editedBusiness: null
        }
    }

    public componentDidMount = () => {
        this.updateBusiness()

        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
    }

    public render = () => {
        return (
            <div className="account-businesses-payment-options-page">
                {
                    this.state.business !== null
                    && this.state.editedBusiness !== null
                    &&
                    <Page>
                        <PageTitle route={this.currentRoute.route} previous={this.currentRoute.previous} />
                        <div className="row first">
                            <FormGroup label="Account number / IBAN">
                                <FormInput value={this.state.editedBusiness.accountNumber!} onChange={(accountNumber) => {
                                    this.editBusinessProperty('accountNumber', accountNumber)
                                }} />
                            </FormGroup>
                            <FormGroup label="Bank code / Swift / BIC">
                                <FormInput value={this.state.editedBusiness.bankCode!} onChange={(bankCode) => {
                                    this.editBusinessProperty('bankCode', bankCode)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="accept-online-payments">
                            <div className="accept-online-payments-title">
                                Accept online payments?
                            </div><div className="accept-online-payments-description">
                                A payment button will appear in your invoices.
                            </div><div className="accept-online-payments-image">
                                <img src={paymentMethods} alt="Payment methods" />
                            </div>
                        </div>
                        <div className="hr"></div>
                        <div className="row">
                            <div className="column">
                                <FormSelect value={String(this.state.editedBusiness.acceptOnlinePayments)} label="No" options={[
                                    {
                                        label: 'Yes',
                                        value: 'true'
                                    },
                                    {
                                        label: 'No',
                                        value: 'false'
                                    }
                                ]} onChange={(value) => {
                                    this.editBusinessProperty('acceptOnlinePayments', value === 'true')
                                }} />
                            </div>
                        </div>
                        {
                            this.state.editedBusiness.acceptOnlinePayments
                            &&
                            <div className="row">
                                <FormGroup label="Mollie.com public key" required>
                                    <FormInput value={this.state.editedBusiness.molliePublicKey!} onChange={(molliePublicKey) => {
                                        this.editBusinessProperty('molliePublicKey', molliePublicKey)
                                    }} />
                                </FormGroup>
                            </div>
                        }
                        <div className="row padding">
                            <FormButton variant="primary" disabled={!this.hasChanges() || !this.validate()} onClick={() => {
                                this.putBusinessPaymentOptions()
                            }}>
                                Save
                            </FormButton>
                        </div>
                    </Page>
                }
            </div>
        )
    }

    /**
     * Update the business
     */
    private updateBusiness = () => {
        const uuid = this.props.match.params.businessId
        GraphQL.query<{
            getBusiness: DataResponseBase<BusinessBase>
        }>(`
            query {
                getBusiness(input:{
                    uuid: ${GraphQL.serializeString(uuid)}
                }) {
                    code
                    data{
                        uuid
                        bankCode
                        accountNumber
                        acceptOnlinePayments
                        molliePublicKey
                    }
                }
            }
        `).then((data) => {
            switch (data.data.getBusiness.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        business: data.data.getBusiness.data!,
                        editedBusiness: data.data.getBusiness.data!
                    })
                    break
            }
        })
    }

    /**
     * PUT update the business on the server
     */
    private putBusinessPaymentOptions = () => {
        if (!this.hasChanges() || !this.validate()) {
            return
        }
        GraphQL.query<{
            putBusinessPaymentOptions: DataResponseBase<BusinessBase>
        }>(`
            mutation {
                putBusinessPaymentOptions(input:{
                    uuid: ${GraphQL.serializeString(this.state.editedBusiness!.uuid)},
                    bankCode: ${GraphQL.serializeString(this.state.editedBusiness!.bankCode)},
                    accountNumber: ${GraphQL.serializeString(this.state.editedBusiness!.accountNumber)},
                    acceptOnlinePayments: ${this.state.editedBusiness!.acceptOnlinePayments ? 'true' : 'false'},
                    molliePublicKey: ${GraphQL.serializeString(this.state.editedBusiness!.molliePublicKey)}
                }) {
                    code
                    data{
                        uuid
                        bankCode
                        accountNumber
                        acceptOnlinePayments
                        molliePublicKey
                    }
                }
            }
        `).then((data) => {
            switch (data.data.putBusinessPaymentOptions.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        business: data.data.putBusinessPaymentOptions.data!,
                        editedBusiness: data.data.putBusinessPaymentOptions.data!
                    })
                    App.unsavedChanges.next(false)
                    break
            }
        })
    }

    /**
     * Edit a business property
     */
    private editBusinessProperty = (propertyName: keyof BusinessBase, value: any) => {
        this.setState({
            editedBusiness: {
                ...this.state.editedBusiness,
                [propertyName]: value
            }
        }, () => {
            if (this.hasChanges()) {
                App.unsavedChanges.next(true)
            } else {
                App.unsavedChanges.next(false)
            }
        })
    }

    /**
     * Checks whether the business has changes
     */
    private hasChanges = (): boolean => {
        return Object.keys(this.state.business!)
            .map((v) => {
                const a = (this.state.editedBusiness as any)[v] || ''
                const b = (this.state.business as any)[v] || ''
                return a !== b
            })
            .filter((v) => v)
            .length > 0
    }

    /**
     * Validate the form
     */
    private validate = (): boolean => {
        if (this.state.editedBusiness!.acceptOnlinePayments) {
            return Boolean(
                this.state.editedBusiness!.molliePublicKey && this.state.editedBusiness!.molliePublicKey.length
            )
        }
        return true
    }
}