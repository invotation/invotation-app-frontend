import { Add } from '@material-ui/icons'
import React from 'react'
import App from '../../../../App'
import { BusinessBase } from '../../../../backend/entities/BusinessBase'
import GraphQL from '../../../../backend/GraphQL'
import { PaginatedResponseBase } from '../../../../backend/responses/base/PaginatedResponseBase'
import { ResponseBase } from '../../../../backend/responses/base/ResponseBase'
import { ResponseCode } from '../../../../backend/responses/base/ResponseCode'
import FormButton from '../../../../components/form-button/FormButton'
import ListView from '../../../../components/list-view/ListView'
import Menu from '../../../../components/menu/Menu'
import InfoModal from '../../../../components/modals/info/InfoModal'
import Modal from '../../../../components/modals/Modal'
import PageTitle from '../../../../components/page-title/PageTitle'
import Page from '../../../../components/page/Page'
import Validation from '../../../../core/Validation'
import { Route } from '../../../../enum/Route'
import Storage from '../../../../storage/Storage'
import Translations from '../../../../translations/Translations'

interface AccountBusinessesDashboardState {
    /**
     * All businesses
     */
    businesses: Array<Partial<BusinessBase>>

    /**
     * The pagination
     */
    pagination: {
        currentPage: number
        lastPage: number
    }

    /**
     * The sort
     */
    sort: {
        propertyName: string
        sortDirection: string
    }

    /**
     * The filters
     */
    filters: {
        search: string
    }
}

/**
 * The dashboard for the businesses of the account
 * @author Stan Hurks
 */
export default class AccountBusinessesDashboard extends React.Component<any, AccountBusinessesDashboardState> {
    private readonly currentRoute = {
        route: Route.ACCOUNT_BUSINESSES
    }
    
    constructor(props: any) {
        super(props)
        this.state = {
            businesses: [],
            pagination: {
                currentPage: 1,
                lastPage: 1
            },
            sort: {
                propertyName: 'name',
                sortDirection: 'asc'
            },
            filters: {
                search: ''
            }
        }
    }
    
    public componentDidMount = () => {
        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
        this.updateBusinesses()
    }

    public render = () => {
        return (
            <div className="account-businesses-dashboard">
                <Page>
                    <PageTitle route={this.currentRoute.route} right={(
                        <FormButton onClick={() => {
                            App.redirect.next({
                                route: Route.ACCOUNT_BUSINESSES_PROFILE,
                                variables: {
                                    businessId: 'new'
                                }
                            })
                        }}>
                            <Add />
                        </FormButton>
                    )} />
                    <ListView
                        data={this.state.businesses}
                        columns={[
                            {
                                width: 150,
                                propertyName: 'name',
                                label: 'Business name',
                                getValue: (object: BusinessBase) => {
                                    return object.name
                                }
                            },
                            {
                                width: 200,
                                propertyName: 'street1',
                                label: 'Street',
                                getValue: (object: BusinessBase) => {
                                    return object.street1
                                }
                            },
                            {
                                width: 150,
                                propertyName: 'city',
                                label: 'City',
                                getValue: (object: BusinessBase) => {
                                    return object.city
                                }
                            },
                            {
                                width: 125,
                                propertyName: 'zipCode',
                                label: 'Zip code',
                                getValue: (object: BusinessBase) => {
                                    return object.zipCode
                                }
                            }
                        ]}
                        getActionsForUuid={(uuid) => (
                            <div className="table">
                                <Menu options={[
                                    {
                                        label: 'Business profile',
                                        onClick: () => {
                                            App.redirect.next({
                                                route: Route.ACCOUNT_BUSINESSES_PROFILE,
                                                variables: {
                                                    businessId: uuid
                                                }
                                            })
                                        },
                                        color: this.validateBusinessProfile(this.findBusinessByUuid(uuid))
                                            ? 'primary'
                                            : 'white'
                                    },
                                    {
                                        label: 'Payment options',
                                        onClick: () => {
                                            App.redirect.next({
                                                route: Route.ACCOUNT_BUSINESSES_PAYMENTS,
                                                variables: {
                                                    businessId: uuid
                                                }
                                            })
                                        },
                                        color: this.validatePaymentOptions(this.findBusinessByUuid(uuid))
                                            ? 'primary'
                                            : 'white'
                                    },
                                    {
                                        label: 'Delete',
                                        onClick: () => {
                                            this.deleteBusiness(uuid)
                                        },
                                        color: 'error',
                                        hide: Storage.data.session.user.business.uuid === uuid
                                    }
                                ]} />
                            </div>
                        )}
                        topPinnedUuids={[
                            Storage.data.session.user.business.uuid!
                        ]}
                        onChangeSort={(propertyName, sortDirection) => {
                            this.setState({
                                sort: {
                                    propertyName,
                                    sortDirection
                                },
                                pagination: {
                                    ...this.state.pagination,
                                    currentPage: 1
                                }
                            }, () => {
                                this.updateBusinesses()
                            })
                        }}
                        onChangeSearch={(search) => {
                            if (this.state.filters.search === search) {
                                return
                            }
                            this.setState({
                                filters: {
                                    ...this.state.filters,
                                    search
                                }
                            }, () => {
                                this.updateBusinesses()
                            })
                        }}
                        pagination={{
                            lastPage: this.state.pagination.lastPage,
                            currentPage: this.state.pagination.currentPage,
                            onChangePage: (page: number) => {
                                this.setState({
                                    pagination: {
                                        ...this.state.pagination,
                                        currentPage: page
                                    }
                                }, () => {
                                    this.updateBusinesses()
                                })
                            }
                        }}
                         />
                </Page>
            </div>
        )
    }

    /**
     * Update the businesses
     */
    private updateBusinesses = () => {
        GraphQL.query<{
            listBusinesses: PaginatedResponseBase<BusinessBase[]>
        }>(`
        query {
            listBusinesses(input: {
                #TODO: search
                page: ${this.state.pagination.currentPage},
                take: 10,
                sortProperty: "${this.state.sort.propertyName}",
                sortDirection: "${this.state.sort.sortDirection}",
                search: ${GraphQL.serializeString(this.state.filters.search)}
            }){
                code
                data {
                    uuid
                    name
                    country
                    city
                    zipCode
                    street1
                    email
                    bankCode
                    accountNumber
                    acceptOnlinePayments
                }
                lastPage
            }
        }
        `).then((data) => {
            if (data.data.listBusinesses.code === ResponseCode.OK) {
                this.setState({
                    businesses: data.data.listBusinesses.data || [],
                    pagination: {
                        ...this.state.pagination,
                        lastPage: data.data.listBusinesses.lastPage!
                    }
                })
            }
        })
    }

    /**
     * Deletes a business by uuid
     */
    private deleteBusiness = (uuid: string) => {
        GraphQL.query<{
            getBusinessRelations: {
                code: ResponseCode
                quotations: number
                invoices: number
                templates: number
            }
        }>(`
            query {
                getBusinessRelations(input:{
                    uuid: ${GraphQL.serializeString(uuid)}
                }){
                    code
                    quotations
                    invoices
                    templates
                }
            }
        `).then((data) => {
            if (data.data.getBusinessRelations.code === ResponseCode.OK) {
                Modal.mount.next((
                    <InfoModal
                        title={Translations.translations.pages.account.businesses.dashboard.delete.modal.title}
                        content={Translations.translations.pages.account.businesses.dashboard.delete.modal.content(
                            data.data.getBusinessRelations.invoices,
                            data.data.getBusinessRelations.quotations,
                            data.data.getBusinessRelations.templates
                        )}
                        button={(
                            <FormButton variant="primary" onClick={() => {
                                GraphQL.query<{
                                    deleteBusiness: ResponseBase
                                }>(`
                                    mutation {
                                        deleteBusiness(input:{
                                            uuid: ${GraphQL.serializeString(uuid)}
                                        }){
                                            code
                                        }
                                    }
                                `).then((data) => {
                                    Modal.unmount.next()
                                    this.updateBusinesses()
                                })
                            }}>
                                {Translations.translations.pages.account.businesses.dashboard.delete.modal.button}
                            </FormButton>
                        )} />
                ))
            }
        })
    }

    /**
     * Unsafely find a business by uuid
     */
    private findBusinessByUuid = (uuid: string): BusinessBase => {
        return this.state.businesses.filter((v) => v.uuid === uuid)[0] as BusinessBase
    }

    /**
     * Validate the business profile for a business 
     */
    private validateBusinessProfile = (business: Partial<BusinessBase>): boolean => {
        return Boolean(
            business.name && business.name.length
            && business.country && business.country.length
            && business.street1 && business.street1.length
            && business.email && business.email.length && Validation.validateEmail(business.email)
        )
    }

    /**
     * Validate the payment options for a business
     */
    private validatePaymentOptions = (business: Partial<BusinessBase>): boolean => {
        return Boolean(
            business.bankCode && business.bankCode.length
            && business.accountNumber && business.accountNumber.length
            && business.acceptOnlinePayments
        )
    }
}