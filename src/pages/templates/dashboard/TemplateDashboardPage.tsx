import { Add } from '@material-ui/icons'
import React from 'react'
import App from '../../../App'
import { TemplateBase } from '../../../backend/entities/TemplateBase'
import GraphQL from '../../../backend/GraphQL'
import { PaginatedResponseBase } from '../../../backend/responses/base/PaginatedResponseBase'
import { ResponseBase } from '../../../backend/responses/base/ResponseBase'
import { ResponseCode } from '../../../backend/responses/base/ResponseCode'
import FormButton from '../../../components/form-button/FormButton'
import ListView from '../../../components/list-view/ListView'
import Menu from '../../../components/menu/Menu'
import InfoModal from '../../../components/modals/info/InfoModal'
import Modal from '../../../components/modals/Modal'
import PageTitle from '../../../components/page-title/PageTitle'
import Page from '../../../components/page/Page'
import { Route } from '../../../enum/Route'
import Translations from '../../../translations/Translations'

/**
 * The state
 */
interface TemplateDashboardPageState {
    /**
     * The templates
     */
    templates: Array<Partial<TemplateBase>>

    /**
     * The pagination
     */
    pagination: {
        currentPage: number
        lastPage: number
    }

    /**
     * The sort
     */
    sort: {
        propertyName: string
        sortDirection: string
    }

    /**
     * The filters
     */
    filters: {
        search: string
    }
}

/**
 * The template dashboard page
 * @author Stan Hurks
 */
export default class TemplateDashboardPage extends React.Component<any, TemplateDashboardPageState> {
    /**
     * The current route
     */
    private currentRoute = {
        route: Route.TEMPLATES
    }

    constructor(props: any) {
        super(props)
        this.state = {
            templates: [],
            pagination: {
                currentPage: 1,
                lastPage: 1
            },
            sort: {
                propertyName: 'name',
                sortDirection: 'asc'
            },
            filters: {
                search: ''
            }
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
        this.updateTemplates()
    }

    public render = () => {
        return (
            <div className="template-dashboard-page">
                <Page>
                    <PageTitle route={this.currentRoute.route} right={(
                        <FormButton variant="primary" onClick={() => {
                            App.redirect.next({
                                route: Route.TEMPLATES_ADD
                            })
                        }}>
                            <Add />
                        </FormButton>
                    )} />
                    <ListView
                        data={this.state.templates}
                        columns={[
                            {
                                width: 200,
                                propertyName: 'name',
                                label: 'Template name',
                                getValue: (object: TemplateBase) => {
                                    return object.name
                                }
                            }
                        ]}
                        selectable={true}
                        getActionsForSelection={(uuids) => (
                            <Menu variant="primary" label="Actions" disabled={!uuids.length} options={[
                                {
                                    label: 'Delete',
                                    color: 'error',
                                    onClick: () => {
                                        this.deleteTemplates(uuids)
                                    }
                                }
                            ]} />
                        )}
                        getActionsForUuid={(uuid) => (
                            <div className="table">
                                <Menu options={[
                                    {
                                        label: 'Edit',
                                        onClick: () => {
                                            App.redirect.next({
                                                route: Route.TEMPLATES_EDIT,
                                                variables: {
                                                    templateId: uuid
                                                }
                                            })
                                        }
                                    },
                                    {
                                        label: 'Delete',
                                        color: 'error',
                                        onClick: () => {
                                            this.deleteTemplates([uuid])
                                        }
                                    }
                                ]} />
                            </div>
                        )}
                        onChangeSort={(propertyName, sortDirection) => {
                            this.setState({
                                sort: {
                                    propertyName,
                                    sortDirection
                                },
                                pagination: {
                                    ...this.state.pagination,
                                    currentPage: 1
                                }
                            }, () => {
                                this.updateTemplates()
                            })
                        }}
                        onChangeSearch={(search) => {
                            if (this.state.filters.search === search) {
                                return
                            }
                            this.setState({
                                filters: {
                                    ...this.state.filters,
                                    search
                                }
                            }, () => {
                                this.updateTemplates()
                            })
                        }}
                        pagination={{
                            lastPage: this.state.pagination.lastPage,
                            currentPage: this.state.pagination.currentPage,
                            onChangePage: (page: number) => {
                                this.setState({
                                    pagination: {
                                        ...this.state.pagination,
                                        currentPage: page
                                    }
                                }, () => {
                                    this.updateTemplates()
                                })
                            }
                        }}
                         />
                </Page>
            </div>
        )
    }

    /**
     * Update the templates
     */
     private updateTemplates = () => {
        GraphQL.query<{
            listTemplates: PaginatedResponseBase<TemplateBase[]>
        }>(`
        query {
            listTemplates(input: {
                page: ${this.state.pagination.currentPage},
                take: 10,
                sortProperty: "${this.state.sort.propertyName}",
                sortDirection: "${this.state.sort.sortDirection}",
                search: ${GraphQL.serializeString(this.state.filters.search)}
            }){
                code
                data {
                    uuid
                    name
                }
                lastPage
            }
        }
        `).then((data) => {
            if (data.data.listTemplates.code === ResponseCode.OK) {
                this.setState({
                    templates: data.data.listTemplates.data || [],
                    pagination: {
                        ...this.state.pagination,
                        lastPage: data.data.listTemplates.lastPage!
                    }
                })
            }
        })
    }

    /**
     * Deletes a business by uuids
     */
    private deleteTemplates = (uuidsArray: string[]) => {
        const uuids = uuidsArray.map(v => GraphQL.serializeString(v)).join(`,`)
        GraphQL.query<{
            getTemplateRelations: {
                code: ResponseCode
                quotations: number
                invoices: number
            }
        }>(`
            query {
                getTemplateRelations(input:{
                    uuids: [${uuids}]
                }){
                    code
                    quotations
                    invoices
                }
            }
        `).then((data) => {
            if (data.data.getTemplateRelations.code === ResponseCode.OK) {
                Modal.mount.next((
                    <InfoModal
                        title={Translations.translations.pages.templates.dashboard.delete.modal.title(uuidsArray.length)}
                        content={Translations.translations.pages.templates.dashboard.delete.modal.content(
                            uuidsArray.length,
                            data.data.getTemplateRelations.invoices,
                            data.data.getTemplateRelations.quotations
                        )}
                        button={(
                            <FormButton variant="primary" onClick={() => {
                                GraphQL.query<{
                                    deleteBusiness: ResponseBase
                                }>(`
                                    mutation {
                                        deleteTemplates(input:{
                                            uuids: [${uuids}]
                                        }){
                                            code
                                        }
                                    }
                                `).then((data) => {
                                    Modal.unmount.next()
                                    this.updateTemplates()
                                })
                            }}>
                                {Translations.translations.pages.templates.dashboard.delete.modal.button}
                            </FormButton>
                        )} />
                ))
            }
        })
    }
}