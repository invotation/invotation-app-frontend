import { Description, ListAlt } from '@material-ui/icons'
import classNames from 'classnames'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import App from '../../../App'
import { BusinessBase } from '../../../backend/entities/BusinessBase'
import { TemplateBase } from '../../../backend/entities/TemplateBase'
import GraphQL from '../../../backend/GraphQL'
import { DataResponseBase } from '../../../backend/responses/base/DataResponseBase'
import { ResponseCode } from '../../../backend/responses/base/ResponseCode'
import DocumentPreview from '../../../components/document-preview/DocumentPreview'
import FormButton from '../../../components/form-button/FormButton'
import FormColor from '../../../components/form-color/FormColor'
import FormGroup from '../../../components/form-group/FormGroup'
import FormImage from '../../../components/form-image/FormImage'
import FormInput from '../../../components/form-input/FormInput'
import FormSelect from '../../../components/form-select/FormSelect'
import PageTitle from '../../../components/page-title/PageTitle'
import Page from '../../../components/page/Page'
import Device from '../../../core/Device'
import { Objects } from '../../../core/Objects'
import { Route } from '../../../enum/Route'
import './template-edit-page.scss'

/**
 * The state
 */
interface TemplateEditPageState {
    /**
     * All businesses
     */
    businesses: Partial<BusinessBase>[]

    /**
     * The template
     */
    template: Partial<TemplateBase> | null

    /**
     * The edited business
     */
    editedTemplate: Partial<TemplateBase> | null

    /**
     * The view
     */
    view: 'form' | 'document'

    /**
     * Whether or not the viewport is for mobile
     */
    isMobile: boolean

    /**
     * The window resolution
     */
    windowResolution: {
        width: number
        height: number
    }
}

/**
 * The edit page for a template.
 * @author Stan Hurks
 */
export default class TemplateEditPage extends React.Component<RouteComponentProps<{templateId: string}>, TemplateEditPageState> {
    /**
     * All template color presets
     */
    private static presets: {[name: string]: Partial<TemplateBase>} = {
        default: {
            primaryColor: 'rgb(0, 125, 240)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(46, 117, 182)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(222, 239, 255)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        green: {
            primaryColor: 'rgb(43, 187, 75)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(48, 150, 95)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(206, 241, 216)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        red: {
            primaryColor: 'rgb(187, 43, 60)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(150, 48, 66)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(241, 206, 211)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        orange: {
            primaryColor: 'rgb(227, 126, 20)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(186, 111, 33)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(255, 235, 214)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        cyan: {
            primaryColor: 'rgb(20, 196, 227)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(18, 165, 191)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(216, 237, 241)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        purple: {
            primaryColor: 'rgb(136, 20, 227)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(95, 11, 162)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(236, 229, 241)',
            lightTextColor: 'rgb(51, 51, 51)'
        },
        grey: {
            primaryColor: 'rgb(51, 51, 51)',
            primaryTextColor: 'rgb(247, 247, 247)',
            darkColor: 'rgb(68, 68, 68)',
            darkTextColor: 'rgb(247, 247, 247)',
            lightColor: 'rgb(238, 238, 238)',
            lightTextColor: 'rgb(51, 51, 51)'
        }
    }

    private currentRoute = {
        route: Route.TEMPLATES_EDIT,
        previous: {
            route: Route.TEMPLATES
        }
    }
    
    constructor(props: any) {
        super(props)
        
        this.state = {
            businesses: [],
            template: null,
            editedTemplate: null,
            view: 'form',
            isMobile: window.innerWidth <= Device.mobileResolution,
            windowResolution: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }
    
    public componentDidMount = () => {
        const templateId = this.props.match.params.templateId
        if (templateId === 'new') {
            this.currentRoute.route = Route.TEMPLATES_ADD
            const template = {
                name: '',
                templateId: 1,
                logo: null,
                signature: null,
                business: {
                    uuid: null
                },
                ...TemplateEditPage.presets.default
            }
            this.setState({
                template: Object.assign({}, template as any),
                editedTemplate: Object.assign({}, template as any)
            })
            this.initializeBusinesses()
        } else {
            this.initializeTemplate(() => {
                this.initializeBusinesses()
            })
        }

        window.addEventListener('resize', this.onResize)

        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
    }

    public componentWillUnmount = () => {
        window.removeEventListener('resize', this.onResize)
    }

    public render = () => {
        return (
            <Page>
                {
                    this.state.template !== null
                    && this.state.editedTemplate !== null
                    &&
                    <div className="template-edit-page">
                        <div className="template-edit-page-left" style={{
                            ...(this.state.isMobile && this.state.view !== 'form')
                            && {
                                display: 'none'
                            }
                        }}>
                            <div className="template-edit-page-left-title">
                                {this.renderPageTitle()}
                            </div>
                            <div className="template-edit-page-left-content">
                                <div className="template-edit-page-left-content-name">
                                    <FormGroup label="Name" required>
                                        <FormInput value={this.state.editedTemplate.name!} onChange={(name) => {
                                            this.editTemplateProperty('name', name)
                                        }} />
                                    </FormGroup>
                                </div>                            
                                <div className="template-edit-page-left-content-business">
                                    <FormGroup label="Business" required>
                                        <FormSelect search label="Select" value={this.state.editedTemplate!.business!.uuid} options={this.state.businesses.map((v) => ({
                                            label: v.name!,
                                            value: v.uuid! 
                                        }))} onChange={(uuid) => {
                                            const businesses = this.state.businesses.filter((v) => v.uuid === uuid)
                                            if (!businesses.length) {
                                                return
                                            }
                                            this.setState({
                                                editedTemplate: {
                                                    ...this.state.editedTemplate,
                                                    business: {
                                                        ...this.state.editedTemplate!.business!,
                                                        ...businesses[0]
                                                    }
                                                }
                                            }, () => {
                                                if (this.hasChanges()) {
                                                    App.unsavedChanges.next(true)
                                                } else {
                                                    App.unsavedChanges.next(false)
                                                }
                                            })
                                        }} />
                                    </FormGroup>
                                </div>
                                <div className="template-edit-page-left-content-presets">
                                    <div className="template-edit-page-left-content-presets-title">
                                        Presets
                                    </div>
                                    <div className="template-edit-page-left-content-presets-content">
                                        {
                                            Object.keys(TemplateEditPage.presets).map((presetName) => (
                                                <div className="template-edit-page-left-content-presets-content-preset" key={presetName} onClick={() => {
                                                    this.setState({
                                                        editedTemplate: {
                                                            ...this.state.editedTemplate,
                                                            ...TemplateEditPage.presets[presetName]
                                                        }
                                                    }, () => {
                                                        if (this.hasChanges()) {
                                                            App.unsavedChanges.next(true)
                                                        } else {
                                                            App.unsavedChanges.next(false)
                                                        }
                                                    })
                                                }}>
                                                    {
                                                        [
                                                            'primaryColor',
                                                            'primaryTextColor',
                                                            'darkColor',
                                                            'darkTextColor',
                                                            'lightColor',
                                                            'lightTextColor'
                                                        ].map((propertyName) => (
                                                            <div className="template-edit-page-left-content-presets-content-preset-color" key={propertyName} style={{
                                                                background: (TemplateEditPage.presets[presetName] as any)[propertyName]
                                                            }}>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                                <div className="template-edit-page-left-content-colors">
                                    <div className="row">
                                        <FormGroup label="Primary">
                                            <FormColor color={this.state.editedTemplate.primaryColor!} onChange={(primaryColor) => {
                                                this.editTemplateProperty('primaryColor', primaryColor)
                                            }} />
                                        </FormGroup>
                                        <FormGroup label="Primary (text color)">
                                            <FormColor color={this.state.editedTemplate.primaryTextColor!} onChange={(primaryTextColor) => {
                                                this.editTemplateProperty('primaryTextColor', primaryTextColor)
                                            }} />
                                        </FormGroup>
                                    </div>
                                    <div className="row">
                                        <FormGroup label="Dark">
                                            <FormColor color={this.state.editedTemplate.darkColor!} onChange={(darkColor) => {
                                                this.editTemplateProperty('darkColor', darkColor)
                                            }} />
                                        </FormGroup>
                                        <FormGroup label="Dark (text color)">
                                            <FormColor color={this.state.editedTemplate.darkTextColor!} onChange={(darkTextColor) => {
                                                this.editTemplateProperty('darkTextColor', darkTextColor)
                                            }} />
                                        </FormGroup>
                                    </div>
                                    <div className="row">
                                        <FormGroup label="Light">
                                            <FormColor color={this.state.editedTemplate.lightColor!} onChange={(lightColor) => {
                                                this.editTemplateProperty('lightColor', lightColor)
                                            }} />
                                        </FormGroup>
                                        <FormGroup label="Light (text color)">
                                            <FormColor color={this.state.editedTemplate.lightTextColor!} onChange={(lightTextColor) => {
                                                this.editTemplateProperty('lightTextColor', lightTextColor)
                                            }} />
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="template-edit-page-left-content-template-id">
                                    <div className="template-edit-page-left-content-template-id-title">
                                        Stijl
                                    </div><div className="template-edit-page-left-content-template-id-content">
                                        {
                                            [1].map((templateId) => (
                                                <div className={classNames({
                                                    'template-edit-page-left-content-template-id-content-entry': true,
                                                    'selected': this.state.editedTemplate!.templateId === templateId
                                                })} key={templateId} onClick={() => {
                                                    this.editTemplateProperty('templateId', templateId)
                                                }}>
                                                    <DocumentPreview
                                                        primaryColor={this.state.editedTemplate!.primaryColor!}
                                                        primaryTextColor={this.state.editedTemplate!.primaryTextColor!}
                                                        darkColor={this.state.editedTemplate!.darkColor!}
                                                        darkTextColor={this.state.editedTemplate!.darkTextColor!}
                                                        lightColor={this.state.editedTemplate!.lightColor!}
                                                        lightTextColor={this.state.editedTemplate!.lightTextColor!}
                                                        templateId={templateId}
                                                        business={this.state.editedTemplate!.business!.uuid
                                                            ? this.state.editedTemplate!.business
                                                            : undefined
                                                        }
                                                        document={{
                                                            ...DocumentPreview.dummyDocument,
                                                            logoUrl: this.state.editedTemplate!.logo === null
                                                                ? null
                                                                : (
                                                                    this.state.editedTemplate!.logo === this.state.template!.logo
                                                                        ? this.getHost() + this.state.editedTemplate!.logo
                                                                        : this.state.editedTemplate!.logo!
                                                                ),
                                                            signatureUrl: this.state.editedTemplate!.signature === null
                                                                ? null
                                                                : (
                                                                    this.state.editedTemplate!.signature === this.state.template!.signature
                                                                        ? this.getHost() + this.state.editedTemplate!.signature
                                                                        : this.state.editedTemplate!.signature!
                                                                )
                                                        }}
                                                        />
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                                <div className="template-edit-page-left-content-images">
                                    <FormGroup label="Logo">
                                        <FormImage value={
                                            this.state.editedTemplate!.logo === this.state.template!.logo
                                                ? (
                                                    this.state.editedTemplate!.logo === null
                                                    ? null
                                                    : this.getHost() + this.state.editedTemplate!.logo || null
                                                )
                                                : this.state.editedTemplate!.logo || null
                                        } onChange={(base64) => {
                                            this.editTemplateProperty('logo', base64)
                                        }} desiredWidth={200} />
                                    </FormGroup>
                                    <FormGroup label="Signature">
                                        <FormImage value={
                                            this.state.editedTemplate!.signature === this.state.template!.signature
                                                ? (
                                                    this.state.editedTemplate!.signature === null
                                                    ? null
                                                    : this.getHost() + this.state.editedTemplate!.signature || null
                                                )
                                                : this.state.editedTemplate!.signature || null
                                        } onChange={(base64) => {
                                            this.editTemplateProperty('signature', base64)
                                        }} desiredWidth={200} />
                                    </FormGroup>
                                </div>
                                <FormButton variant="primary" disabled={!this.hasChanges() || !this.validate()} onClick={() => {
                                    if (this.state.template!.uuid) {
                                        this.putTemplate()
                                    } else {
                                        this.postTemplate()
                                    }
                                }}>
                                    Save
                                </FormButton>
                            </div>
                        </div>
                        <div className="template-edit-page-right" style={{
                            ...(this.state.isMobile && this.state.view !== 'document')
                            && {
                                display: 'none'
                            },
                            maxWidth: (this.state.windowResolution.height - 60) * 747 / 1056
                        }}>
                            {
                                this.state.isMobile
                                &&
                                this.renderPageTitle()
                            }
                            <DocumentPreview
                                primaryColor={this.state.editedTemplate.primaryColor!}
                                primaryTextColor={this.state.editedTemplate.primaryTextColor!}
                                darkColor={this.state.editedTemplate.darkColor!}
                                darkTextColor={this.state.editedTemplate.darkTextColor!}
                                lightColor={this.state.editedTemplate.lightColor!}
                                lightTextColor={this.state.editedTemplate.lightTextColor!}
                                business={this.state.editedTemplate.business!.uuid
                                    ? this.state.editedTemplate.business
                                    : undefined
                                }
                                document={{
                                    ...DocumentPreview.dummyDocument,
                                    logoUrl: this.state.editedTemplate.logo === null
                                        ? null
                                        : (
                                            this.state.editedTemplate.logo === this.state.template.logo
                                                ? this.getHost() + this.state.editedTemplate.logo
                                                : this.state.editedTemplate.logo!
                                        ),
                                    signatureUrl: this.state.editedTemplate.signature === null
                                        ? null
                                        : (
                                            this.state.editedTemplate.signature === this.state.template.signature
                                                ? this.getHost() + this.state.editedTemplate.signature
                                                : this.state.editedTemplate.signature!
                                        )
                                }}
                                templateId={this.state.editedTemplate.templateId!}
                                />
                        </div>
                    </div>
                }
            </Page>
        )
    }

    /**
     * Renders the page title
     */
    private renderPageTitle = () => {
        return (
            <PageTitle route={this.currentRoute.route} previous={this.state.isMobile ? undefined : this.currentRoute.previous} right={
                this.state.isMobile
                ? (
                    <FormButton variant="white" onClick={() => {
                        this.setState({
                            view: this.state.view === 'document'
                                ? 'form'
                                : 'document'
                        })
                    }}>
                        {
                            this.state.view === 'form'
                            ?
                            <Description />
                            :
                            <ListAlt />
                        }
                    </FormButton>
                )
                : undefined
            } />
        )
    }

    /**
     * Get the host
     */
    private getHost = () => {
        return window.location.protocol + '//' + window.location.hostname + ':4000/'
    }

    /**
     * On resize
     */
    private onResize = () => {
        this.setState({
            isMobile: window.innerWidth <= Device.mobileResolution,
            windowResolution: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        })
    }

    /**
     * Initialize the businesses
     */
    private initializeBusinesses = () => {
        GraphQL.query<{
            listAllBusinesses: DataResponseBase<BusinessBase[]>
        }>(`
            query {
                listAllBusinesses {
                    code
                    data {
                        uuid
                        name
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                        cocNumber
                        vatNumber
                        email
                        phone
                        website
                        bankCode
                        accountNumber
                        acceptOnlinePayments
                        molliePublicKey
                    }
                }
            }
        `).then((data) => {
            if (data.data.listAllBusinesses.code === ResponseCode.OK) {
                this.setState({
                    businesses: data.data.listAllBusinesses.data!
                }, () => {
                    console.log(this.state.businesses)
                })
            }
        })
    }

    /**
     * Initialize the template
     */
    private initializeTemplate = (callback: () => void) => {
        GraphQL.query<{
            getTemplate: DataResponseBase<TemplateBase>
        }>(`
            query {
                getTemplate(input: {
                    uuid: ${GraphQL.serializeString(this.state.editedTemplate?.uuid || this.props.match.params.templateId)}
                }) {
                    code
                    data {
                        uuid
                        name
                        business {
                            uuid
                            name
                            country
                            street1
                            street2
                            zipCode
                            city
                            provinceState
                            cocNumber
                            vatNumber
                            email
                            phone
                            website
                            bankCode
                            accountNumber
                            acceptOnlinePayments
                            molliePublicKey
                        }
                        templateId
                        primaryColor
                        primaryTextColor
                        darkColor
                        darkTextColor
                        lightColor
                        lightTextColor
                        logo
                        signature
                    }
                }
            }
        `).then((data) => {
            switch (data.data.getTemplate.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        template: data.data.getTemplate.data!,
                        editedTemplate: data.data.getTemplate.data!
                    }, () => {
                        callback()
                    })
                    break
            }
        })
    }

    /**
     * PUT edit the template
     */
    private putTemplate = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            putTemplate: DataResponseBase<TemplateBase>
        }>(`
            mutation {
                putTemplate(input:{
                    uuid: ${GraphQL.serializeString(this.state.editedTemplate!.uuid)},
                    businessUuid: ${GraphQL.serializeString(this.state.editedTemplate!.business!.uuid)},
                    name: ${GraphQL.serializeString(this.state.editedTemplate!.name)},
                    templateId: ${this.state.editedTemplate!.templateId},
                    primaryColor: ${GraphQL.serializeString(this.state.editedTemplate!.primaryColor)}, 
                    primaryTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.primaryTextColor)}, 
                    darkColor: ${GraphQL.serializeString(this.state.editedTemplate!.darkColor)}, 
                    darkTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.darkTextColor)}, 
                    lightColor: ${GraphQL.serializeString(this.state.editedTemplate!.lightColor)}, 
                    lightTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.lightTextColor)},
                    logo: ${GraphQL.serializeString(this.state.editedTemplate!.logo)},
                    signature: ${GraphQL.serializeString(this.state.editedTemplate!.signature)}
                }) {
                    code
                    data{
                        uuid
                        name
                        business {
                            uuid
                        }
                        templateId
                        primaryColor
                        primaryTextColor
                        darkColor
                        darkTextColor
                        lightColor
                        lightTextColor
                        logo
                        signature
                    }
                }
            }
        `).then((data) => {
            switch (data.data.putTemplate.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    App.unsavedChanges.next(false)
                    this.initializeTemplate(() => {})
                    break
            }
        })
    }

    private postTemplate = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            postTemplate: DataResponseBase<TemplateBase>
        }>(`
            mutation {
                postTemplate(input:{
                    name: ${GraphQL.serializeString(this.state.editedTemplate!.name)},
                    businessUuid: ${GraphQL.serializeString(this.state.editedTemplate!.business!.uuid)},
                    templateId: ${this.state.editedTemplate!.templateId},
                    primaryColor: ${GraphQL.serializeString(this.state.editedTemplate!.primaryColor)}, 
                    primaryTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.primaryTextColor)}, 
                    darkColor: ${GraphQL.serializeString(this.state.editedTemplate!.darkColor)}, 
                    darkTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.darkTextColor)}, 
                    lightColor: ${GraphQL.serializeString(this.state.editedTemplate!.lightColor)}, 
                    lightTextColor: ${GraphQL.serializeString(this.state.editedTemplate!.lightTextColor)},
                    logo: ${GraphQL.serializeString(this.state.editedTemplate!.logo)},
                    signature: ${GraphQL.serializeString(this.state.editedTemplate!.signature)}
                }) {
                    code
                    data{
                        uuid
                        name
                        business {
                            uuid
                        }
                        templateId
                        primaryColor
                        primaryTextColor
                        darkColor
                        darkTextColor
                        lightColor
                        lightTextColor
                        logo
                        signature
                    }
                }
            }
        `).then((data) => {
            switch (data.data.postTemplate.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        editedTemplate: {
                            ...this.state.editedTemplate,
                            ...data.data.postTemplate.data
                        },
                        template: {
                            ...this.state.template,
                            ...data.data.postTemplate.data
                        },
                    }, () => {
                        App.unsavedChanges.next(false)
                        this.currentRoute.route = Route.TEMPLATES_EDIT
                        App.setCurrentRoute.next(this.currentRoute)
                        this.forceUpdate()
                        window.history.pushState('', '', window.location.origin + Route.TEMPLATES_EDIT.replace(':templateId', this.state.editedTemplate!.uuid!))
                        this.initializeTemplate(() => {})
                    })
                    break
            }
        })
    }

    /**
     * Edit a template property
     */
    private editTemplateProperty = (propertyName: keyof TemplateBase, value: any) => {
        this.setState({
            editedTemplate: {
                ...this.state.editedTemplate,
                [propertyName]: value
            }
        }, () => {
            if (this.hasChanges()) {
                App.unsavedChanges.next(true)
            } else {
                App.unsavedChanges.next(false)
            }
        })
    }

    /**
     * Checks whether the business has changes
     */
    private hasChanges = (): boolean => {
        return Object.keys(this.state.template!)
            .map((v) => {
                const a = (this.state.editedTemplate as any)[v] || ''
                const b = (this.state.template as any)[v] || ''
                if (Objects.isObject(a) && Objects.isObject(b)) {
                    return !Objects.isEqual(a, b)
                }
                return a !== b
            })
            .filter((v) => v)
            .length > 0
    }

    /**
     * Validates the form
     */
    private validate = (): boolean => {
        return Boolean(
            this.state.editedTemplate!.name && this.state.editedTemplate!.name.length
            && this.state.editedTemplate!.templateId
            && this.state.editedTemplate!.templateId > 0
            && this.state.editedTemplate!.templateId < 2
            && this.state.editedTemplate!.primaryColor && this.state.editedTemplate!.primaryColor.length
            && this.state.editedTemplate!.primaryTextColor && this.state.editedTemplate!.primaryTextColor.length
            && this.state.editedTemplate!.darkColor && this.state.editedTemplate!.darkColor.length
            && this.state.editedTemplate!.darkTextColor && this.state.editedTemplate!.darkTextColor.length
            && this.state.editedTemplate!.lightColor && this.state.editedTemplate!.lightColor.length
            && this.state.editedTemplate!.lightTextColor && this.state.editedTemplate!.lightTextColor.length
            && this.state.editedTemplate!.business!.uuid
        )
    }
}