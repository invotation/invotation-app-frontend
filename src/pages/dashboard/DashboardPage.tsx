import React from 'react'
import App from '../../App'
import Page from '../../components/page/Page'
import { Route } from '../../enum/Route'

/**
 * The dashboard page
 * @author Stan Hurks
 */
export default class DashboardPage extends React.Component {
    public componentDidMount = () => {
        setTimeout(() => {
            App.setCurrentRoute.next({
                route: Route.DASHBOARD
            })
        })
    }
    
    public render = () => {
        return (
            <div className="dashboard-page">
                <Page>
                </Page>
            </div>
        )
    }
}