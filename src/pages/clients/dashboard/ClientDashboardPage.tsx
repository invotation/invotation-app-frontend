import { Add } from '@material-ui/icons'
import React from 'react'
import App from '../../../App'
import { ClientBase } from '../../../backend/entities/ClientBase'
import GraphQL from '../../../backend/GraphQL'
import { PaginatedResponseBase } from '../../../backend/responses/base/PaginatedResponseBase'
import { ResponseBase } from '../../../backend/responses/base/ResponseBase'
import { ResponseCode } from '../../../backend/responses/base/ResponseCode'
import FormButton from '../../../components/form-button/FormButton'
import ListView from '../../../components/list-view/ListView'
import Menu from '../../../components/menu/Menu'
import InfoModal from '../../../components/modals/info/InfoModal'
import Modal from '../../../components/modals/Modal'
import PageTitle from '../../../components/page-title/PageTitle'
import Page from '../../../components/page/Page'
import { Route } from '../../../enum/Route'
import Translations from '../../../translations/Translations'

/**
 * The state
 */
interface ClientDashboardPageState {
    /**
     * The clients
     */
    clients: Array<Partial<ClientBase>>

    /**
     * The pagination
     */
    pagination: {
        currentPage: number
        lastPage: number
    }

    /**
     * The sort
     */
    sort: {
        propertyName: string
        sortDirection: string
    }

    /**
     * The filters
     */
    filters: {
        search: string
    }
}

/**
 * The clients dashboard page.
 * @author Stan Hurks
 */
export default class ClientDashboardPage extends React.Component<any, ClientDashboardPageState> {
    /**
     * The current route
     */
    private currentRoute = {
        route: Route.CLIENTS
    }

    constructor(props: any) {
        super(props)
        this.state = {
            clients: [],
            pagination: {
                currentPage: 1,
                lastPage: 1
            },
            sort: {
                propertyName: 'name',
                sortDirection: 'asc'
            },
            filters: {
                search: ''
            }
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
        this.updateClients()
    }
    
    public render = () => {
        return (
            <div className="clients-dashboard-page">
                <Page>
                    <PageTitle route={this.currentRoute.route} right={(
                        <FormButton variant="primary" onClick={() => {
                            App.redirect.next({
                                route: Route.CLIENTS_ADD
                            })
                        }}>
                            <Add />
                        </FormButton>
                    )} />
                    <ListView
                        data={this.state.clients}
                        columns={[
                            {
                                width: 200,
                                propertyName: 'name',
                                label: 'Client name',
                                getValue: (object: ClientBase) => {
                                    return object.name
                                }
                            }
                        ]}
                        selectable={true}
                        getActionsForSelection={(uuids) => (
                            <Menu variant="primary" label="Actions" disabled={!uuids.length} options={[
                                {
                                    label: 'Delete',
                                    color: 'error',
                                    onClick: () => {
                                        this.deleteClients(uuids)
                                    }
                                }
                            ]} />
                        )}
                        getActionsForUuid={(uuid) => (
                            <div className="table">
                                <Menu options={[
                                    {
                                        label: 'Edit',
                                        onClick: () => {
                                            App.redirect.next({
                                                route: Route.CLIENTS_EDIT,
                                                variables: {
                                                    clientId: uuid
                                                }
                                            })
                                        }
                                    },
                                    {
                                        label: 'Delete',
                                        color: 'error',
                                        onClick: () => {
                                            this.deleteClients([uuid])
                                        }
                                    }
                                ]} />
                            </div>
                        )}
                        onChangeSort={(propertyName, sortDirection) => {
                            this.setState({
                                sort: {
                                    propertyName,
                                    sortDirection
                                },
                                pagination: {
                                    ...this.state.pagination,
                                    currentPage: 1
                                }
                            }, () => {
                                this.updateClients()
                            })
                        }}
                        onChangeSearch={(search) => {
                            if (this.state.filters.search === search) {
                                return
                            }
                            this.setState({
                                filters: {
                                    ...this.state.filters,
                                    search
                                }
                            }, () => {
                                this.updateClients()
                            })
                        }}
                        pagination={{
                            lastPage: this.state.pagination.lastPage,
                            currentPage: this.state.pagination.currentPage,
                            onChangePage: (page: number) => {
                                this.setState({
                                    pagination: {
                                        ...this.state.pagination,
                                        currentPage: page
                                    }
                                }, () => {
                                    this.updateClients()
                                })
                            }
                        }}
                         />
                </Page>
            </div>
        )
    }

    /**
     * Update the clients
     */
    private updateClients = () => {
        GraphQL.query<{
            listClients: PaginatedResponseBase<ClientBase[]>
        }>(`
        query {
            listClients(input: {
                page: ${this.state.pagination.currentPage},
                take: 10,
                sortProperty: "${this.state.sort.propertyName}",
                sortDirection: "${this.state.sort.sortDirection}",
                search: ${GraphQL.serializeString(this.state.filters.search)}
            }){
                code
                data {
                    uuid
                    name
                }
                lastPage
            }
        }
        `).then((data) => {
            if (data.data.listClients.code === ResponseCode.OK) {
                this.setState({
                    clients: data.data.listClients.data || [],
                    pagination: {
                        ...this.state.pagination,
                        lastPage: data.data.listClients.lastPage!
                    }
                })
            }
        })
    }

    /**
     * Deletes a business by uuids
     */
    private deleteClients = (uuidsArray: string[]) => {
        const uuids = uuidsArray.map(v => GraphQL.serializeString(v)).join(`,`)
        GraphQL.query<{
            getClientRelations: {
                code: ResponseCode
                quotations: number
                invoices: number
            }
        }>(`
            query {
                getClientRelations(input:{
                    uuids: [${uuids}]
                }){
                    code
                    quotations
                    invoices
                }
            }
        `).then((data) => {
            if (data.data.getClientRelations.code === ResponseCode.OK) {
                Modal.mount.next((
                    <InfoModal
                        title={Translations.translations.pages.clients.dashboard.delete.modal.title(uuidsArray.length)}
                        content={Translations.translations.pages.clients.dashboard.delete.modal.content(
                            uuidsArray.length,
                            data.data.getClientRelations.invoices,
                            data.data.getClientRelations.quotations
                        )}
                        button={(
                            <FormButton variant="primary" onClick={() => {
                                GraphQL.query<{
                                    deleteBusiness: ResponseBase
                                }>(`
                                    mutation {
                                        deleteClients(input:{
                                            uuids: [${uuids}]
                                        }){
                                            code
                                        }
                                    }
                                `).then((data) => {
                                    Modal.unmount.next()
                                    this.updateClients()
                                })
                            }}>
                                {Translations.translations.pages.clients.dashboard.delete.modal.button}
                            </FormButton>
                        )} />
                ))
            }
        })
    }
}