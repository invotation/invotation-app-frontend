import React from 'react'
import { RouteComponentProps } from 'react-router'
import App from '../../../App'
import { ClientBase } from '../../../backend/entities/ClientBase'
import GraphQL from '../../../backend/GraphQL'
import { DataResponseBase } from '../../../backend/responses/base/DataResponseBase'
import { ResponseCode } from '../../../backend/responses/base/ResponseCode'
import FormButton from '../../../components/form-button/FormButton'
import FormGroup from '../../../components/form-group/FormGroup'
import FormInput from '../../../components/form-input/FormInput'
import PageTitle from '../../../components/page-title/PageTitle'
import Page from '../../../components/page/Page'
import { Objects } from '../../../core/Objects'
import Validation from '../../../core/Validation'
import { Route } from '../../../enum/Route'
import './client-edit-page.scss'

/**
 * The state
 */
interface ClientEditPageState {
    /**
     * The client
     */
    client: Partial<ClientBase> | null
    
    /**
     * The edited client
     */
    editedClient: Partial<ClientBase> | null
}

/**
 * The edit/add page for a client.
 * @author Stan Hurks
 */
export default class ClientEditPage extends React.Component<RouteComponentProps<{clientId: string}>, ClientEditPageState> {
    /**
     * The current route
     */
    private currentRoute = {
        route: Route.CLIENTS_EDIT,
        previous: {
            route: Route.CLIENTS
        }
    }

    constructor(props: any) {
        super(props)

        this.state = {
            client: null,
            editedClient: null
        }
    }

    public componentDidMount = () => {
        const clientId = this.props.match.params.clientId
        if (clientId === 'new') {
            this.currentRoute.route = Route.CLIENTS_ADD
            const client: Partial<ClientBase> = {
                name: '',
                email: '',
                country: '',
                street1: '',
                street2: '',
                zipCode: '',
                city: '',
                provinceState: ''
            }
            this.setState({
                client: Object.assign({}, client),
                editedClient: Object.assign({}, client)
            })
        } else {
            this.initializeClient()
        }

        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
    }
    
    public render = () => {
        return (
            <Page>
                {
                    this.state.client !== null
                    && this.state.editedClient !== null
                    &&
                    <div className="client-edit-page">
                        <PageTitle route={this.currentRoute.route} previous={this.currentRoute.previous} />
                        <div className="row first">
                            <FormGroup label="Client name" required>
                                <FormInput value={this.state.editedClient.name!} onChange={(name) => {
                                    this.editClientProperty('name', name)
                                }} />
                            </FormGroup>
                            <FormGroup label="Email" required>
                                <FormInput type="email" value={this.state.editedClient.email!} onChange={(email) => {
                                    this.editClientProperty('email', email)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="hr"></div>
                        <div className="row">
                            <FormGroup label="Country">
                                <FormInput value={this.state.editedClient.country!} onChange={(country) => {
                                    this.editClientProperty('country', country)
                                }} />
                            </FormGroup>
                            <FormGroup label="Street">
                                <FormInput value={this.state.editedClient.street1!} onChange={(street1) => {
                                    this.editClientProperty('street1', street1)
                                }} />
                            </FormGroup>
                            <FormGroup label="Street 2">
                                <FormInput value={this.state.editedClient.street2!} onChange={(street2) => {
                                    this.editClientProperty('street2', street2)
                                }} />
                            </FormGroup>
                            <FormGroup label="Zip code">
                                <FormInput value={this.state.editedClient.zipCode!} onChange={(zipCode) => {
                                    this.editClientProperty('zipCode', zipCode)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormGroup label="City">
                                <FormInput value={this.state.editedClient.city!} onChange={(city) => {
                                    this.editClientProperty('city', city)
                                }} />
                            </FormGroup>
                            <FormGroup label="Province/state">
                                <FormInput value={this.state.editedClient.provinceState!} onChange={(provinceState) => {
                                    this.editClientProperty('provinceState', provinceState)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormButton variant="primary" disabled={!this.hasChanges() || !this.validate()} onClick={() => {
                                if (this.state.client!.uuid) {
                                    this.putClient()
                                } else {
                                    this.postClient()
                                }
                            }}>
                                Save
                            </FormButton>
                        </div>
                    </div>
                }
            </Page>
        )
    }

    /**
     * Initialize the template
     */
    private initializeClient = () => {
        GraphQL.query<{
            getClient: DataResponseBase<ClientBase>
        }>(`
            query {
                getClient(input: {
                    uuid: ${GraphQL.serializeString(this.state.editedClient?.uuid || this.props.match.params.clientId)}
                }) {
                    code
                    data {
                        uuid
                        name
                        email
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                    }
                }
            }
        `).then((data) => {
            switch (data.data.getClient.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        client: data.data.getClient.data!,
                        editedClient: data.data.getClient.data!
                    })
                    break
            }
        })
    }

    /**
     * PUT edit the client
     */
     private putClient = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            putClient: DataResponseBase<ClientBase>
        }>(`
            mutation {
                putClient(input:{
                    uuid: ${GraphQL.serializeString(this.state.editedClient!.uuid)}, 
                    name: ${GraphQL.serializeString(this.state.editedClient!.name)}, 
                    email: ${GraphQL.serializeString(this.state.editedClient!.email)}, 
                    country: ${GraphQL.serializeString(this.state.editedClient!.country)}, 
                    street1: ${GraphQL.serializeString(this.state.editedClient!.street1)}, 
                    street2: ${GraphQL.serializeString(this.state.editedClient!.street2)}, 
                    zipCode: ${GraphQL.serializeString(this.state.editedClient!.zipCode)}, 
                    city: ${GraphQL.serializeString(this.state.editedClient!.city)}, 
                    provinceState: ${GraphQL.serializeString(this.state.editedClient!.provinceState)}
                }) {
                    code
                    data{
                        uuid
                        name
                        email
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                    }
                }
            }
        `).then((data) => {
            switch (data.data.putClient.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    App.unsavedChanges.next(false)
                    this.initializeClient()
                    break
            }
        })
    }

    /**
     * POST edit the client
     */
    private postClient = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            postClient: DataResponseBase<ClientBase>
        }>(`
            mutation {
                postClient(input:{
                    name: ${GraphQL.serializeString(this.state.editedClient!.name)}, 
                    email: ${GraphQL.serializeString(this.state.editedClient!.email)}, 
                    country: ${GraphQL.serializeString(this.state.editedClient!.country)}, 
                    street1: ${GraphQL.serializeString(this.state.editedClient!.street1)}, 
                    street2: ${GraphQL.serializeString(this.state.editedClient!.street2)}, 
                    zipCode: ${GraphQL.serializeString(this.state.editedClient!.zipCode)}, 
                    city: ${GraphQL.serializeString(this.state.editedClient!.city)}, 
                    provinceState: ${GraphQL.serializeString(this.state.editedClient!.provinceState)}
                }) {
                    code
                    data{
                        uuid
                        name
                        email
                        country
                        street1
                        street2
                        zipCode
                        city
                        provinceState
                    }
                }
            }
        `).then((data) => {
            switch (data.data.postClient.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        editedClient: {
                            ...this.state.editedClient,
                            ...data.data.postClient.data
                        },
                        client: {
                            ...this.state.client,
                            ...data.data.postClient.data
                        },
                    }, () => {
                        App.unsavedChanges.next(false)
                        this.currentRoute.route = Route.CLIENTS_EDIT
                        App.setCurrentRoute.next(this.currentRoute)
                        this.forceUpdate()
                        window.history.pushState('', '', window.location.origin + Route.CLIENTS_EDIT.replace(':clientId', this.state.editedClient!.uuid!))
                        this.initializeClient()
                    })
                    break
            }
        })
    }

    /**
     * Edit a client property
     */
    private editClientProperty = (propertyName: keyof ClientBase, value: any) => {
        this.setState({
            editedClient: {
                ...this.state.editedClient,
                [propertyName]: value
            }
        }, () => {
            if (this.hasChanges()) {
                App.unsavedChanges.next(true)
            } else {
                App.unsavedChanges.next(false)
            }
        })
    }

    /**
     * Checks whether the business has changes
     */
    private hasChanges = (): boolean => {
        return Object.keys(this.state.client!)
            .map((v) => {
                const a = (this.state.editedClient as any)[v] || ''
                const b = (this.state.client as any)[v] || ''
                if (Objects.isObject(a) && Objects.isObject(b)) {
                    return !Objects.isEqual(a, b)
                }
                return a !== b
            })
            .filter((v) => v)
            .length > 0
    }

    /**
     * Validates the form
     */
    private validate = (): boolean => {
        return Boolean(
            this.state.editedClient!.name && this.state.editedClient!.name.length
            && this.state.editedClient!.email && this.state.editedClient!.email.length
            && Validation.validateEmail(this.state.editedClient!.email)        )
    }
}