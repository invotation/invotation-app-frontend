import { Add } from '@material-ui/icons';
import React from 'react'
import App from '../../../App';
import { ItemBase } from '../../../backend/entities/ItemBase';
import GraphQL from '../../../backend/GraphQL';
import { PaginatedResponseBase } from '../../../backend/responses/base/PaginatedResponseBase';
import { ResponseBase } from '../../../backend/responses/base/ResponseBase';
import { ResponseCode } from '../../../backend/responses/base/ResponseCode';
import FormButton from '../../../components/form-button/FormButton';
import ListView from '../../../components/list-view/ListView';
import Menu from '../../../components/menu/Menu';
import InfoModal from '../../../components/modals/info/InfoModal';
import Modal from '../../../components/modals/Modal';
import PageTitle from '../../../components/page-title/PageTitle';
import Page from '../../../components/page/Page';
import { Route } from '../../../enum/Route';
import Translations from '../../../translations/Translations';

/**
 * The state
 */
interface ItemDashboardPageState {
    /**
     * The clients
     */
    items: Array<Partial<ItemBase>>

    /**
     * The pagination
     */
    pagination: {
        currentPage: number
        lastPage: number
    }

    /**
     * The sort
     */
    sort: {
        propertyName: string
        sortDirection: string
    }

    /**
     * The filters
     */
    filters: {
        search: string
    }
}

/**
 * The item dashboard page.
 * @author Stan Hurks
 */
export default class ItemDashboardPage extends React.Component<any, ItemDashboardPageState> {
    /**
     * The current route
     */
    private currentRoute = {
        route: Route.ITEMS
    }

    constructor(props: any) {
        super(props)
        this.state = {
            items: [],
            pagination: {
                currentPage: 1,
                lastPage: 1
            },
            sort: {
                propertyName: 'name',
                sortDirection: 'asc'
            },
            filters: {
                search: ''
            }
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
        this.updateItems()
    }

    public render = () => {
        return (
            <div className="items-dashboard-page">
                <Page>
                    <PageTitle route={this.currentRoute.route} right={(
                        <FormButton variant="primary" onClick={() => {
                            App.redirect.next({
                                route: Route.ITEMS_ADD
                            })
                        }}>
                            <Add />
                        </FormButton>
                    )} />
                    <ListView
                        data={this.state.items}
                        columns={[
                            {
                                width: 200,
                                propertyName: 'name',
                                label: 'Item name',
                                getValue: (object: ItemBase) => {
                                    return object.name
                                }
                            },
                            {
                                width: 200,
                                propertyName: 'priceExclVAT',
                                label: 'Price excl. VAT',
                                getValue: (object: ItemBase) => {
                                    return object.priceExclVAT.toFixed(2)
                                }
                            }
                        ]}
                        selectable={true}
                        getActionsForSelection={(uuids) => (
                            <Menu variant="primary" label="Actions" disabled={!uuids.length} options={[
                                {
                                    label: 'Delete',
                                    color: 'error',
                                    onClick: () => {
                                        this.deleteItems(uuids)
                                    }
                                }
                            ]} />
                        )}
                        getActionsForUuid={(uuid) => (
                            <div className="table">
                                <Menu options={[
                                    {
                                        label: 'Edit',
                                        onClick: () => {
                                            App.redirect.next({
                                                route: Route.ITEMS_EDIT,
                                                variables: {
                                                    itemId: uuid
                                                }
                                            })
                                        }
                                    },
                                    {
                                        label: 'Delete',
                                        color: 'error',
                                        onClick: () => {
                                            this.deleteItems([uuid])
                                        }
                                    }
                                ]} />
                            </div>
                        )}
                        onChangeSort={(propertyName, sortDirection) => {
                            this.setState({
                                sort: {
                                    propertyName,
                                    sortDirection
                                },
                                pagination: {
                                    ...this.state.pagination,
                                    currentPage: 1
                                }
                            }, () => {
                                this.updateItems()
                            })
                        }}
                        onChangeSearch={(search) => {
                            if (this.state.filters.search === search) {
                                return
                            }
                            this.setState({
                                filters: {
                                    ...this.state.filters,
                                    search
                                }
                            }, () => {
                                this.updateItems()
                            })
                        }}
                        pagination={{
                            lastPage: this.state.pagination.lastPage,
                            currentPage: this.state.pagination.currentPage,
                            onChangePage: (page: number) => {
                                this.setState({
                                    pagination: {
                                        ...this.state.pagination,
                                        currentPage: page
                                    }
                                }, () => {
                                    this.updateItems()
                                })
                            }
                        }}
                        />
                </Page>
            </div>
        )
    }

    /**
     * Update the items
     */
    private updateItems = () => {
        GraphQL.query<{
            listItems: PaginatedResponseBase<ItemBase[]>
        }>(`
        query {
            listItems(input: {
                page: ${this.state.pagination.currentPage},
                take: 10,
                sortProperty: "${this.state.sort.propertyName}",
                sortDirection: "${this.state.sort.sortDirection}",
                search: ${GraphQL.serializeString(this.state.filters.search)}
            }){
                code
                data {
                    uuid
                    name
                    priceExclVAT
                }
                lastPage
            }
        }
        `).then((data) => {
            if (data.data.listItems.code === ResponseCode.OK) {
                this.setState({
                    items: data.data.listItems.data || [],
                    pagination: {
                        ...this.state.pagination,
                        lastPage: data.data.listItems.lastPage!
                    }
                })
            }
        })
    }

    /**
     * Deletes one or multiple items by uuids
     */
    private deleteItems = (uuidsArray: string[]) => {
        const uuids = uuidsArray.map(v => GraphQL.serializeString(v)).join(`,`)
        Modal.mount.next((
            <InfoModal
                title={Translations.translations.pages.items.dashboard.delete.modal.title(uuidsArray.length)}
                content={Translations.translations.pages.items.dashboard.delete.modal.content(uuidsArray.length)}
                button={(
                    <FormButton variant="primary" onClick={() => {
                        GraphQL.query<{
                            deleteBusiness: ResponseBase
                        }>(`
                            mutation {
                                deleteItems(input:{
                                    uuids: [${uuids}]
                                }){
                                    code
                                }
                            }
                        `).then((data) => {
                            Modal.unmount.next()
                            this.updateItems()
                        })
                    }}>
                        {Translations.translations.pages.items.dashboard.delete.modal.button}
                    </FormButton>
                )} />
            ))
    }
}