import React from 'react'
import { RouteComponentProps } from 'react-router-dom';
import App from '../../../App';
import { ItemBase } from '../../../backend/entities/ItemBase';
import GraphQL from '../../../backend/GraphQL';
import { DataResponseBase } from '../../../backend/responses/base/DataResponseBase';
import { ResponseCode } from '../../../backend/responses/base/ResponseCode';
import FormButton from '../../../components/form-button/FormButton';
import FormGroup from '../../../components/form-group/FormGroup';
import FormInput from '../../../components/form-input/FormInput';
import PageTitle from '../../../components/page-title/PageTitle';
import Page from '../../../components/page/Page';
import { Objects } from '../../../core/Objects';
import { Route } from '../../../enum/Route';
import './item-edit-page.scss'

/**
 * The state
 */
interface ItemEditPageState {
    /**
     * The item
     */
    item: Partial<ItemBase> | null

    /**
     * The edited item
     */
    editedItem: Partial<ItemBase> | null
}

/**
 * The page for editing/adding an item.
 * @author Stan Hurks
 */
export default class ItemEditPage extends React.Component<RouteComponentProps<{itemId: string}>, ItemEditPageState> {
    /**
     * The current route
     */
    private currentRoute = {
        route: Route.ITEMS_EDIT,
        previous: {
            route: Route.ITEMS
        }
    }

    constructor(props: any) {
        super(props)
        this.state = {
            item: null,
            editedItem: null
        }
    }

    public componentDidMount = () => {
        const itemId = this.props.match.params.itemId
        if (itemId === 'new') {
            this.currentRoute.route = Route.ITEMS_ADD
            const item: Partial<ItemBase> = {
                name: '',
                unit: '',
                priceExclVAT: null as any,
                vatPercentage: null as any
            }
            this.setState({
                item: Object.assign({}, item),
                editedItem: Object.assign({}, item)
            })
        } else {
            this.initializeItem()
        }

        setTimeout(() => {
            App.setCurrentRoute.next(this.currentRoute)
        })
    }

    public render = () => {
        return (
            <Page>
                {
                    this.state.item !== null
                    && this.state.editedItem !== null
                    &&
                    <div className="item-edit-page">
                        <PageTitle route={this.currentRoute.route} previous={this.currentRoute.previous} />
                        <div className="row first">
                            <FormGroup label="Item name" required>
                                <FormInput value={this.state.editedItem.name!} onChange={(name) => {
                                    this.editItemProperty('name', name)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormGroup label="Unit">
                                <FormInput value={this.state.editedItem.unit!} onChange={(unit) => {
                                    this.editItemProperty('unit', unit)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormGroup label="Price excl. VAT" required>
                                <FormInput type="number" value={this.state.editedItem.priceExclVAT!} onChange={(priceExclVAT) => {
                                    this.editItemProperty('priceExclVAT', priceExclVAT)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormGroup label="VAT %" required>
                                <FormInput type="number" value={this.state.editedItem.vatPercentage!} onChange={(vatPercentage) => {
                                    this.editItemProperty('vatPercentage', vatPercentage)
                                }} />
                            </FormGroup>
                        </div>
                        <div className="row">
                            <FormButton variant="primary" disabled={!this.hasChanges() || !this.validate()} onClick={() => {
                                if (this.state.item!.uuid) {
                                    this.putItem()
                                } else {
                                    this.postItem()
                                }
                            }}>
                                Save
                            </FormButton>
                        </div>
                    </div>
                }
            </Page>
        )
    }

    /**
     * Initialize the item
     */
    private initializeItem = () => {
        GraphQL.query<{
            getItem: DataResponseBase<ItemBase>
        }>(`
            query {
                getItem(input: {
                    uuid: ${GraphQL.serializeString(this.state.editedItem?.uuid || this.props.match.params.itemId)}
                }) {
                    code
                    data {
                        uuid
                        name
                        unit
                        priceExclVAT
                        vatPercentage
                    }
                }
            }
        `).then((data) => {
            switch (data.data.getItem.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        item: data.data.getItem.data!,
                        editedItem: data.data.getItem.data!
                    })
                    break
            }
        })
    }

    /**
     * PUT edit the item
     */
    private putItem = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            putItem: DataResponseBase<ItemBase>
        }>(`
            mutation {
                putItem(input:{
                    uuid: ${GraphQL.serializeString(this.state.editedItem!.uuid!)}, 
                    name: ${GraphQL.serializeString(this.state.editedItem!.name!)},
                    unit: ${GraphQL.serializeString(this.state.editedItem!.unit!)},
                    priceExclVAT: ${Number(this.state.editedItem!.priceExclVAT!).toFixed(2)},
                    vatPercentage: ${Number(this.state.editedItem!.vatPercentage!).toFixed(2)}
                }) {
                    code
                    data{
                        uuid
                        name
                        unit
                        priceExclVAT
                        vatPercentage
                    }
                }
            }
        `).then((data) => {
            switch (data.data.putItem.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    App.unsavedChanges.next(false)
                    this.initializeItem()
                    break
            }
        })
    }

    /**
     * POST edit the item
     */
    private postItem = () => {
        if (!this.validate() || !this.hasChanges()) {
            return
        }
        GraphQL.query<{
            postItem: DataResponseBase<ItemBase>
        }>(`
            mutation {
                postItem(input:{
                    name: ${GraphQL.serializeString(this.state.editedItem!.name!)},
                    unit: ${GraphQL.serializeString(this.state.editedItem!.unit!)},
                    priceExclVAT: ${Number(this.state.editedItem!.priceExclVAT!).toFixed(2)},
                    vatPercentage: ${Number(this.state.editedItem!.vatPercentage!).toFixed(2)}
                }) {
                    code
                    data{
                        uuid
                        name
                        unit
                        priceExclVAT
                        vatPercentage
                    }
                }
            }
        `).then((data) => {
            switch (data.data.postItem.code) {
                case ResponseCode.ENTITY_NOT_FOUND:
                    App.redirect.next(this.currentRoute.previous)
                    break
                case ResponseCode.OK:
                    this.setState({
                        editedItem: {
                            ...this.state.editedItem,
                            ...data.data.postItem.data
                        },
                        item: {
                            ...this.state.item,
                            ...data.data.postItem.data
                        },
                    }, () => {
                        App.unsavedChanges.next(false)
                        this.currentRoute.route = Route.ITEMS_EDIT
                        App.setCurrentRoute.next(this.currentRoute)
                        this.forceUpdate()
                        window.history.pushState('', '', window.location.origin + Route.ITEMS_EDIT.replace(':itemId', this.state.editedItem!.uuid!))
                        this.initializeItem()
                    })
                    break
            }
        })
    }

    /**
     * Edit a item property
     */
    private editItemProperty = (propertyName: keyof ItemBase, value: any) => {
        this.setState({
            editedItem: {
                ...this.state.editedItem,
                [propertyName]: value
            }
        }, () => {
            if (this.hasChanges()) {
                App.unsavedChanges.next(true)
            } else {
                App.unsavedChanges.next(false)
            }
        })
    }

    /**
     * Checks whether the business has changes
     */
    private hasChanges = (): boolean => {
        return Object.keys(this.state.item!)
            .map((v) => {
                const a = (this.state.editedItem as any)[v] || ''
                const b = (this.state.item as any)[v] || ''
                if (Objects.isObject(a) && Objects.isObject(b)) {
                    return !Objects.isEqual(a, b)
                }
                return a !== b
            })
            .filter((v) => v)
            .length > 0
    }

    /**
     * Validates the form
     */
    private validate = (): boolean => {
        return Boolean(
            this.state.editedItem!.name && this.state.editedItem!.name.length
            && this.state.editedItem!.priceExclVAT && !isNaN(this.state.editedItem!.priceExclVAT)
            && this.state.editedItem!.vatPercentage && !isNaN(this.state.editedItem!.vatPercentage)
        )
    }
}