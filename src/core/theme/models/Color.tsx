/**
 * Represents a color.
 *
 * @author Anonymous
 */
export class Color {
    /**
     * The red value of the color (0-255)
     */
    public r: number = 0

    /**
     * The green value of the color (0-255)
     */
    public g: number = 0

    /**
     * The blue value of the color (0-255)
     */
    public b: number = 0

    /**
     * The alpha value of the color (0-255)
     */
    public a: number = 0
}