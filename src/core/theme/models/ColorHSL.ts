/**
 * Represents a HSL based color.
 * @author Anonymous
 */
export interface ColorHSL {
    /**
     * The hue (0-360)
     */
    h: number

    /**
     * The saturation (0-100%)
     */
    s: number

    /**
     * The lightness (0-100%)
     */
    l: number

    /**
     * The alpha (0-255)
     */
    a: number
}