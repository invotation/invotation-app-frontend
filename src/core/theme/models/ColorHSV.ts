/**
 * Represents a HSV based color.
 * @author Anonymous
 */
export interface ColorHSV {
    /**
     * The hue
     */
    h: number

    /**
     * The saturation
     */
    s: number

    /**
     * The value
     */
    v: number

    /**
     * Alpha
     */
    a: number
}