import {Color} from './models/Color'
import { ColorHSL } from './models/ColorHSL'
import { ColorHSV } from './models/ColorHSV'

/**
 * All functionality for  the theme in the front-end.
 *
 * @author Anonymous
 */
export class Theme {
    /**
     * The default color; it is black with no opacity.
     */
    public static defaultColor: Color = {
        r: 0,
        g: 0,
        b: 0,
        a: 255
    }

    /**
     * All CSS color names mapped by hex code
     */
    private static hexCodePerCssColor = {
        lightsalmon: '#FFA07A',
        salmon: '#FA8072',
        darksalmon: '#E9967A',
        lightcoral: '#F08080',
        indianred: '#CD5C5C',
        crimson: '#DC143C',
        firebrick: '#B22222',
        red: '#FF0000',
        darkred: '#8B0000',
        coral: '#FF7F50',
        tomato: '#FF6347',
        orangered: '#FF4500',
        gold: '#FFD700',
        orange: '#FFA500',
        darkorange: '#FF8C00',
        lightyellow: '#FFFFE0',
        lemonchiffon: '#FFFACD',
        lightgoldenrodyellow: '#FAFAD2',
        papayawhip: '#FFEFD5',
        moccasin: '#FFE4B5',
        peachpuff: '#FFDAB9',
        palegoldenrod: '#EEE8AA',
        khaki: '#F0E68C',
        darkkhaki: '#BDB76B',
        yellow: '#FFFF00',
        lawngreen: '#7CFC00',
        chartreuse: '#7FFF00',
        limegreen: '#32CD32',
        lime: '#00FF00',
        forestgreen: '#228B22',
        green: '#008000',
        darkgreen: '#006400',
        greenyellow: '#ADFF2F',
        yellowgreen: '#9ACD32',
        springgreen: '#00FF7F',
        mediumspringgreen: '#00FA9A',
        lightgreen: '#90EE90',
        palegreen: '#98FB98',
        darkseagreen: '#8FBC8F',
        mediumseagreen: '#3CB371',
        seagreen: '#2E8B57',
        olive: '#808000',
        darkolivegreen: '#556B2F',
        olivedrab: '#6B8E23',
        lightcyan: '#E0FFFF',
        cyan: '#00FFFF',
        aqua: '#00FFFF',
        aquamarine: '#7FFFD4',
        mediumaquamarine: '#66CDAA',
        paleturquoise: '#AFEEEE',
        turquoise: '#40E0D0',
        mediumturquoise: '#48D1CC',
        darkturquoise: '#00CED1',
        lightseagreen: '#20B2AA',
        cadetblue: '#5F9EA0',
        darkcyan: '#008B8B',
        teal: '#008080',
        powderblue: '#B0E0E6',
        lightblue: '#ADD8E6',
        lightskyblue: '#87CEFA',
        skyblue: '#87CEEB',
        deepskyblue: '#00BFFF',
        lightsteelblue: '#B0C4DE',
        dodgerblue: '#1E90FF',
        cornflowerblue: '#6495ED',
        steelblue: '#4682B4',
        royalblue: '#4169E1',
        blue: '#0000FF',
        mediumblue: '#0000CD',
        darkblue: '#00008B',
        navy: '#000080',
        midnightblue: '#191970',
        mediumslateblue: '#7B68EE',
        slateblue: '#6A5ACD',
        darkslateblue: '#483D8B',
        lavender: '#E6E6FA',
        thistle: '#D8BFD8',
        plum: '#DDA0DD',
        violet: '#EE82EE',
        orchid: '#DA70D6',
        fuchsia: '#FF00FF',
        magenta: '#FF00FF',
        mediumorchid: '#BA55D3',
        mediumpurple: '#9370DB',
        blueviolet: '#8A2BE2',
        darkviolet: '#9400D3',
        darkorchid: '#9932CC',
        darkmagenta: '#8B008B',
        purple: '#800080',
        indigo: '#4B0082',
        pink: '#FFC0CB',
        lightpink: '#FFB6C1',
        hotpink: '#FF69B4',
        deeppink: '#FF1493',
        palevioletred: '#DB7093',
        mediumvioletred: '#C71585',
        white: '#FFFFFF',
        snow: '#FFFAFA',
        honeydew: '#F0FFF0',
        mintcream: '#F5FFFA',
        azure: '#F0FFFF',
        aliceblue: '#F0F8FF',
        ghostwhite: '#F8F8FF',
        whitesmoke: '#F5F5F5',
        seashell: '#FFF5EE',
        beige: '#F5F5DC',
        oldlace: '#FDF5E6',
        floralwhite: '#FFFAF0',
        ivory: '#FFFFF0',
        antiquewhite: '#FAEBD7',
        linen: '#FAF0E6',
        lavenderblush: '#FFF0F5',
        mistyrose: '#FFE4E1',
        gainsboro: '#DCDCDC',
        lightgray: '#D3D3D3',
        silver: '#C0C0C0',
        darkgray: '#A9A9A9',
        gray: '#808080',
        dimgray: '#696969',
        lightslategray: '#778899',
        slategray: '#708090',
        darkslategray: '#2F4F4F',
        black: '#000000',
        cornsilk: '#FFF8DC',
        blanchedalmond: '#FFEBCD',
        bisque: '#FFE4C4',
        navajowhite: '#FFDEAD',
        wheat: '#F5DEB3',
        burlywood: '#DEB887',
        tan: '#D2B48C',
        rosybrown: '#BC8F8F',
        sandybrown: '#F4A460',
        goldenrod: '#DAA520',
        peru: '#CD853F',
        chocolate: '#D2691E',
        saddlebrown: '#8B4513',
        sienna: '#A0522D',
        brown: '#A52A2A',
        maroon: '#800000'
    }

    /**
     * Serializes any css color interpretation to an RGBA color.
     * @param cssColorCode the color code in CSS
     */
    public static serializeToColor = (cssColorCode: string): Color => {
        const colorCode = cssColorCode.trim()
        if (colorCode.startsWith('rgba')) {
            const colorCodeParts = colorCode
                .replace('rgba', '')
                .replace('(', '')
                .replace(')', '')
                .split(',')
                .map((v) => Number(v.trim()) || 0)
            return {
                r: colorCodeParts[0],
                g: colorCodeParts[1],
                b: colorCodeParts[2],
                a: colorCodeParts[3] * 255
            } as Color
        } else if (colorCode.startsWith('rgb')) {
            const colorCodeParts = colorCode
                .replace('rgb', '')
                .replace('(', '')
                .replace(')', '')
                .split(',')
                .map((v) => Number(v.trim()) || 0)
            return {
                r: colorCodeParts[0],
                g: colorCodeParts[1],
                b: colorCodeParts[2],
                a: 255
            } as Color
        } else if (colorCode.startsWith('#')) {
            if (colorCode.length === 4) {
                const hex = [
                    colorCode.substr(0, 1),
                    colorCode.substr(1, 1),
                    colorCode.substr(1, 1),
                    colorCode.substr(2, 1),
                    colorCode.substr(2, 1),
                    colorCode.substr(3, 1),
                    colorCode.substr(3, 1)
                ].join('')
                return Theme.convertHexToColor(hex)
            } else {
                return Theme.convertHexToColor(colorCode)
            }
        } else if (Object.keys(Theme.hexCodePerCssColor).indexOf(colorCode) !== -1) {
            return Theme.convertHexToColor((Theme.hexCodePerCssColor as any)[colorCode])
        }
        console.warn('Color is not recognized, returning black:', cssColorCode)
        return Theme.defaultColor
    }

    /**
     * Converts a hex color code to a `Color`
     * @param hex the hex
     */
    public static convertHexToColor = (hex: string): Color => {
        if (!/^#(?:[0-9a-fA-F]{3}){1,2}$/g.test(hex)) {
            return Theme.defaultColor
        }
        if (hex.length === 4) {
            return {
                r: parseInt(hex.substring(1, 2) + hex.substring(1, 2), 16),
                g: parseInt(hex.substring(2, 3) + hex.substring(2, 3), 16),
                b: parseInt(hex.substring(3, 4) + hex.substring(3, 4), 16),
                a: 255
            } as Color
        }
        return {
            r: parseInt(hex.substring(1, 3), 16),
            g: parseInt(hex.substring(3, 5), 16),
            b: parseInt(hex.substring(5, 7), 16),
            a: 255
        } as Color
    }

    /**
     * Converts a `Color` to a hex color code
     */
    public static convertColorToHex = (color: Color) => {
        let r = Math.floor(color.r).toString(16),
            g = Math.floor(color.g).toString(16),
            b = Math.floor(color.b).toString(16);
      
        if (r.length === 1) {
            r = `0${r}`
        }
        if (g.length === 1) {
            g = `0${g}`
        }
        if (b.length === 1) {
            b = `0${b}`
        }
        return `#${r}${g}${b}`.toUpperCase()
    }

    /**
     * Converts an HSL color value to RGB. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * 
     * Assumes h, s, and l are contained in the set [0, 1] and
     * returns r, g, and b in the set [0, 255].
     * 
     * @ref https://stackoverflow.com/a/9493060
     */
    public static convertHSLToColor = (colorHSL: ColorHSL): Color => {
        let r: number, g: number, b: number

        if (colorHSL.s === 0) {
            r = g = b = colorHSL.l // achromatic
        }
        else {
            let hue2rgb = function hue2rgb(p: number, q: number, t: number){
                if(t < 0) t += 1
                if(t > 1) t -= 1
                if(t < 1/6) return p + (q - p) * 6 * t
                if(t < 1/2) return q
                if(t < 2/3) return p + (q - p) * (2/3 - t) * 6
                return p
            }

            let q = colorHSL.l < 0.5 ? colorHSL.l * (1 + colorHSL.s) : colorHSL.l + colorHSL.s - colorHSL.l * colorHSL.s
            let p = 2 * colorHSL.l - q
            r = hue2rgb(p, q, colorHSL.h + 1/3)
            g = hue2rgb(p, q, colorHSL.h)
            b = hue2rgb(p, q, colorHSL.h - 1/3)
        }

        return {
            r: Math.round(r * 255),
            g: Math.round(g * 255),
            b: Math.round(b * 255),
            a: colorHSL.a
        }
    }

    /**
     * Converts an RGB color value to HSL. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * 
     * Assumes r, g, and b are contained in the set [0, 255] and
     * returns h, s, and l in the set [0, 1].
     * 
     * @ref https://stackoverflow.com/a/9493060
     */
    public static convertColorToHSL = (color: Color): ColorHSL => {
        const newColor = {
            r: color.r / 255,
            g: color.g / 255,
            b: color.b / 255,
            a: color.a
        }

        const
            max = Math.max(newColor.r, newColor.g, newColor.b), 
            min = Math.min(newColor.r, newColor.g, newColor.b)
        
        let h: number = 0,
            s: number = 0,
            l: number = (max + min) / 2

        if (max === min) {
            h = s = 0 // achromatic
        } else {
            const d = max - min
            s = l > 0.5
                ? d / (2 - max - min)
                : d / (max + min)

            switch (max) {
                case newColor.r: h = (newColor.g - newColor.b) / d + (newColor.g < newColor.b ? 6 : 0)
                    break
                case newColor.g: h = (newColor.b - newColor.r) / d + 2
                    break
                case newColor.b: h = (newColor.r - newColor.g) / d + 4
                    break
            }
            h /= 6
        }

        return {
            h: h * 360,
            s: s * 100,
            l: l * 100,
            a: newColor.a
        }
    }

    /**
     * Convert a color to a HSV based color
     */
    public static convertColorToHSV = (color: Color): ColorHSV => {
        let {r, g, b} = color

        let computedH = 0
        let computedS = 0
        let computedV = 0
       
        r /= 255
        g /= 255
        b /= 255

        const minRGB = Math.min(r, Math.min(g,b))
        const maxRGB = Math.max(r, Math.max(g,b))
       
        // Black-grey-white
        if (minRGB === maxRGB) {
            computedV = minRGB
            return {
                h: 0,
                s: 0,
                v: computedV * 100,
                a: color.a
            }
        }
       
        // Colors other than black-grey-white:
        const d = r === minRGB
            ? g - b 
            : (b === minRGB ? r - g : b - r)

        const h = r === minRGB
            ? 3
            : (b === minRGB ? 1 : 5)

        computedH = 60 * (h - d / (maxRGB - minRGB))
        computedS = (maxRGB - minRGB) / maxRGB
        computedV = maxRGB

        return {
            h: computedH,
            s: computedS * 100,
            v: computedV * 100,
            a: color.a
        }
    }

    /**
     * Get the accent type of a color, based on its RGB values.
     * The accent type represents the color of text that is readable on the background color.
     */
    public static getAccentType = (color: Color): 'dark' | 'light' => {
        const brightness  = ((color.r * 299) + (color.g * 587) + (color.b * 114)) / 1000
        return brightness > 155
            ? 'dark'
            : 'light'   
    }
}