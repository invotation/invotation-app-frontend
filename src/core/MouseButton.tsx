/**
 * Represents the indices per mouse button
 * @author Anonymous
 */
 export enum MouseButton {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2
}