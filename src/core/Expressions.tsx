/**
 * All regular expressions (RegEx) used throughout the application.
 * @author Anonymous
 */
 export default class Expressions {

    /**
     * Validate an email address
     */
    public static readonly email = /^[\w-+]+(\.[\w]+)*@[\w-]+(\.[\w]+)*(\.[a-zA-Z]{2,})$/

    /**
     * Validate a dutch phone number
     */
    // eslint-disable-next-line
    public static readonly dutchPhoneNumber = /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/

    /**
     * Validate a full name
     */
    public static readonly fullName = /^(\S+ )+\S+$/

    /**
     * Validate a duch postal code
     */
    public static readonly dutchPostalCode = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i

    /**
     * Validate a numeric value
     */
    public static readonly numeric = /^[0-9]*$/

    /**
     * Validate a hex color code
     */
    public static readonly hexColor = /^#(?:[0-9a-fA-F]{3}){1,2}$/g

    /**
     * Validate a firstname and/or last name
     */
    public static readonly firstNameLastName = /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/g

    /**
     * Validate a measurement unit
     */
    public static readonly measurementUnit = /^(-)?[0-9]+([.][0-9]+)?(px|rem|em|%|vw|vh)?$/g

    /**
     * Validate an integer or a decimal value
     */
    public static readonly integerOrDecimal = /^[0-9]+([.][0-9]+)?$/g
}