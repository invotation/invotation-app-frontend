/**
 * All locales in the application
 * @author Stan Hurks
 */
export enum Locale {
    EN_GB = 'EN_GB',
    NL_NL = 'NL_NL'
}